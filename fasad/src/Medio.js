export function atendElektRealig(elekt) {
  // Nombro de trovitaj cimoj: 3
  return new Promise(function(resolve) {
    /* Ĉi tiu funkcio jam kaŭzis ~2-3 cimojn. Jes, ĝi aspektas abomenege, sed ĉi tiu
    versio almenaŭ laboras. Do estu raciulo: ne tuŝu ĉi tiun imperativan abomenaĵon.
    Kaj bonvole ne tuŝu imperativaĵojn entute. */
    let res = document.querySelector(elekt);
    if (res) {
      resolve(res);
    } else {
      let obs = new MutationObserver(function() {
        let res = document.querySelector(elekt);
        if (res) {
          obs.disconnect();
          resolve(res);
        }
      });
      obs.observe(document.body, {
        childList: true,
        subtree: true
      });
    }
  });
}

export function dinamika() {
  let orig = location.origin;
  // Nombro de trovitaj cimoj: 2
  let obs = new MutationObserver(function() {
    document.querySelectorAll(".relargx-y").forEach(function(target) {
        if (target.scrollHeight != 0) {
          let parent = target.parentElement;
          while (parent != null && !parent.classList.contains("relargx-baz")) {
            parent = parent.parentElement;
          }
          if (parent == null) {
            console.error("No scroll parent");
            return;
          }
          let t = parent.scrollTop;
          target.style.removeProperty('height');
          target.style.height = target.scrollHeight + "px";
          parent.scrollTop = t;
          // AVERTO. VERŜAJNE KAŬZAS REGRESON!
        }
    });

    document.querySelectorAll("img").forEach(function(target) {
      if (target.dataset.fiask == null) {
        target.dataset.fiask = true;
        target.addEventListener("error", () => {
          try {
            let petUrl = new URL(target.src);
            if (petUrl.origin != orig) {
              target.src = `${orig}/-/ekster-risurc/${petUrl.host}${petUrl.pathname}`;
            }
          } catch(e) {}
        });
      }
    });
  });
  obs.observe(document.body, {
    childList: true,
    subtree: true
  });
}
