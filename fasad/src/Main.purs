module Main (main) where

import Prelude
import Control.Lazy (fix)
import Control.Monad.Maybe.Trans (MaybeT(..), runMaybeT)
import Data.Argonaut (class DecodeJson, Json, decodeJson, isNull)
import Data.Either (hush)
import Data.Foldable (for_)
import Data.HashMap as HM
import Data.Lazy (Lazy, defer, force)
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.String (Pattern(..), split)
import Data.Tuple.Nested ((/\))
import Datum (GxisKuntKerna(..), Knt(..), Model, Msg(..), Pagx, GxisKunt, de, kaz, kernTr, kntFokus, kntSe, mankAuxS, var)
import Effect (Effect)
import Effect.Aff (Aff, launchAff_)
import Effect.Aff.Class (liftAff)
import Effect.Class (liftEffect)
import Flame (Html, QuerySelector(..))
import Flame.Application.Effectful (Environment, mount_)
import Flame.Html.Attribute as HA
import Flame.Html.Element as HE
import Flame.Subscription.Internal.Create as SC
import Flame.Types (Source(..))
import Medio (JsonHM(..), Konservata(..), Lingv, akirK, dinamika, et_, injV, lastVizK, malpak, nunLok, petKernSenAux, pord, simbol, stilK)
import Pagx.Hered.Analiz (cxuStila)
import Pagx.Hered.Montr (sxprucErar)
import Pagx.Hered.Montr as PM
import Supr.KreiKont (auxStatus, kreiKont)
import Supr.Nav (krTestNav, montrNav)
import Supr.UzantMenu (uzantMenu)
import Voj (SavStat, alVoj, cxesSxarg, pagxKondut, sekurasForlasi)
import Web.HTML (window)
import Web.HTML.HTMLDocument (setTitle)
import Web.HTML.Location (hash, pathname)
import Web.HTML.Window (document)

gxisTraktil :: Environment Model Msg -> Aff (Model -> Model)
gxisTraktil { message, model, display } = case message of
  Gxis f -> f (force $ fix \si -> defer \_ -> gkt si) $> identity
  Efekt ag -> ag $> identity
  Nul -> pure identity
  where
  gkt :: Lazy (GxisKunt Model) -> GxisKunt Model
  gkt si = { model, revid: \x -> liftAff $ display x, kerna: GxisKuntKerna si }

vid :: Model -> Html Msg
vid model =
  HE.div_ $ kntFokus kern (et_ :: _ "pagx")
    # \p ->
        montrPagx p
          <> let
              { superniv, stilu } =
                kaz p
                  ( de
                      # var (et_ :: _ "regPagx")
                          ( \(Knt { val }) ->
                              { superniv:
                                  kntSe
                                    (kntFokus kern (et_ :: _ "auxStatus"))
                                    (et_ :: _ "atendKreiS")
                                    (\x -> [ kreiKont x ])
                                    (\_ -> [])
                              , stilu: not $ cxuStila val
                              }
                          )
                      # var (et_ :: _ "erarPagx") (\_ -> { superniv: [], stilu: true })
                      # var (et_ :: _ "sxalt") (\_ -> { superniv: [], stilu: true })
                  )
            in
              superniv <> pord stilu [ HE.style' [ HA.innerHtml model.css ] ]
  where
  trd' :: String -> ∀ a. DecodeJson a => Maybe a
  trd' x = do
    json <- HM.lookup x model.lingv
    hush $ decodeJson json

  trd x = fromMaybe x $ trd' x

  sek = defer \_ -> sekurasForlasi model.pagx

  mond =
    { trd
    , trd'
    , sekurasForlasi: sek
    , superskribBild: model.superskribBild
    , lastViz: model.lastViz
    }

  kern = Knt { tr: kernTr, val: model, mond }

  -- FARENDE: `sxarg` por aliaj tipoj de paĝoj?
  montrPagx :: Knt Pagx -> Array (Html Msg)
  montrPagx p =
    kaz p
      ( de
          # var (et_ :: _ "regPagx")
              ( \rp ->
                  let
                    auxKnt = kern `kntFokus` (et_ :: _ "auxStatus")

                    { rulum, pagx } = PM.montrPagx rp
                  in
                    [ HE.div [ HA.id "supr" ]
                        $ [ PM.redaktBtn rp, montrNav mond krTestNav ]
                        <> uzantMenu
                            ( kntSe auxKnt (et_ :: _ "atendKreiS")
                                (\krei -> kntFokus krei (et_ :: _ "malferm"))
                                (\_ -> kntFokus rp (et_ :: _ "malfermUzInf"))
                            )
                            auxKnt
                    , HE.div [ HA.id "rulumej", HA.class' "relargx-baz" ]
                        ( rulum
                            <> ( model.sxarg
                                  # maybe [] \{ msg, finit } ->
                                      let
                                        sim /\ tekst = fromMaybe ("pending" /\ "") msg
                                      in
                                        [ HE.div
                                            ( [ HA.id "sxarg" ]
                                                <> case finit of
                                                    Nothing -> []
                                                    Just { gracie } ->
                                                      [ HA.class'
                                                          $ if gracie then
                                                              "fin-gracie"
                                                            else
                                                              "fin-malgracie"
                                                      ]
                                            )
                                            [ simbol [ HA.class' $ "simbol-" <> sim ] sim
                                            , HE.div_ $ trd tekst
                                            ]
                                        ]
                              )
                        )
                    ]
                      <> pagx
              )
          # var (et_ :: _ "erarPagx")
              ( \(Knt { val: tekst }) ->
                  [ HE.div
                      [ HA.class' "supernivel" ]
                      [ sxprucErar
                          mond
                          [ simbol
                              [ HA.id "avert-mark" ]
                              "warning"
                          ]
                          tekst
                      ]
                  ]
              )
          # var (et_ :: _ "sxalt") (\_ -> [])
      )

main :: Effect Unit
main =
  launchAff_ do
    lastViz <- akirK lastVizK
    css <- fromMaybe "" <$> akirK stilK
    let
      lingvar :: MaybeT Aff Lingv
      lingvar = (\(JsonHM x) -> x) <$> (malpak =<< petKernSenAux "/-/lingvar" unit)

      komEf =
        Gxis \g@{ revid } -> do
          liftEffect dinamika -- Ekfunkciigi dinamikajn partojn de la paĝo
          let
            f (ag :: MaybeT Aff _) =
              liftEffect
                $ launchAff_ do
                    r <- runMaybeT ag
                    case r of
                      Just x -> revid x
                      Nothing -> pure unit
          f do
            lok <- nunLok
            voj <- liftEffect $ pathname lok
            plenHash <- liftEffect $ hash lok
            liftAff $ alVoj g { voj, plenHash }
            pure identity
          f $ lingvar <#> \x m -> m { lingv = x }
          f $ (MaybeT auxStatus) <#> \x m -> m { auxStatus = x }

      komenc :: Model
      komenc =
        { lingv: HM.empty
        , auxStatus: mankAuxS
        , sxarg: Nothing
        , pagx: injV (et_ :: _ "sxalt") unit
        , superskribBild: HM.empty
        , pagxKasxm: HM.empty
        , lastViz
        , css
        }
    liftEffect
      $ mount_ (QuerySelector "body")
          { init: komenc /\ Just komEf
          , subscribe:
              [ SC.createRawSubscription Window "hashchange" \(ev :: { newURL :: String }) -> case split (Pattern "#") ev.newURL of
                  [ _, h ] -> Gxis \m -> (pagxKondut m.model.pagx).alHash { neNulHash: h, tuj: false }
                  _ -> Nul
              , SC.createRawSubscription Window "popstate" \(ev :: { state :: Json }) ->
                  Gxis \gxisKnt@{ revid } ->
                    when (not $ isNull ev.state) $ void
                      $ runMaybeT do
                          { ald, pagx } :: SavStat <- malpak $ decodeJson ev.state
                          let
                            kondut = pagxKondut pagx
                          for_ kondut.stat \stat' -> do
                            stat'.leg ald
                          liftEffect (window >>= document >>= setTitle kondut.titol)
                          cxesSxarg gxisKnt
                          revid _ { pagx = pagx, sxarg = Nothing }
              , SC.createRawSubscription Window "storage" \(ev :: { key :: String, newValue :: Json }) ->
                  Gxis \{ revid } -> do
                    let
                      key' = Konservata ev.key
                    if key' == lastVizK then
                      revid _ { lastViz = hush $ decodeJson ev.newValue }
                    else if key' == stilK then
                      revid _ { css = fromMaybe "" $ hush $ decodeJson ev.newValue }
                    else
                      pure unit
              ]
          , update: gxisTraktil
          , view: vid
          }
