"use strict";

export function enigDosierKlak() {
  let dos = document.getElementById("enig-dosier");
  dos.value = null;
  dos.click();
}

export function enigDosierMet(ev) {
  return function () {
    ev.preventDefault();
    return ev.dataTransfer.files;
  }
}

export function enigDosierLeg() {
  return document.getElementById("enig-dosier").files;
}

export function kreiUzantbild(inf) {
  return function() {
    let bild = document.createElement("div");
    setTimeout(function() {
      let kons = bild.parentElement;
      bild.cropper = new Croppie(bild, {
        url: inf.font,
        points: inf.akirPart(null)(function(part) {
          return [part.topx, part.topy, part.bottomx, part.bottomy];
        }),
        viewport: {
          width: kons.offsetWidth,// * 0.85,
          height: kons.offsetHeight,// * 0.85,
          type: "square"
        }});
      inf.injBildPart({
        part: function() {
          let r = bild.cropper.get().points.map(function(x) { return parseInt(x); });
          return {
            topx: r[0],
            topy: r[1],
            bottomx: r[2],
            bottomy: r[3]
          };
        },
        part64: function() {
          return bild.cropper.result({type: "base64", size: "original"});
        }
      })();
    }, 100);
    return bild;
  }
}

export function gxisUzantbild(bild) {
  return function(prev) {
    return function(inf) {
      return function() {
        if (prev.font == inf.font) {
          return bild;
        }
        return kreiUzantbild(font);
      }
    }
  }
}

export function dosAlPlenBase64(dos) {
  return function() {
    return new Promise(function(rezultig) {
       var l = new FileReader();
       l.readAsDataURL(dos);
       l.onload = function () {
         rezultig(l.result);
       };
    })
  }
}
