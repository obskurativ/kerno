module Supr.KreiKont where

import Prelude
import Control.Monad.Maybe.Trans (MaybeT(..), runMaybeT)
import Control.Monad.Trans.Class (lift)
import Control.Promise (Promise, toAff)
import Data.Array (sortBy)
import Data.Array as A
import Data.Bifunctor (bimap)
import Data.Either (Either(..))
import Data.HashMap (toArrayBy)
import Data.HashMap as HM
import Data.Int (ceil)
import Data.Lens ((.~))
import Data.Lens.AffineTraversal (cloneAffineTraversal)
import Data.Lens.Record (prop)
import Data.Lens.Setter ((%~))
import Data.List (List(..), (:))
import Data.List as L
import Data.Maybe (Maybe(..), fromMaybe, isJust, isNothing, maybe)
import Data.Newtype (unwrap)
import Data.Set as S
import Data.String (CodePoint, Pattern(..), Replacement(..), drop, fromCodePointArray, indexOf, joinWith, length, replace, toCodePointArray, toLower, uncons)
import Data.Traversable (for)
import Data.Tuple (Tuple(..))
import Data.Tuple.Nested (type (/\), (/\))
import Datum (AnstAr, AuxS, BildPart, GxisKunt, Knt(..), KreiKontErar(..), KreiKontS, Msg(..), UzantPri, Model, gxis, kernTr, longGxis, mankAuxS)
import Effect (Effect)
import Effect.Aff (Aff)
import Effect.Aff.Class (liftAff)
import Effect.Class (liftEffect)
import Effect.Ref as Ref
import Flame (Html)
import Flame.Html.Attribute as HA
import Flame.Html.Element (managed_)
import Flame.Html.Element as HE
import Medio (JsonHM(..), UnitResp, Var, akirK, btn, enFiltr, erari, et_, forigK, injV, jxetonK, malpak, onV, petKernAux, petKernSenAux, pord, simbol)
import Web.DOM (Node)
import Web.Event.Event (Event, preventDefault)
import Web.File.File (File, size, type_)
import Web.File.FileList (FileList, item)

atendKreiS :: Maybe { name :: String, pri :: UzantPri } -> MaybeT Aff AuxS
atendKreiS status = do
  agord <-
    malpak =<< petKernSenAux "/-/aux/agord" {}
  lastM <-
    for status \uzant -> ado
      avatar <- malpak =<< petKernSenAux "/-/uzantbild" { name: uzant.name }
      in { avatar, uzant }
  profilM <-
    for lastM \{ avatar, uzant: { pri } } -> ado
      bildPart <- liftEffect $ Ref.new Nothing
      in { pri, avatar: { bild: _, bildPart } <$> avatar }
  pure
    $ injV (et_ :: _ "atendKreiS")
        { malferm: true
        , jamUzitajNomoj: Nil
        , agord:
            agord
              { replaces = kreiAnst agord.replaces
              }
        , profil:
            fromMaybe
              { avatar: Nothing, pri: { fullName: "", about: "" } }
              profilM
        , last: lastM
        , atendu: false
        , erar: Nothing
        }

auxStatus :: Aff (Maybe AuxS)
auxStatus =
  runMaybeT do
    jxeton <- MaybeT $ akirK jxetonK
    { profile } :: { profile :: Maybe { name :: _, fullName :: _, about :: _ } } <-
      (MaybeT <<< pure)
        =<< malpak
        =<< petKernSenAux "/-/aux/status" { token: jxeton }
    case profile of
      Nothing -> atendKreiS Nothing
      Just f -> pure $ injV (et_ :: _ "auxS") { elir: false, uzant: { name: f.name, pri: { fullName: f.fullName, about: f.about } } }

-- FARENDE: KreiKont'an y-largxon kaj sxangxi Mainan sistemon do enhavon
-- de la paĝo ne montriĝas
-- Respektivan butonon por malfermi la menuon
-- Butonon por elsaluti...
strAlList :: String -> List CodePoint
strAlList =
  uncons
    >>> case _ of
        Just { head, tail } -> head : strAlList tail
        Nothing -> Nil

listAlStr :: List CodePoint -> String
listAlStr = fromCodePointArray <<< A.fromFoldable

kreiAnst :: JsonHM String -> AnstAr
kreiAnst (JsonHM ar) =
  bimap strAlList strAlList
    <$> sortBy
        (\(t1 /\ _) (t2 /\ _) -> compare (length t2) (length t1))
        (toArrayBy Tuple ar)

aplAnst :: AnstAr -> List CodePoint -> List CodePoint
aplAnst ar = p
  where
  p list = p' 0
    where
    p' n = case ar `A.index` n of
      Just (sxab /\ anst) -> case L.stripPrefix (L.Pattern sxab) list of
        Just rest -> anst <> p rest
        Nothing -> p' $ n + 1
      Nothing -> case list of
        Nil -> Nil
        (x : xs) -> x : p xs

perm :: S.Set CodePoint
perm = S.fromFoldable $ toCodePointArray "abcdefghijklmnopqrstuvwxyz0123456789"

part :: List CodePoint -> List (List CodePoint)
part = p' Nil
  where
  p' nun = case _ of
    Nil -> L.reverse nun : Nil
    (x : xs)
      | x `S.member` perm -> p' (x : nun) xs
      | otherwise -> (L.reverse nun) : part xs

kreiId :: AnstAr -> String -> String
kreiId anst =
  toLower >>> strAlList >>> aplAnst anst
    >>> part
    >>> L.filter (_ /= Nil)
    >>> map (listAlStr)
    >>> A.fromFoldable
    >>> joinWith "-"

foreign import enigDosierKlak :: Effect Unit

foreign import enigDosierMet :: Event -> Effect FileList

foreign import enigDosierLeg :: Effect FileList

type UzantBild'
  = { injBildPart :: { part :: Effect BildPart, part64 :: Effect (Promise String) } -> Effect Unit, akirPart :: ∀ a. a -> (BildPart -> a) -> a, font :: String }

foreign import kreiUzantbild :: UzantBild' -> Effect Node

foreign import gxisUzantbild :: Node -> UzantBild' -> UzantBild' -> Effect Node

foreign import dosAlPlenBase64 :: File -> Effect (Promise String)

kreiKont :: Knt KreiKontS -> Html Msg
kreiKont (Knt { tr, mond: { trd }, val }) =
  if not val.malferm then
    HE.br
  else
    HE.div [ HA.class' "meze supernivel" ]
      [ HE.div [ HA.id "krei-kont" ]
          ( pord (isNothing val.last)
              [ simbol
                  [ HA.id "rezign"
                  , HA.class' "simbol-logout"
                  , HA.title $ trd "aux.krei-kont.rezign"
                  , HA.onClick
                      $ gxis kernTr do
                          forigK jxetonK
                          -- FARENDE: Informu la servilon pri eliro.
                          pure $ _ { auxStatus = mankAuxS }
                  ]
                  "logout"
              ]
              <> [ simbol
                    ( [ HA.id "ferm" ]
                        <> case val.last of
                            Nothing ->
                              [ HA.title $ trd "aux.krei-kont.ferm"
                              , HA.onClick $ gxis tr $ pure _ { malferm = false }
                              ]
                            Just { uzant } -> [ HA.onClick $ gxis kernTr $ pure _ { auxStatus = injV (et_ :: _ "auxS") { uzant, elir: false } } ]
                    )
                    "close"
                , HE.h1_ [ HE.text $ trd "aux.krei-kont.bonvenon" ]
                , HE.hr
                , HE.div_
                    [ HE.lazy Nothing
                        ( \imgMIME ->
                            HE.input
                              [ HA.id "enig-dosier"
                              , HA.type' "file"
                              , HA.accept $ joinWith "," $ A.fromFoldable (("image/" <> _) <$> imgMIME)
                              , HA.onChange' \_ -> sxaltAv $ enigDosierLeg
                              ]
                        )
                        agord.imageMIME
                    , HE.div
                        ( [ HA.id "uzantbild" ]
                            <> if isJust avatar then
                                [ HA.class' "elektit" ]
                              else
                                [ HA.onDragover' \ev -> Efekt $ liftEffect $ preventDefault ev
                                , HA.onClick' \_ -> Efekt $ liftEffect $ enigDosierKlak
                                , HA.onDrop' \ev ->
                                    sxaltAv $ enigDosierMet ev
                                ]
                        ) case avatar of
                        Just b ->
                          [ simbol
                              [ HA.id "ferm"
                              , HA.onClick $ gxis tr $ pure $ _ { profil { avatar = Nothing } }
                              ]
                              "cancel"
                          , managed_
                              ( { createNode: kreiUzantbild
                                , updateNode: gxisUzantbild
                                }
                              )
                              $ let
                                  { mime, base64, part } = b.bild
                                in
                                  { font: "data:" <> mime <> ";base64," <> base64
                                  , akirPart: \a f -> maybe a f part
                                  , injBildPart: \x -> Ref.write (Just x) b.bildPart
                                  }
                          ]
                        Nothing ->
                          [ simbol [] "file_upload"
                          , HE.div
                              [ HA.id "uzantbild-konsilet" ]
                              [ HE.text $ trd "aux.krei-kont.alsxult" ]
                          ]
                    , HE.input
                        [ HA.placeholder $ trd "aux.krei-kont.uzantnomo"
                        , HA.type' "text"
                        , HA.onInput \v -> gxis tr $ pure _ { profil { pri { fullName = enFiltr v } } }
                        , HA.value fullName
                        , HA.maxlength agord.fullNameLimit
                        ]
                    , HE.div [ HA.id "subskrib" ]
                        [ HE.text $ if nom == "" then "" else "(" <> nom <> ")" ]
                    , HE.div [ HA.class' "fantom" ] [ HE.text fullName ]
                    , HE.textarea'
                        [ HA.placeholder $ trd "aux.krei-kont.uzantpri"
                        , HA.onInput \v -> gxis tr $ pure $ _ { profil { pri { about = v } } }
                        , HA.value about
                        , HA.maxlength agord.aboutLimit
                        ]
                    , let
                        nesendebla = subNomLim || nomoJamUzita || val.atendu
                      in
                        HE.div
                          ( [ HA.id "krei-btn" ]
                              <> btn
                                  ( pord (not nesendebla) $ Just
                                      $ longGxis kernTr sendRedakt
                                  )
                              <> pord subNomLim
                                  [ HA.title $ replace (Pattern "$0") (Replacement (show agord.nameMin))
                                      $ trd "aux.krei-kont.erar.nom-min"
                                  ]
                          )
                          [ HE.text $ trd "aux.krei-kont.fini" ]
                    ]
                ]
              <> ( case val.erar of
                    Just e -> case e of
                      NevalidAvatar -> sxpruc' "erar.bildformat" $ listig agord.imageMIME
                      TroGrandaAvatar -> sxpruc' "erar.tro-granda" $ show agord.avatarLimitMB
                    Nothing -> if nomoJamUzita then sxpruc' "aux.krei-kont.erar.nom-okupita" nom else []
                )
          )
      ]
  where
  { avatar, pri: { fullName, about } } = val.profil

  nom = kreiId agord.replaces fullName

  agord = val.agord

  nomoJamUzita = nom `L.elem` val.jamUzitajNomoj

  subNomLim = length nom < agord.nameMin

  sxpruc t = [ HE.div [ HA.class' "sxpruc-erar" ] [ HE.text t ] ]

  sxpruc' net anst = sxpruc $ replace (Pattern "$0") (Replacement anst) $ trd net

  sxaltAv :: Effect FileList -> Msg
  sxaltAv afl =
    gxis tr do
      fl <- liftEffect afl
      f <- MaybeT $ pure $ item 0 fl
      mime <- unwrap <$> MaybeT (pure $ type_ f)
      if not (mime `L.elem` (agord.imageMIME)) then
        pure $ _ { erar = Just NevalidAvatar }
      -- FARENDE: Eblon malplibonigi la bildon!
      else if (ceil (size f) >= agord.avatarLimitMB * 1024 * 1024) then
        pure $ _ { erar = Just TroGrandaAvatar }
      else do
        base64 <- plen64al64 <$> ((liftAff <<< toAff) =<< (liftEffect $ dosAlPlenBase64 f))
        bildPart <- liftEffect $ Ref.new Nothing
        pure $ _ { profil { avatar = Just { bild: { mime, base64, part: Nothing }, bildPart } } }

  listig :: List String -> String
  listig = case _ of
    (x1 : x2 : x3 : xs) -> x1 <> ", " <> listig (x2 : x3 : xs)
    (x1 : x2 : Nil) -> x1 <> " " <> trd "au" <> " " <> x2
    (x : Nil) -> x
    Nil -> ""

  plen64al64 :: String -> String
  plen64al64 e = case indexOf (Pattern ",") e of
    Just x -> drop (x + 1) e
    Nothing -> e

  sendRedakt :: GxisKunt Model -> Aff Unit
  sendRedakt { revid } =
    void
      $ runMaybeT do
          (novBild /\ avatar') ::
            ( _
                /\ Var
                  ( new :: { base64 :: String, mime :: String } /\ BildPart
                  , newPart :: BildPart
                  , keep :: Unit
                  , null :: Unit
                  )
            ) <- case avatar of
            Nothing -> pure (Left { nul: true } /\ injV (et_ :: _ "null") unit)
            Just avatar' -> do
              { part: partAkiril, part64: part64Akiril } <- MaybeT $ liftEffect $ Ref.read avatar'.bildPart
              bildPart <- liftEffect partAkiril
              let
                akirNovBild = Right <$> liftAff (toAff =<< liftEffect part64Akiril)
              case avatar'.bild.part of
                Nothing ->
                  let
                    { base64, mime } = avatar'.bild
                  in
                    akirNovBild <#> (_ /\ (injV (et_ :: _ "new") $ { base64, mime } /\ bildPart))
                Just oldPart ->
                  if oldPart == bildPart then do
                    pure (Left { nul: false } /\ injV (et_ :: _ "keep") unit)
                  else do
                    akirNovBild <#> (_ /\ injV (et_ :: _ "newPart") bildPart)
          r <-
            lift do
              let
                atTr = cloneAffineTraversal tr <<< prop (et_ :: _ "atendu")
              revid (atTr .~ true)
              r <- petKernAux "/-/aux/redakt" { fullName: fullName, avatar: avatar', about: about }
              revid (atTr .~ false)
              pure r
          case r of
            Left e ->
              onV
                (et_ :: _ "nonUniqueName")
                (\_ -> revid $ (cloneAffineTraversal tr) %~ \x -> x { jamUzitajNomoj = nom : x.jamUzitajNomoj })
                (\e' -> erari (show e'))
                e
            Right (_ :: UnitResp) -> do
              s <- MaybeT auxStatus
              revid \m ->
                -- FARENDE: Bildeton de neekzistanta ulo
                m { auxStatus = s }
                  # let
                      met v = _ { superskribBild = HM.insert nom v m.superskribBild }
                    in
                      case novBild of
                        Left { nul: true } -> met ""
                        Left { nul: false } -> identity
                        Right v -> met v
