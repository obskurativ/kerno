module Supr.UzantMenu where

import Control.Plus (empty)
import Data.Either (Either(..), fromRight, isRight)
import Data.HashMap as HM
import Data.Lens ((.~), (^?))
import Data.Lens.AffineTraversal (cloneAffineTraversal)
import Data.Lens.Record (prop)
import Data.Maybe (Maybe(..))
import Data.Monoid (guard)
import Data.String (toLower)
import Data.Tuple.Nested ((/\))
import Datum (AuxS, Knt(..), Msg(..), afTrP, de, gxis, kaz, kernTr, mankAuxS, met, var)
import Effect.Aff (Milliseconds(..), delay)
import Effect.Aff.Class (liftAff)
import Flame (Html)
import Flame.Html.Attribute as HA
import Flame.Html.Element as HE
import Flame.Html.Event as HEv
import Medio (UnitResp, btn, erari, et_, forigK, injV, jxetonK, onV, petKernSenAux, pord, simbol)
import Prelude (bind, discard, not, pure, show, unless, (#), ($), (*>), (<$>), (<<<), (<>), (==))
import Supr.KreiKont (atendKreiS)
import Text.Email.Validate (EmailAddress(..), validate)

-- FARENDE: Fermi uzantmenuo je aliloka klako?
uzantMenu :: Knt Boolean -> Knt AuxS -> Array (Html Msg)
uzantMenu (Knt malfknt) (knt@(Knt { tr: uzMTr, mond: { trd, superskribBild } })) =
  [ HE.div
      [ HA.id "uzant"
      , HEv.onClick $ gxis malfknt.tr $ pure not
      ]
      $ let
          mankAux = [ simbol [] "account_circle" ]
        in
          kaz knt
            ( de
                # var (et_ :: _ "mankAuxS") (\_ -> mankAux)
                # var (et_ :: _ "mankAuxSenditS") (\_ -> mankAux)
                # var (et_ :: _ "atendKreiS") (\_ -> [ simbol [] "hourglass_empty" ])
                # var (et_ :: _ "auxS")
                    ( \(Knt { val }) ->
                        [ HE.img
                            [ HA.src
                                $ case HM.lookup val.uzant.name superskribBild of
                                    Nothing -> "/-/uzantbild/" <> val.uzant.name
                                    Just x -> x
                            ]
                        , HE.div' [ HA.id "ombro" ]
                        ]
                    )
            )
  ]
    <> guard malfknt.val
        ( kaz knt
            ( de
                # var (et_ :: _ "mankAuxS") jeMankAuxS
                # var (et_ :: _ "mankAuxSenditS")
                    ( \(Knt { val }) ->
                        -- FARENDE: Reenbutonon
                        uzMBlok
                          [ HE.text $ trd "aux.sukces.1"
                          , HE.br
                          , HE.text $ trd "aux.sukces.2"
                          , HE.br
                          , simbol [ HA.class' "sukc-mark" ] "task_alt"
                          , HE.br
                          , HE.div_
                              [ HE.text (trd "aux.vizitu" <> " ")
                              , HE.a [ HA.href $ "https://" <> val.cellok ] [ HE.text val.cellok ]
                              ]
                          ]
                    )
                # var (et_ :: _ "atendKreiS") (\_ -> [])
                # var (et_ :: _ "auxS")
                    ( \(Knt { val: ekz, tr }) ->
                        uzMBlok
                          [ HE.div "list"
                              [ HE.div
                                  (btn $ Just $ met uzMTr $ atendKreiS $ Just ekz.uzant)
                                  [ simbol [] "manage_accounts", HE.div_ [ HE.text $ trd "aux.redakt" ] ]
                              , HE.div
                                  ( ( btn $ Just
                                        $ Gxis \{ model, revid } -> case model ^? cloneAffineTraversal tr of
                                            Just { elir: true } -> do
                                              forigK jxetonK
                                              revid (cloneAffineTraversal uzMTr .~ mankAuxS)
                                            _ -> do
                                              let
                                                elirTr = cloneAffineTraversal tr <<< prop (et_ :: _ "elir")
                                              revid $ (elirTr .~ true)
                                              liftAff $ delay $ Milliseconds 2000.0
                                              revid (elirTr .~ false)
                                    )
                                      <> pord ekz.elir [ HA.class' "elir-konfirm" ]
                                  )
                                  [ simbol [ HA.class' "simbol-logout" ] "logout"
                                  , HE.div_
                                      [ HE.text $ trd $ "aux.eliri" ]
                                  ]
                              ]
                          ]
                    )
            )
        )
  where
  uzMBlok = pure <<< HE.div [ HA.id "uzant-menu" ]

  jeMankAuxS (Knt { val, tr }) =
    let
      -- FARENDE: sxaltindikilon?
      msend =
        gxis kernTr do
          unless val.gxusta empty
          r <- petKernSenAux "/-/aux/pet" { email: val.v }
          case r of
            Left e ->
              onV
                (et_ :: _ "invalidEmail")
                ( \_ ->
                    pure
                      $ afTrP tr (et_ :: _ "malbonRetposxt")
                      .~ true
                )
                (\_ -> erari (show e) *> empty)
                e
            Right (_ :: UnitResp) ->
              pure
                $ (cloneAffineTraversal uzMTr)
                .~ injV (et_ :: _ "mankAuxSenditS") { cellok: val.dom }
    in
      uzMBlok
        ( [ HE.text $ trd $ "aux.auxtentigxo"
          , HE.input
              [ HA.type' "text"
              , HA.placeholder $ trd $ "aux.retposxt"
              , HA.value val.v
              , HEv.onInput \nov ->
                  met tr do
                    let
                      v = toLower nov

                      verig = validate v

                      gxusta = isRight verig

                      dom =
                        fromRight ""
                          $ (\(EmailAddress { domainPart }) -> domainPart)
                          <$> verig
                    pure { v, gxusta, dom, malbonRetposxt: false }
              , HEv.onKeydown \(key /\ _) ->
                  if (key == "Enter") then
                    msend
                  else
                    Nul
              ]
          , HE.div
              ([ HA.id "ensalut-btn" ] <> btn (pord val.gxusta $ Just msend))
              [ HE.text $ trd "aux.ensalutu" ]
          -- FARENDE: ERARON!
          ]
            <> ( if val.malbonRetposxt then
                  [ HE.div [ HA.class' "sxpruc-erar" ] [ HE.text $ trd "aux.erar.ne-ekzistas" ]
                  ]
                else
                  []
              )
        )
