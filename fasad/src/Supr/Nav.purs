module Supr.Nav where

import Prelude
import Data.Array (length, (..))
import Data.Tuple (Tuple(..))
import Data.Tuple.Nested ((/\), type (/\))
import Datum (Msg, Mondo)
import Flame (Html)
import Flame.Html.Attribute as HA
import Flame.Html.Element as HE
import Voj (sligil)

type Nav
  = Array NavKat

type NavKat
  = String /\ Array NavSubkat

type NavSubkat
  = Array NavElem

type NavElem
  = { nom :: String, voj :: String }

krTestNav :: Nav
krTestNav =
  1 .. 3
    <#> \k ->
        Tuple ("Kategorio " <> show k) $ 1 .. 14
          <#> \s ->
              let
                elemNom = case s `mod` 3 of
                  0 -> 7
                  1 -> 5
                  _ -> 2
              in
                1 .. elemNom
                  <#> \e ->
                      { nom: "Elem " <> show e, voj: "/obs/elem-" <> show k <> show s <> show e }

montrNav :: Mondo -> Nav -> Html Msg
montrNav mond = \n -> HE.div [ HA.id "menu" ] $ n <#> montrKat
  where
  montrKat (kNom /\ skj) =
    HE.div [ HA.class' "menu-kat" ]
      [ HE.div_ [ HE.text kNom ]
      , HE.div [ HA.class' "menu-malvolv" ] $ skj <#> montrSkt
      ]

  montrSkt elj =
    let
      span aldn = HA.styleAttr $ "grid-row: span " <> show ((length elj) * 2 + aldn)
    in
      HE.div [ span 1 ] $ HE.div [ span 0 ] $ elj <#> \dat -> sligil mond { voj: dat.voj, nom: [ HE.text dat.nom ] }
