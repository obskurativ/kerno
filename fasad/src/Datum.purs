module Datum where

import Control.Monad.Maybe.Trans (MaybeT, runMaybeT)
import Control.Promise (Promise)
import Data.Argonaut (class DecodeJson, class EncodeJson)
import Data.Argonaut.Decode.Generic (genericDecodeJson)
import Data.Argonaut.Encode.Generic (genericEncodeJson)
import Data.Bounded (class Bounded)
import Data.Bounded.Generic (genericBottom, genericTop)
import Data.Enum (class Enum)
import Data.Enum.Generic (genericPred, genericSucc)
import Data.Eq (class Eq)
import Data.Eq.Generic (genericEq)
import Data.Generic.Rep (class Generic)
import Data.HashMap (HashMap)
import Data.Lazy (Lazy)
import Data.Lens (Prism', prism', (%~), (^?))
import Data.Lens.AffineTraversal (AffineTraversal', cloneAffineTraversal)
import Data.Lens.Record (prop)
import Data.Lens.Types (AnAffineTraversal')
import Data.List (List)
import Data.Maybe (Maybe)
import Data.Ord (class Ord)
import Data.Ord.Generic (genericCompare)
import Data.String (CodePoint)
import Data.Symbol (class IsSymbol)
import Data.Traversable (for_, traverse_)
import Data.Tuple.Nested (type (/\))
import Effect (Effect)
import Effect.Aff (Aff, Fiber)
import Effect.Aff.Class (class MonadAff)
import Effect.Ref (Ref)
import Medio (JsonHM, Lingv, Var, caseV, et_, injV, onV, prjV)
import Pagx.Hered.Tipoj (PagxInf, PagxPart, PagxVoj)
import Prelude (Unit, const, identity, unit, ($), (<$>), (<<<), (>>=))
import Prim.Row (class Cons)
import Record (get)
import Type.Proxy (Proxy)
import Unsafe.Coerce (unsafeCoerce)

type GxisKunt m
  = { model :: m, revid :: (m -> m) -> ∀ a. MonadAff a => a Unit, kerna :: GxisKuntKerna }

newtype GxisKuntKerna
  = GxisKuntKerna (Lazy (GxisKunt Model))

data Msg
  = Gxis
    (GxisKunt Model -> Aff Unit)
  | Efekt (Aff Unit)
  | Nul

data KreiKontErar
  = NevalidAvatar
  | TroGrandaAvatar

type AnstAr
  = Array (List CodePoint /\ List CodePoint)

type UzantPri
  = { fullName :: String
    , about :: String
    }

type Uzant
  = { name :: String, pri :: UzantPri }

type BildPart
  = { topx :: Int, topy :: Int, bottomx :: Int, bottomy :: Int }

type UzantBild
  = { mime :: String, base64 :: String, part :: Maybe BildPart }

type KreiKontS
  = { malferm :: Boolean
    , jamUzitajNomoj :: List String
    , agord ::
        { replaces :: AnstAr
        , fullNameLimit :: Int
        , nameMin :: Int
        , aboutLimit :: Int
        , avatarLimitMB :: Int
        , imageMIME :: List String
        }
    , profil ::
        { avatar :: Maybe { bildPart :: Ref (Maybe { part :: Effect BildPart, part64 :: Effect (Promise String) }), bild :: UzantBild }, pri :: UzantPri }
    , last :: Maybe { avatar :: Maybe UzantBild, uzant :: Uzant }
    , atendu :: Boolean
    , erar :: Maybe KreiKontErar
    }

type AuxS
  = Var
      ( mankAuxS :: { v :: String, gxusta :: Boolean, dom :: String, malbonRetposxt :: Boolean }
      , mankAuxSenditS :: { cellok :: String }
      , atendKreiS :: KreiKontS
      , auxS :: { uzant :: Uzant, elir :: Boolean }
      )

mankAuxS :: AuxS
mankAuxS = injV (et_ :: _ "mankAuxS") { v: "", gxusta: false, dom: "", malbonRetposxt: false }

-- Pagx
type PagxEnhav inf
  = { titol :: String
    , etikedoj :: Array String
    , kod :: String
    , analizit :: Array PagxPart /\ inf
    }

type RegPagxEnhav
  = PagxEnhav PagxInf

type RegPagxRedakt
  = { antmontr :: Boolean
    , antEnhav :: Maybe RegPagxEnhav
    , enhav :: RegPagxEnhav
    , etikedUzoj :: JsonHM Int
    , etikedSercx :: Maybe { ind :: Int, proponoj :: Array String }
    }

type RegPagx
  = { malfermUzInf :: Boolean
    , spac :: String
    , voj :: Array String
    , regxim ::
        Var
          ( montr :: Maybe RegPagxEnhav
          , redakt :: RegPagxRedakt
          )
    , redaktebla :: Boolean
    }

type Pagx
  = Var ( regPagx :: RegPagx, erarPagx :: String, sxalt :: Unit )

-- FARENDE: Eble ni konservu kernon anstataŭ ol "mondo"?
type Mondo
  = { sekurasForlasi :: Lazy Boolean
    , trd :: String -> String
    , trd' :: String -> ∀ a. DecodeJson a => Maybe a
    , superskribBild :: HashMap String String
    , lastViz :: Maybe String
    }

data KasxmFresx
  = KasxmRapida
  | KasxmLonga

-- rubo
derive instance genericKasxm :: Generic KasxmFresx _

instance eqKasxm :: Eq KasxmFresx where
  eq = genericEq

instance ordKasxm :: Ord KasxmFresx where
  compare = genericCompare

instance enumKasxm :: Enum KasxmFresx where
  pred = genericPred
  succ = genericSucc

instance boundedKasxm :: Bounded KasxmFresx where
  bottom = genericBottom
  top = genericTop

instance encodeKasxm :: EncodeJson KasxmFresx where
  encodeJson = genericEncodeJson

instance decodeKasxm :: DecodeJson KasxmFresx where
  decodeJson = genericDecodeJson

-- /rubo?
type PagxKasxm
  = HashMap PagxVoj KasxmFresx

type Model
  = { lingv :: Lingv
    , auxStatus :: AuxS
    , pagx :: Pagx
    , sxarg ::
        Maybe
          { f :: Fiber Unit
          , msg :: Maybe (String /\ String)
          , finit :: Maybe { gracie :: Boolean }
          }
    , superskribBild :: HashMap String String -- Cxi tio estas anstataŭenda per propra datumbazo de uzantoj?
    , pagxKasxm :: PagxKasxm
    , lastViz :: Maybe String
    , css :: String
    }

kernTr :: AnAffineTraversal' Model Model
kernTr = identity

longGxis ::
  ∀ b.
  AnAffineTraversal' Model b ->
  (GxisKunt b -> Aff Unit) ->
  Msg
longGxis tr f =
  Gxis \{ model, revid, kerna } ->
    let
      revid' = revid <<< (tr' %~ _)

      tr' = cloneAffineTraversal tr
    in
      for_ (model ^? tr') \model' ->
        f { model: model', revid: revid', kerna }

-- FARENDE: Interŝanĝi argumentojn
gxis ::
  ∀ b.
  AnAffineTraversal' Model b -> MaybeT Aff (b -> b) -> Msg
gxis tr f =
  Gxis \{ revid } ->
    runMaybeT f
      >>= traverse_ \sxang ->
          revid $ ((cloneAffineTraversal tr) %~ sxang)

met ::
  ∀ b.
  AnAffineTraversal' Model b -> MaybeT Aff b -> Msg
met tr ag = gxis tr (const <$> ag)

newtype Knt a
  = Knt
  { tr :: AnAffineTraversal' Model a
  , val :: a
  , mond :: Mondo
  }

-- Paki?
var ::
  ∀ nom a b r1 r2.
  Cons nom a r1 r2 =>
  IsSymbol nom =>
  Proxy nom ->
  (Knt a -> b) ->
  (Mondo -> AnAffineTraversal' Model (Var r1) -> Var r1 -> b) ->
  Mondo -> AnAffineTraversal' Model (Var r2) -> Var r2 -> b
var nom f rest mond kern =
  onV
    nom
    (\a -> f (Knt { mond, tr: (cloneAffineTraversal kern) <<< varPrism nom, val: a }))
    (rest mond $ unsafeCoerce kern)

de :: ∀ i1 i2 b. i1 -> i2 -> Var () -> b
de _ _ = caseV

kaz :: ∀ b r. Knt (Var r) -> (Mondo -> AnAffineTraversal' Model (Var r) -> Var r -> b) -> b
kaz (Knt { tr, val, mond }) f = f mond tr val

kntFokus ::
  ∀ nom a a' b.
  IsSymbol nom =>
  Cons nom b a' a =>
  Knt (Record a) -> Proxy nom -> Knt b
kntFokus (Knt { tr, val, mond }) nom = (Knt { mond, tr: afTrP tr nom, val: get nom val })

kntSe ::
  ∀ nom a r r' b.
  Cons nom a r r' =>
  IsSymbol nom =>
  Knt (Var r') ->
  Proxy nom ->
  (Knt a -> b) -> (Unit -> b) -> b
kntSe (Knt { tr, val, mond }) nom f au = var nom f (\_ _ _ -> au unit) mond tr val

-- FARENDE: Forigi?
afTrP ::
  ∀ nom a a' b.
  IsSymbol nom =>
  Cons nom b a' a =>
  AnAffineTraversal' Model (Record a) -> Proxy nom -> AffineTraversal' Model b
afTrP tr nom = cloneAffineTraversal tr <<< prop nom

varPrism ::
  ∀ nom e r1 r2.
  Cons nom e r1 r2 =>
  IsSymbol nom =>
  Proxy nom ->
  Prism' (Var r2) e
varPrism nom = prism' (injV nom) (prjV nom)
