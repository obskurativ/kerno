module Medio where

import Affjax.RequestBody (json) as Affj.Pet
import Affjax.RequestHeader (RequestHeader(..)) as Affj.Pet
import Affjax.ResponseFormat as Affj.Resp
import Affjax.Web as Affj
import Control.Alternative (class Alternative, class Plus)
import Control.Monad.Maybe.Trans (MaybeT)
import Control.Plus (empty)
import Data.Argonaut (class DecodeJson, class EncodeJson, Json, JsonDecodeError(..), decodeJson, encodeJson, fromString, jsonEmptyObject, printJsonDecodeError, (.:))
import Data.Argonaut.Encode.Combinators ((:=), (~>))
import Data.Array as A
import Data.Either (Either(..))
import Data.Eq (class Eq, eq)
import Data.Foldable (class Foldable, foldM, foldMap, foldl, foldr)
import Data.HTTP.Method (Method(..))
import Data.HashMap as HM
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype)
import Data.String (Pattern(..), Replacement(..), replaceAll, trim)
import Data.String.CodeUnits (toCharArray)
import Data.Symbol (class IsSymbol, reflectSymbol)
import Data.Traversable (class Traversable, sequence, traverse)
import Data.Tuple (Tuple(..))
import Data.Variant (Variant, case_, inj, on, prj)
import Effect (Effect)
import Effect.Aff (Milliseconds(..), delay)
import Effect.Aff.Class (class MonadAff, liftAff)
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Class.Console (log)
import Flame (Html)
import Flame.Html.Attribute as HA
import Flame.Html.Element as HE
import Flame.Types (NodeData)
import Foreign.Object as F
import Prelude (class Functor, class Show, Unit, bind, identity, map, otherwise, pure, show, ($), (*), (*>), (+), (-), (<#>), (<$>), (<<<), (<=), (<>), (==), (>>=))
import Prim.Row as R
import Prim.RowList (class RowToList, RowList)
import Prim.RowList as RL
import Record (insert)
import Type.Proxy (Proxy(..))
import Unsafe.Coerce (unsafeCoerce)
import Web.HTML (Location, window)
import Web.HTML.Window (localStorage, location)
import Web.Storage.Storage (getItem, removeItem, setItem)

type Lingv
  = HM.HashMap String Json

newtype Konservata
  = Konservata String

instance eqKonservata :: Eq Konservata where
  eq (Konservata a) (Konservata b) = eq a b

akirK :: ∀ m. MonadEffect m => Konservata -> m (Maybe String)
akirK (Konservata k) = liftEffect $ window >>= localStorage >>= getItem k

metK :: ∀ m. MonadEffect m => String -> Konservata -> m Unit
metK v (Konservata k) = liftEffect $ window >>= localStorage >>= setItem k v

forigK :: ∀ m. MonadEffect m => Konservata -> m Unit
forigK (Konservata k) = liftEffect $ window >>= localStorage >>= removeItem k

lastVizK :: Konservata
lastVizK = Konservata "last_visit"

jxetonK :: Konservata
jxetonK = Konservata "token"

stilK :: Konservata
stilK = Konservata "stil"

erari :: ∀ m. MonadEffect m => String -> m Unit
erari = log

map2 :: ∀ f g a b. Functor f => Functor g => (a -> b) -> f (g a) -> f (g b)
map2 f x = ((f <$> _) <$> x)

infixl 4 map2 as <<$>>

newtype JsonHM v
  = JsonHM (HM.HashMap String v)

derive instance newtypeJsonHM :: Newtype (JsonHM v) _

instance encodeJsonHM :: EncodeJson v => EncodeJson (JsonHM v) where
  encodeJson (JsonHM v) = do
    encodeJson $ F.fromFoldable $ HM.toArrayBy Tuple v

instance decodeJsonHM :: DecodeJson v => DecodeJson (JsonHM v) where
  decodeJson json = do
    decodeJson json
      <#> \(obj :: F.Object v) ->
          JsonHM $ HM.fromFoldableWithIndex obj

instance eqJsonHM :: Eq (HM.HashMap String v) => Eq (JsonHM v) where
  eq (JsonHM a) (JsonHM b) = eq a b

instance showJsonHM :: Show a => Show (JsonHM a) where
  show (JsonHM a) = show a

instance functorJsonHM :: Functor JsonHM where
  map f (JsonHM x) = JsonHM (f <$> x)

instance foldJsonHM :: Foldable JsonHM where
  foldr f a (JsonHM x) = foldr f a x
  foldl f a (JsonHM x) = foldl f a x
  foldMap f (JsonHM x) = foldMap f x

instance travJsonHM :: Traversable JsonHM where
  traverse f (JsonHM x) = JsonHM <$> traverse f x
  sequence (JsonHM x) = JsonHM <$> sequence x

class JsonVar (rl :: RowList Type) (r :: Row Type) | rl -> r where
  decodeJsonVar ::
    Proxy rl ->
    String ->
    Json ->
    Maybe (Either JsonDecodeError (Variant r))
  encodeJsonVar ::
    Proxy rl ->
    Variant r ->
    Json

instance jsonVar1 :: JsonVar RL.Nil () where
  decodeJsonVar _ _ _ = Nothing
  encodeJsonVar _ = case_

instance jsonVar2 ::
  (R.Cons sym v r' r, IsSymbol sym, DecodeJson v, EncodeJson v, JsonVar rlr r') =>
  JsonVar (RL.Cons sym v rlr) r where
  decodeJsonVar _ etiked json =
    if reflectSymbol (et_ :: _ sym) == etiked then
      Just $ inj (et_ :: _ sym) <$> decodeJson json
    else
      exp <<$>> decodeJsonVar (et_ :: _ rlr) etiked json
    where
    exp :: Variant r' -> Variant r
    exp = unsafeCoerce
  encodeJsonVar _ =
    on
      (et_ :: _ sym)
      ( \v ->
          "tag" := reflectSymbol (et_ :: _ sym)
            ~> "value"
            := v
            ~> jsonEmptyObject
      )
      (encodeJsonVar (et_ :: _ rlr))

newtype Var r
  = Var (Variant r)

instance encodeJsonVar1 :: (RowToList r rl, JsonVar rl r) => DecodeJson (Var r) where
  decodeJson json = do
    obj <- decodeJson json
    etiked <- obj .: "tag"
    en <- obj .: "value"
    case decodeJsonVar (et_ :: _ rl) etiked en of
      Nothing -> Left $ AtKey "tag" $ UnexpectedValue $ fromString etiked
      Just x -> Var <$> x

instance encodeJsonVar2 :: (RowToList r rl, JsonVar rl r) => EncodeJson (Var r) where
  encodeJson (Var x) = encodeJsonVar (et_ :: _ rl) x

instance showVar :: Show (Variant r) => Show (Var r) where
  show (Var x) = show x

instance eqVar :: Eq (Variant r) => Eq (Var r) where
  eq (Var a) (Var b) = eq a b

type Erar
  = ( networkError :: UnitResp
    , notFound :: UnitResp
    , internalError :: UnitResp
    , invalidEmail :: UnitResp
    , invalidCode :: UnitResp
    , missingField :: String
    , parseError :: String
    , fieldParseError :: String
    , nonUniqueName :: UnitResp
    , noAuth :: UnitResp
    , limitNotSatisfied :: String
    , profileFilled :: UnitResp
    )

data ApiRespond a b
  = ERROR a
  | OK b

instance decodeJsonApi :: (DecodeJson a, DecodeJson b) => DecodeJson (ApiRespond a b) where
  decodeJson json = do
    x <- decodeJson json
    etiked <- x .: "tag"
    case etiked of
      "OK" -> OK <$> (x .: "value")
      "ERROR" -> ERROR <$> (x .: "value")
      _ -> Left $ AtKey "tag" $ UnexpectedValue $ fromString etiked

-- Farende: reprovi nur je servila fiasko
petKernSenAux ::
  ∀ v m r.
  EncodeJson v =>
  MonadAff m =>
  DecodeJson r =>
  Affj.URL ->
  v ->
  m (Either (Var Erar) r)
petKernSenAux url en =
  let
    jsonEn = encodeJson en

    pruv =
      liftAff
        $ Affj.request
        $ Affj.defaultRequest
            { method = Left POST
            , responseFormat = Affj.Resp.json
            , headers = [ Affj.Pet.RequestHeader "Obs-Apl" "1" ]
            , url = url
            , content = Just $ Affj.Pet.json jsonEn
            }

    f n = do
      let
        f' e
          | n <= 1 = (erari e) *> pure (Left $ injV (Proxy :: _ "networkError") [])
          | otherwise = liftAff (delay $ Milliseconds 500.0) *> f (n - 1)
      r <- pruv
      case r of
        Left x -> f' $ Affj.printError x
        Right x -> case decodeJson x.body of
          Left e -> f' $ printJsonDecodeError e
          Right (ERROR e) -> pure $ Left e
          Right (OK x') -> pure $ Right x'
  in
    f 3

petKernAux ::
  ∀ v m r.
  EncodeJson (Record ( token :: String | v )) =>
  R.Lacks "token" v =>
  MonadAff m =>
  DecodeJson r =>
  Affj.URL ->
  Record v ->
  m (Either (Var Erar) r)
petKernAux url v = do
  jxetonM <- akirK jxetonK
  case jxetonM of
    Just jxeton ->
      petKernSenAux url
        (insert (Proxy :: _ "token") jxeton v :: Record ( token :: String | v ))
    Nothing -> pure $ Left $ injV (Proxy :: _ "noAuth") []

type UnitResp
  = Array Unit

alNom :: String -> Maybe Int
alNom =
  foldM
    ( \a lit -> ado
        cif <- lit `A.elemIndex` toCharArray "0123456789"
        in a * 10 + cif
    )
    0
    <<< toCharArray

simbol :: ∀ msg. Array (NodeData msg) -> String -> Html msg
simbol ald v = HE.div ([ HA.class' "material-symbols-outlined" ] <> ald) [ HE.text v ]

-- | Malplenigas konservujon okaze de `false`
pord :: ∀ f a. Plus f => Boolean -> f a -> f a
pord = case _ of
  true -> identity
  false -> \_ -> empty

btn :: ∀ msg. Maybe msg -> Array (NodeData msg)
btn x =
  [ HA.class' "btn"
  , case x of
      Just msg -> HA.onClick msg
      Nothing -> HA.class' "malaktiv"
  ]

malpak :: ∀ m e r. MonadEffect m => Show e => Either e r -> MaybeT m r
malpak = case _ of
  Left e -> erari (show e) *> empty
  Right r -> pure r

nunLok :: forall m. MonadEffect m => m Location
nunLok = liftEffect $ window >>= location

et_ :: ∀ nom. Proxy nom
et_ = Proxy

estas :: ∀ nom a r1 r2. IsSymbol nom => R.Cons nom a r1 r2 => Var r2 -> Proxy nom -> Boolean
estas var e = onV e (\_ -> true) (\_ -> false) var

injV :: forall sym a r1 r2. R.Cons sym a r1 r2 => IsSymbol sym => Proxy sym -> a -> Var r2
injV a b = Var $ inj a b

onV :: forall sym a b r1 r2. R.Cons sym a r1 r2 => IsSymbol sym => Proxy sym -> (a -> b) -> (Var r1 -> b) -> Var r2 -> b
onV a b f (Var d) = on a b (f <<< Var) d

prjV :: forall sym a r1 r2 f. R.Cons sym a r1 r2 => IsSymbol sym => Alternative f => Proxy sym -> Var r2 -> f a
prjV a (Var b) = prj a b

caseV :: forall a. Var () -> a
caseV = case_ <<< \(Var x) -> x

-- | 'Input' kampo
enFiltr :: String -> String
enFiltr =
  let
    anst r = replaceAll (Pattern r) (Replacement " ")
  in
    anst "  " <<< anst " " <<< trim

foreign import data AtendElektRealig :: Type

foreign import atendElektRealig :: AtendElektRealig

-- Sistemo, prilaboranta dinamikaj partoj de paĝo (ekzemple, relarĝigas kampojn se bezonate)
foreign import dinamika :: Effect Unit
