module Pagx.Hered.Tipoj where

import Prelude
import Data.Argonaut (class DecodeJson, class EncodeJson, decodeJson, encodeJson)
import Data.Argonaut.Decode.Generic (genericDecodeJson)
import Data.Argonaut.Encode.Generic (genericEncodeJson)
import Data.Array as A
import Data.Array.NonEmpty.Internal (NonEmptyArray)
import Data.Either (Either)
import Data.Eq.Generic (genericEq)
import Data.Foldable (foldl)
import Data.Generic.Rep (class Generic)
import Data.HashMap as HM
import Data.Lazy as DL
import Data.List.Lazy (List(..), Step(..))
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.Newtype (class Newtype)
import Data.Show.Generic (genericShow)
import Data.String.CodeUnits (toCharArray)
import Data.Tuple.Nested (type (/\), (/\))
import Medio (JsonHM, Var)
import Pagx.Hered.ObsceneWordsList as OWL

data TekstStil
  = Kursiv
  | Dik
  | Sublin
  | Trastrek

data Arangx'
  = Liv
  | Mez
  | Dek
  | Alkadr

type Arangx
  = { flos :: Boolean, arangx :: Arangx' }

data KonsujInf
  = Div { lin :: Boolean } (JsonHM (Array TekstObj))
  | Cit
  | TekstLargx String
  | TekstKolor String
  | Arangx Arangx
  | KunStil TekstStil
  | SeEt { and :: Array String, not :: Array String }

-- Uzo de `data` ĉi tie senfincikligas encodeJson de PaĝPart.
data TabelEl
  = TabelEl Int Boolean (Maybe Arangx) (Array PagxPart)

type PagxInfIndeks
  = Int

data TekstObj
  = Tekst String
  | Anst String (Maybe (Array PagxPart))
  | Param String (Maybe String)

data PagxPart
  = Fiask String
  | TekstObj TekstObj
  | Konsuj KonsujInf (Array PagxPart)
  | ListPagx PagxInfIndeks
  | Titol PagxInfIndeks
  | Bild (Maybe Arangx) (Array TekstObj) (JsonHM (Array TekstObj))
  | Tabel (Array (Array TabelEl))
  | SupSub { sup :: Array PagxPart, sub :: Array PagxPart }
  | Stilar String
  | Ligil { cel :: Var ( plen :: String, space :: String ), nom :: Array PagxPart }
  | Subpagx PagxInfIndeks
  | Dat Int (Maybe String)
  | Libro
    PagxInfIndeks
    (Array { nom :: Array PagxPart, enhav :: Array PagxPart })
  | Hr

frTekst :: String -> PagxPart
frTekst = TekstObj <<< Tekst

data UnikId
  = UnikId Int (List Char)

data TempAmpl
  = ALast Int
  | AAntau Int
  | AMalpli Int
  | APli Int
  | AInter Int Int

type PagxInfKon
  = { piednot :: Array (Array PagxPart)
    , subtitol :: Array { nivel :: Int, enhav :: Array PagxPart }
    , listPagx :: Array ListPagx'
    , subpagx :: Array { wikidotFont :: Maybe String, voj :: Array String, anst :: JsonHM (Array PagxPart) }
    , subvoj :: Array Int
    }

newtype PagxInf
  = PagxInf
  { subtitol :: Array { nivel :: Int, id :: UnikId, enhav :: Array PagxPart }
  , piednot :: Array (Array PagxPart)
  , listPagx :: Array (Maybe (Array PagxPart /\ PagxInf) /\ ListPagx')
  , subpagx :: Array (Either String (Maybe (Array PagxPart /\ PagxInf)))
  , subvoj :: Array Int
  }

type SubPagxSxablon
  = { ant :: Array PagxPart, ripet :: Array PagxPart, post :: Array PagxPart }

-- FARENDE: Renomi ListPagxSxablon!
newtype ListPagx'
  = ListPagx'
  { ald ::
      { category :: Maybe String
      , tags ::
          Maybe
            { or :: Maybe (NonEmptyArray String)
            , and :: Array String
            , not :: Array String
            }
      , created_by :: Maybe String
      , created_at :: Maybe TempAmpl
      , updated_at :: Maybe TempAmpl
      , order :: Maybe { by :: String, asc :: Boolean }
      , offset :: Maybe Int
      , limit :: Maybe Int
      }
  , sxablon ::
      { ant :: Array PagxPart, ripet :: Array PagxPart, post :: Array PagxPart }
        /\ PagxInfKon
  }

type PagxVoj
  = { spac :: String, voj :: Array String }

-- <mi malamegas min>
derive instance newtypePagxInf :: Newtype PagxInf _

derive instance genericPagxInf :: Generic PagxInf _

instance encodePagxInf :: EncodeJson PagxInf where
  encodeJson a = genericEncodeJson a

instance decodePagxInf :: DecodeJson PagxInf where
  decodeJson a = genericDecodeJson a

derive instance genericListPagx' :: Generic ListPagx' _

instance encodeListPagx' :: EncodeJson ListPagx' where
  encodeJson a = genericEncodeJson a

instance decodeListPagx' :: DecodeJson ListPagx' where
  decodeJson a = genericDecodeJson a

derive instance genericTekstStil :: Generic TekstStil _

instance encodeTekstStil :: EncodeJson TekstStil where
  encodeJson = genericEncodeJson

instance decodeTekstStil :: DecodeJson TekstStil where
  decodeJson = genericDecodeJson

instance eqTekstStil :: Eq TekstStil where
  eq = genericEq

derive instance genericArangx' :: Generic Arangx' _

instance encodeArangx' :: EncodeJson Arangx' where
  encodeJson = genericEncodeJson

instance decodeArangx' :: DecodeJson Arangx' where
  decodeJson = genericDecodeJson

derive instance genericTempAmpl :: Generic TempAmpl _

derive instance genericKonsujInf :: Generic KonsujInf _

derive instance genericTabelEl :: Generic TabelEl _

derive instance genericTekstObj :: Generic TekstObj _

derive instance genericPagxPart :: Generic PagxPart _

instance encodeKonsujInf :: EncodeJson KonsujInf where
  encodeJson = genericEncodeJson

instance encodeTabelEl :: EncodeJson TabelEl where
  encodeJson a = genericEncodeJson a

instance encodeTekstObj :: EncodeJson TekstObj where
  encodeJson a = genericEncodeJson a

instance encodePagxPart :: EncodeJson PagxPart where
  encodeJson a = genericEncodeJson a

instance encodeUnikId :: EncodeJson UnikId where
  encodeJson (UnikId id _) = encodeJson id

instance encodeTempAmpl :: EncodeJson TempAmpl where
  encodeJson a = genericEncodeJson a

instance decodeKonsujInf :: DecodeJson KonsujInf where
  decodeJson = genericDecodeJson

instance eqKonsujInf :: Eq KonsujInf where
  eq = genericEq

instance decodeTabelEl :: DecodeJson TabelEl where
  decodeJson a = genericDecodeJson a

instance decodeTekstObj :: DecodeJson TekstObj where
  decodeJson a = genericDecodeJson a

instance decodePagxPart :: DecodeJson PagxPart where
  decodeJson a = genericDecodeJson a

instance decodeUnikId :: DecodeJson UnikId where
  decodeJson id = krId <$> decodeJson id

instance decodeTempAmpl :: DecodeJson TempAmpl where
  decodeJson a = genericDecodeJson a

instance eqTabelEl :: Eq TabelEl where
  eq = genericEq

instance eqArangx' :: Eq Arangx' where
  eq = genericEq

instance eqTekstObj :: Eq TekstObj where
  eq = genericEq

instance eqPagxPart :: Eq PagxPart where
  eq a = genericEq a

instance showTekstObj :: Show TekstObj where
  show a = genericShow a

instance showPagxPart :: Show PagxPart where
  show a = genericShow a

instance showKonsujInf :: Show KonsujInf where
  show a = genericShow a

instance showArangx' :: Show Arangx' where
  show a = genericShow a

instance showTabelEl :: Show TabelEl where
  show a = genericShow a

instance showTekstStil :: Show TekstStil where
  show a = genericShow a

-- </mi malamegas min>
data ForbarArb
  = Forbar
  | ForbarMult (HM.HashMap Char ForbarArb)

instance forbarSemi :: Semigroup ForbarArb where
  append aM bM = case aM /\ bM of
    ForbarMult a /\ ForbarMult b -> ForbarMult $ a <> b
    _ -> Forbar

forbarUn :: Array Char -> ForbarArb
forbarUn x = case A.uncons x of
  Nothing -> Forbar
  Just { head, tail } -> ForbarMult $ HM.singleton head (forbarUn tail)

aldArb :: ForbarArb -> Array Char -> ForbarArb
aldArb Forbar _ = Forbar

aldArb (ForbarMult arb) x = case A.uncons x of
  Nothing -> Forbar
  Just { head, tail } -> ForbarMult $ aldArbMult head tail arb

aldArbMult :: Char -> Array Char -> HM.HashMap Char ForbarArb -> HM.HashMap Char ForbarArb
aldArbMult kap fin arb =
  HM.alter
    ( Just
        <<< case _ of
            Nothing -> forbarUn fin
            Just v -> aldArb v fin
    )
    kap
    arb

malpermId :: HM.HashMap Char ForbarArb
malpermId =
  foldl (\kon { head, tail } -> aldArbMult head tail kon) HM.empty
    $ A.catMaybes
    $ A.uncons
    <<< toCharArray
    <$> OWL.cxiuj

krId :: Int -> UnikId
krId = \id -> UnikId id (krId' (Nothing /\ Nothing) HM.empty id)
  where
  varj :: HM.HashMap Char ForbarArb -> Array (Char /\ HM.HashMap Char ForbarArb)
  varj arb = do
    lit <- toCharArray "abcdefghijklmnoprstuvz"
    case HM.lookup lit arb of
      Just Forbar -> []
      Just (ForbarMult mult) -> [ lit /\ mult ]
      Nothing -> [ lit /\ HM.empty ]

  alArb :: String -> HM.HashMap Char ForbarArb
  alArb = HM.fromFoldableBy identity (\_ -> Forbar) <<< toCharArray

  vokal = alArb "aeiou"

  kons = alArb "bcdfghjklmnprstvz"

  voch = alArb $ "bdgjlmnrtvz" <> "c"

  nevoch = alArb "fhkps"

  -- Se vokalo au konsonanto ripetas dufoje, ni malpermesas uzon de ĝi.
  forbarRipet :: Maybe Char /\ Maybe Char -> HM.HashMap Char ForbarArb
  forbarRipet (aM /\ bM) =
    fromMaybe HM.empty do
      a <- aM
      b <- bM
      let
        f m = a `HM.member` m && b `HM.member` m
      case unit of
        _
          | f vokal -> Just vokal
        _
          | f kons -> Just kons
        _ -> Nothing

  forbarPost :: Maybe Char -> HM.HashMap Char ForbarArb
  forbarPost =
    maybe HM.empty \l ->
      ( case unit of
          _
            | l `HM.member` voch -> (kons <> _)
          _
            | l `HM.member` nevoch -> (voch <> _)
          _ -> identity
      )
        $ HM.singleton l Forbar

  krId' :: Maybe Char /\ Maybe Char -> HM.HashMap Char ForbarArb -> Int -> List Char
  krId' ant@(_ /\ ant2) arb kod =
    List
      $ DL.defer \_ ->
          if kod == 0 then
            Nil
          else
            let
              eblaj = varj (arb <> forbarRipet ant <> forbarPost ant2 <> malpermId)

              eblajL = A.length eblaj
            in
              case A.index eblaj (kod `mod` eblajL) of
                Nothing -> Nil
                Just (elektit /\ novMalperm) -> Cons elektit $ krId' (ant2 /\ Just elektit) novMalperm (kod `div` eblajL)
