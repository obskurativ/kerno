export function kopii(v) {
  return function() {
    navigator.clipboard.writeText(v);
  };
}

/* Taken from https://github.com/garyb/purescript-debug/ which is licensed under MIT */
export const akirNunMs = (function () {
  var perf;
  if (typeof performance !== "undefined") {
    // In browsers, `performance` is available in the global context
    perf = performance;
  }
  return (function() { return (perf || Date).now(); });
})();

export function tempDif(a) {
  return function(b) {
    return b - a;
  };
}

export function rulumAlTitolId(atendElekt) {
  return function(behavior) {
    return function(id) {
      return function() {
        atendElekt(`[data-title-id="${id}"]`).then(function(x) {
          x.scrollIntoView({ behavior });
        });
      };
    };
  };
}

export function amplJarMon(jar) {
  return function(mon) {
    let d = new Date(jar + "-" + mon);
    return { kom: Math.floor(d/1000), fin: Math.floor(d.setMonth(d.getMonth()+1)/1000) };
  }
}

export function amplJar(jar) {
  let d = new Date(jar);
  return { kom: Math.floor(d/1000), fin: Math.floor(d.setYear(d.getYear()+1)/1000) };
}

export function akirNun() {
  return Math.floor(new Date()/1000);
}
