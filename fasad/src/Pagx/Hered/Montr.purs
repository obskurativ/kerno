module Pagx.Hered.Montr where

import Prelude
import Control.Monad.Maybe.Trans (runMaybeT)
import Control.Monad.Writer (runWriter, tell)
import Data.Array (all, elem, fold)
import Data.Array as A
import Data.Either (Either(..))
import Data.Foldable (for_, intercalate)
import Data.Function.Uncurried (Fn5, runFn5)
import Data.HashMap as HM
import Data.Lazy (force)
import Data.Lens (_2, _Just)
import Data.Lens.AffineTraversal (cloneAffineTraversal)
import Data.Lens.Record (prop)
import Data.Lens.Setter ((%~))
import Data.List (List(..))
import Data.Maybe (Maybe(..), fromMaybe, isJust, maybe)
import Data.Monoid (guard)
import Data.Newtype (over2, unwrap)
import Data.String (joinWith, toLower)
import Data.String.CodeUnits as SCU
import Data.String.Utils (startsWith)
import Data.Traversable (traverse)
import Data.Tuple.Nested ((/\), type (/\))
import Datum (GxisKuntKerna(..), Knt(..), Mondo, Msg(..), RegPagx, RegPagxEnhav, RegPagxRedakt, de, gxis, kaz, kntFokus, longGxis, var, varPrism)
import Effect (Effect)
import Effect.Aff (Milliseconds(..), delay)
import Effect.Aff.Class (liftAff)
import Effect.Class (liftEffect)
import Flame (Html)
import Flame.Html.Attribute as HA
import Flame.Html.Element as HE
import Flame.Types (NodeData)
import Medio (AtendElektRealig, JsonHM(..), UnitResp, atendElektRealig, btn, caseV, enFiltr, et_, injV, malpak, metK, nunLok, onV, petKernAux, petKernSenAux, pord, simbol, stilK)
import Pagx.Hered.Analiz (analizPagxMez, cxuStila, kolektTekst, kopii, mallongUnikId, malplenPagxInfKon, postprocezKernaPagx)
import Pagx.Hered.Tipoj (Arangx'(..), KonsujInf(..), PagxInf(..), PagxPart(..), TabelEl(..), TekstObj(..), TekstStil(..), frTekst)
import Voj (cxesSxarg, sligil)
import Web.Event.Internal.Types (Event)
import Web.HTML (window)
import Web.HTML.HTMLDocument (setTitle)
import Web.HTML.Location (origin)
import Web.HTML.Window (document)

sxprucErar ::
  Mondo ->
  Array (Html Msg) ->
  String ->
  Html Msg
sxprucErar mond erarMark erar =
  HE.div [ HA.class' "meze" ]
    [ HE.div [ HA.class' "blok", HA.id "erar-pagx" ]
        ( [ HE.div [ HA.id "erar-mark" ] erarMark
          , HE.text $ mond.trd erar
          ]
            <> pure
                ( HE.div [ HA.id "reen" ] $ ([ "/" ] <> A.catMaybes [ mond.lastViz ])
                    <#> \x ->
                        [ simbol [] "subdirectory_arrow_right"
                        , sligil mond { nom: [ HE.text x ], voj: x }
                        ]
                )
        )
    ]

redaktBtn :: Knt RegPagx -> Html Msg
redaktBtn (Knt { val, tr }) =
  simbol
    ( [ HA.id "redakt-btn" ]
        <> btn
            ( pord val.redaktebla
                $ onV (et_ :: _ "montr")
                    ( \enhav ->
                        Just
                          $ Gxis \gxisKnt@{ revid } -> do
                              let
                                enhav' = fromMaybe nulPagx enhav
                              cxesSxarg gxisKnt
                              revid
                                $ cloneAffineTraversal tr
                                %~ _
                                    { regxim =
                                      injV (et_ :: _ "redakt")
                                        { antEnhav: enhav
                                        , antmontr: false
                                        , enhav: fromMaybe nulPagx enhav
                                        , etikedUzoj: JsonHM HM.empty
                                        , etikedSercx: Nothing
                                        }
                                    }
                              uzojM <-
                                runMaybeT
                                  $ malpak
                                  =<< petKernSenAux ("/" <> val.spac <> "/pagx/etiked/uzoj")
                                      { tags: enhav'.etikedoj }
                              for_ uzojM \uzoj ->
                                revid $ cloneAffineTraversal tr <<< prop (et_ :: _ "regxim") <<< varPrism (et_ :: _ "redakt")
                                  %~ \v -> v { etikedUzoj = over2 JsonHM HM.union v.etikedUzoj uzoj }
                    )
                    (\_ -> Nothing)
                    val.regxim
            )
    )
    "edit"
  where
  nulPagx :: RegPagxEnhav
  nulPagx =
    { titol: ""
    , etikedoj: []
    , kod: ""
    , analizit:
        ( []
            /\ PagxInf
                { subtitol: []
                , piednot: []
                , listPagx: []
                , subpagx: []
                , subvoj: []
                }
        )
    }

display :: Boolean -> NodeData Msg
display x = HA.styleAttr $ "display: " <> if x then "block" else "none"

montrPagx :: Knt RegPagx -> { rulum :: Array (Html Msg), pagx :: Array (Html Msg) }
montrPagx knt@(Knt { mond }) =
  kaz (knt `kntFokus` (et_ :: _ "regxim"))
    ( de
        # var (et_ :: _ "montr")
            ( \(Knt m) ->
                { pagx: []
                , rulum:
                    case m.val of
                      Nothing ->
                        [ sxprucErar
                            mond
                            [ simbol [ HA.id "sercx-mark" ] "search"
                            , simbol [ HA.id "sercxat-mark" ] "question_mark"
                            , HE.div [ HA.id "erar404" ] "404"
                            ]
                            "erar.404"
                        ]
                      Just x -> [ HE.article_ $ montrPagxEnhav knt x ]
                }
            )
        # var (et_ :: _ "redakt") (redaktPagx knt)
    )

montrPagxEnhav :: Knt RegPagx -> RegPagxEnhav -> Array (Html Msg)
montrPagxEnhav knt enhav =
  pord (enhav.titol /= "") [ HE.h1 "titol" [ HE.text enhav.titol, HE.hr ] ]
    <> montrPagxArb { knt, etj: enhav.etikedoj } enhav.analizit
    <> [ montrEt $ HE.div [ HA.class' "etiked" ] <$> enhav.etikedoj ]

-- FARENDE: Forigi?
montrEt :: Array (Html Msg) -> Html Msg
montrEt en = HE.div [ HA.id "fin" ] $ [ HE.hr, HE.div "etikedoj" en ]

foreign import strftime :: Fn5 Int String (Array String) (Array String) (Array String) String

-- Bloku ligilojn!
montrPagxArb ::
  { knt :: Knt RegPagx, etj :: Array String } ->
  Array PagxPart /\ PagxInf ->
  Array (Html Msg)
montrPagxArb kernKnt@{knt: Knt { val: { spac, voj }, mond } } (pagxArb /\ PagxInf pagxInf) = montr' <$> pagxArb
  where
  fiaskA er = [ HA.class' "analiz-fiask", HA.createAttribute "data-debug-errors" $ intercalate "; " er ]
  korupt obj = HE.div (fiaskA [ "METADATA-CORRUPTION" ]) [ HE.text obj ]

  arangx v =
    HA.class'
      <$> guard v.flos [ "float" ]
      <> pure
          ( case v.arangx of
              Liv -> "left"
              Mez -> "center"
              Dek -> "right"
              Alkadr -> "justify"
          )

  legPropj :: JsonHM (Array TekstObj) -> _
  legPropj (JsonHM argKrudT) =
    let
      (rez /\ er) =
        runWriter do
          let
            alAMap =
              HM.fromArray
                [ "id" /\ (\x -> HA.id if SCU.slice 0 2 x == "u-" then x else "u-" <> x)
                , "class" /\ HA.class'
                , "style" /\ HA.styleAttr
                ]
          argKrudS <- traverse kolektTekst argKrudT
          for_ (HM.keys $ HM.difference argKrudS alAMap) \nekonat -> tell [ "unknown " <> nekonat ]
          pure $ HM.values $ HM.intersectionWith (#) argKrudS alAMap
    in
      rez <> pord (not $ A.null er) (fiaskA er)

  kLin = HA.class' "lin"

  montr' :: PagxPart -> Html Msg
  montr' = case _ of
    Konsuj tip v ->
      ( HE.div
          $ case tip of
              Div { lin } argj -> legPropj argj <> pord lin [ kLin ]
              Cit -> [ HA.class' "cit" ]
              Arangx ar -> arangx ar
              TekstLargx largx -> [ kLin, HA.styleAttr $ "font-size: " <> largx ]
              TekstKolor kolor -> [ kLin, HA.styleAttr $ "color: " <> kolor ]
              KunStil stil ->
                A.singleton $ HA.class'
                  $ case stil of
                      Kursiv -> "italic"
                      Dik -> "bold"
                      Sublin -> "underline"
                      Trastrek -> "strikethrough"
              SeEt etj ->
                let
                  montru = all (_ `elem` kernKnt.etj) etj.and && all (not <<< (_ `elem` kernKnt.etj)) etj.not
                in
                  [ display montru ]
      )
        (montr' <$> v)
    TekstObj obj ->
      let
        (v /\ er) = runWriter $ kolektTekst [ obj ]
      in
        case er of
          [] -> HE.text v
          _ -> HE.span (fiaskA er) v
    Titol ind -> case pagxInf.subtitol `A.index` ind of
      Nothing -> korupt "titol"
      Just { nivel, enhav } ->
        ( [ simbol
              [ HA.class' "titlelink"
              , HA.onClick
                  $ Efekt
                  $ liftEffect do
                      or <- nunLok >>= origin
                      kopii $ or <> "/" <> spac <> "/" <> joinWith "/" voj <> "#"
                        <> mallongUnikId ind pagxInf.subtitol
              ]
              "link"
          , HE.span_ (montr' <$> enhav)
          ]
        )
          # ( ( case nivel of
                  1 -> HE.h1
                  2 -> HE.h2
                  3 -> HE.h3
                  4 -> HE.h4
                  5 -> HE.h5
                  _ -> HE.h6
              )
                [ HA.createAttribute "data-title-id" $ show ind ]
            )
    Bild arM font propj ->
      let
        ignorNeTekst = case _ of
          Tekst e -> e
          _ -> ""

        urlT = fold $ ignorNeTekst <$> font
      in
        HE.img
          ( maybe [] arangx arM
              <> [ HA.src urlT ]
              <> legPropj propj
          )
    Tabel lin ->
      HE.table_ $ lin
        <#> \kol ->
            HE.tr_ $ kol
              <#> \(TabelEl long kap arM val) ->
                  HE.td
                    ( [ HA.createAttribute "colspan" (show long) ]
                        <> (if kap then [ HA.class' "table-head" ] else [])
                        <> (maybe [] arangx arM)
                    )
                    (montr' <$> val)
    SupSub { sup, sub } ->
      HE.div [ HA.class' "supsub" ]
        [ HE.div_ (montr' <$> sup)
        , HE.div_ (montr' <$> sub)
        ]
    Stilar v -> HE.style' [ HA.innerHtml v ]
    Ligil { cel, nom } ->
      let
        cel' =
          caseV
            # onV (et_ :: _ "plen") identity
            # onV (et_ :: _ "space") (("/" <> spac <> "/") <> _)
            $ cel

        avertu = startsWith "http://" cel'
      in
        HE.div [ kLin ]
          $ [ sligil mond
                { voj: cel'
                , nom: (montr' <$> nom)
                }
            ]
          <> pord avertu [ simbol [ HA.class' "malsekur", HA.title $ mond.trd "pagx.ligil.malsekur" ] "emergency_home" ]
    Hr -> HE.hr
    Fiask v -> HE.span (fiaskA []) v
    ListPagx ind -> case pagxInf.listPagx `A.index` ind of
      Just (Just val /\ _) -> HE.div_ (montrPagxArb kernKnt val)
      _ -> korupt "ListPages"
    Dat temp strukt ->
      let
        trdList nom lon =
          mond.trd' nom
            # fromMaybe ado
                post <- A.range 1 lon
                in nom <> "." <> show post
      in
        HE.text
          $ runFn5 strftime temp (fromMaybe "%a %d %b %Y %H:%M:%S" strukt)
              (trdList "pagx.temp.tagoj" 7)
              (trdList "pagx.temp.tagoj.mallong" 7)
              (trdList "pagx.temp.monatoj" 12)
    Subpagx ind -> case pagxInf.subpagx `A.index` ind of
      Just (Left e) -> HE.div (fiaskA [ "not-loaded " <> e ]) $ "include"
      Just (Right (Just val)) -> HE.div_ $ montrPagxArb kernKnt val
      _ -> korupt "include"
    Libro subvojInd ar -> fromMaybe (korupt "book") do
      elektInd <- pagxInf.subvoj `A.index` subvojInd
      { enhav } <- ar `A.index` elektInd
      pure $ HE.div
        [ HA.class' "libr" ]
        [ HE.div 
          [HA.class' "libr-foliar"] 
          (ar # A.mapWithIndex \ind { nom } ->
            HE.div
              (if elektInd == ind then [HA.class' "elektit"] else [])
              $ (montr' <$> nom) <> [HE.div' [HA.class' "elekt-areo"]])
        , HE.div [ HA.class' "libr-pagx" ] (montr' <$> enhav)
        ]

foreign import tekstDeCel :: Event -> Effect String

foreign import elektEtiked :: AtendElektRealig -> Int -> Effect Unit

redaktPagx :: Knt RegPagx -> Knt RegPagxRedakt -> { rulum :: Array (Html Msg), pagx :: Array (Html Msg) }
redaktPagx (Knt p@{ mond }) (Knt r) =
  { rulum:
      [ HE.article [ display cxuMontr ] $ montrPagxEnhav (Knt p) r.val.enhav
      , HE.div
          [ HA.id "redaktej", display (not cxuMontr) ]
          [ HE.h1 "titol"
              [ HE.input
                  [ HA.value r.val.enhav.titol
                  , HA.placeholder $ mond.trd "pagx.titol"
                  , HA.onInput \v ->
                      gxis r.tr
                        let
                          titol = enFiltr v
                        in
                          ado
                            liftEffect (window >>= document >>= setTitle titol)
                            in _ { enhav { titol = titol } }
                  ]
              , HE.div [ HA.class' "fantom" ] -- FARENDE: Uzu .relargx?
                  [ r.val.enhav.titol
                      # \t ->
                          if t == "" then
                            mond.trd "pagx.titol"
                          else
                            t
                  ]
              , HE.hr
              ]
          , HE.textarea'
              [ HA.class' "relargx-y"
              , HA.value r.val.enhav.kod
              , HA.onInput \v -> gxis r.tr $ pure $ _ { enhav { kod = v } }
              ]
          , montrEt
              $ ( A.mapWithIndex
                    (\a b -> HE.div [ HA.class' "etiked-lok" ] $ montrRedaktEtiked a b)
                    r.val.enhav.etikedoj
                )
              <> [ HE.div
                    [ HA.class' "etiked-plus"
                    , HA.onClick
                        $ gxis r.tr ado
                            -- Firefox havas problemojn pri tekstentajpado se
                            -- butono subite anstataiĝas per tekstkampo,
                            -- do ni permane reelektu kreitan etikedejon.
                            -- FARENDE: Plibonigi redaktadon de etikedoj?
                            liftEffect
                              $ elektEtiked atendElektRealig (A.length r.val.enhav.etikedoj)
                            in \m -> m { enhav { etikedoj = m.enhav.etikedoj `A.snoc` " " } }
                    ]
                    [ "+" ]
                ]
          ]
      ]
  , pagx:
      [ HE.div_
          [ HE.div "redakt-menu"
              [ simbol
                  [ HA.class' "redakt-nulig"
                  , HA.onClick
                      $ gxis p.tr
                      $ pure
                      $ _ { regxim = injV (et_ :: _ "montr") r.val.antEnhav }
                  ]
                  "close"
              , simbol
                  ( [ HA.class' "redakt-antmontr"
                    , HA.onClick
                        $ longGxis
                            r.tr \gknt -> do
                            let
                              novMontr =
                                if stila then
                                  false
                                else
                                  not cxuMontr

                              pretas = gknt.revid _ { antmontr = novMontr }
                            if stila || novMontr then do
                              kom /\ post <- reanalizF gknt.kerna
                              gknt.revid \x -> x { enhav = kom x.enhav }
                              pretas
                              post \f ->
                                gknt.revid
                                  $ prop (et_ :: _ "enhav")
                                  %~ f
                            else
                              pretas
                    ]
                      <> guard cxuMontr [ HA.class' "aktiv" ]
                  )
                  "preview"
              , simbol
                  [ HA.class' "redakt-bone"
                  , HA.onClick
                      $ longGxis
                          p.tr \gknt -> do
                          m :: Maybe UnitResp <-
                            runMaybeT
                              $ malpak
                              =<< petKernAux ("/" <> p.val.spac <> "/pagx/redakt")
                                  { path: p.val.voj
                                  , title: r.val.enhav.titol
                                  , content:
                                      { title: r.val.enhav.titol
                                      , text: r.val.enhav.kod
                                      , tags: r.val.enhav.etikedoj
                                      }
                                  }
                          when (isJust m) do
                            -- FARENDE: Erarmesaĝon kaze de fiasko
                            kom /\ post <- reanalizF gknt.kerna
                            gknt.revid _ { regxim = injV (et_ :: _ "montr") $ Just $ kom r.val.enhav }
                            post \f ->
                              gknt.revid
                                $ prop (et_ :: _ "regxim")
                                <<< varPrism (et_ :: _ "montr")
                                <<< _Just
                                %~ f
                  ]
                  "check"
              ]
          ]
      ]
  }
  where
  cxuMontr = r.val.antmontr

  stila = cxuStila p.val

  reanalizF (GxisKuntKerna mkntP) = do
    let
      mknt = force mkntP
    arb <-
      if stila then ado
        metK r.val.enhav.kod stilK
        in [ frTekst r.val.enhav.kod ] /\ malplenPagxInfKon
      else
        analizPagxMez r.val.enhav.kod
    let
      analizit /\ sxarg =
        postprocezKernaPagx
          { revidKasxm: \f -> mknt.revid (prop (et_ :: _ "pagxKasxm") %~ f)
          , kasxm: mknt.model.pagxKasxm
          , spac: p.val.spac
          }
          arb
          Nil
    pure
      ( _ { analizit = analizit }
          /\ \traktil -> sxarg \f -> traktil $ prop (et_ :: _ "analizit") <<< _2 %~ f
      )

  etikedLin aldonAgord et =
    [ HE.div ([ HA.class' "etiked-nom" ] <> aldonAgord)
        [ HE.lazy (Just et) (\_ -> HE.text et) unit ]
    , HE.div_ "("
    , HE.div [ HA.class' "etiked-uzoj" ] [ fromMaybe " " (show <$> HM.lookup et (unwrap r.val.etikedUzoj)) ]
    , HE.div_ ")"
    ]

  montrRedaktEtiked ind et =
    let
      elektit = case r.val.etikedSercx of
        Just { ind: ind', proponoj }
          | ind == ind' -> Just proponoj
        _ -> Nothing
    in
      HE.div
        ( [ HA.class' "etiked" ]
            <> pord (isJust elektit)
                [ HA.class' "elektit"
                , HA.onFocusout
                    $ gxis r.tr
                    $ liftAff (delay $ Milliseconds 100.0)
                    $> _ { etikedSercx = Nothing }
                ]
        )
        [ (flip etikedLin)
            et
            [ HA.contentEditable true
            , HA.createAttribute "data-etiked-id" (show ind)
            , HA.class' "etiked-nom"
            -- FARENDE: Enter
            , HA.onInput \v ->
                gxis r.tr do
                  resp :: Array { name :: _, uses :: _ } <-
                    malpak
                      =<< petKernSenAux
                          ("/" <> p.val.spac <> "/pagx/etiked/sercx")
                          { query: v }
                  let
                    uzoj = JsonHM $ HM.fromArrayBy (_.name) (_.uses) resp
                  pure \m ->
                    m
                      { etikedUzoj = over2 JsonHM HM.union m.etikedUzoj uzoj
                      , etikedSercx = Just { ind, proponoj: (_.name) <$> resp }
                      }
            , HA.onBlur' \ev ->
                gxis r.tr ado
                  val <- (toLower <<< enFiltr) <$> liftEffect (tekstDeCel ev)
                  in \m ->
                    let
                      oldEtj = m.enhav.etikedoj
                    in
                      m
                        { enhav
                          { etikedoj =
                            fromMaybe oldEtj
                              $ ( if val == "" then
                                    A.deleteAt ind
                                  else
                                    A.updateAt ind val
                                )
                                  oldEtj
                          }
                        }
            ]
            <> case elektit of
                Just proponoj ->
                  proponoj
                    >>= \propon ->
                        etikedLin
                          [ HA.class' "etiked-propon"
                          , HA.onClick
                              $ gxis r.tr
                              $ pure \m ->
                                  m
                                    { enhav
                                      { etikedoj =
                                        fromMaybe m.enhav.etikedoj
                                          $ A.updateAt ind propon m.enhav.etikedoj
                                      }
                                    }
                          ]
                          propon
                Nothing -> []
        ]
