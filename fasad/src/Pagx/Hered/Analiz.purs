module Pagx.Hered.Analiz where

import Control.Alternative (class Alternative, (<|>))
import Control.Lazy (fix)
import Control.Lazy as CL
import Control.Monad.List.Trans (ListT(..), Step(..), lift, uncons)
import Control.Monad.Maybe.Trans (runMaybeT)
import Control.Monad.Rec.Class (Step(..)) as R
import Control.Monad.Rec.Class (class MonadRec, tailRecM)
import Control.Monad.State (StateT(..), get, mapStateT, modify_, put, runState, runStateT)
import Control.Monad.Writer (Writer, execWriter, runWriter, tell)
import Data.Argonaut (Json, decodeJson, encodeJson, jsonParser, stringify)
import Data.Array (fold, many)
import Data.Array as A
import Data.Array.NonEmpty as NEA
import Data.Bounded (bottom)
import Data.Either (Either(..), hush)
import Data.Enum (succ)
import Data.Foldable (all, elem, foldl, for_, intercalate)
import Data.FoldableWithIndex (foldlWithIndex)
import Data.HashMap as HM
import Data.HashSet (HashSet, fromFoldable, member) as S
import Data.Hashable (hash)
import Data.Lazy as DL
import Data.Lens (_1, _2, _Just, _Right, (%~), (.~))
import Data.Lens.Index (ix)
import Data.Lens.Iso.Newtype (_Newtype)
import Data.Lens.Record (prop)
import Data.Lens.Zoom (zoom)
import Data.List (List, (:))
import Data.List as L
import Data.List.Lazy as LL
import Data.Maybe (Maybe(..), fromMaybe, isJust, maybe)
import Data.Monoid (guard)
import Data.Ord (abs)
import Data.Ord.Max (Max(..))
import Data.String (Pattern(..), Replacement(..), joinWith, replace, split, toLower, toUpper, trim)
import Data.String (take) as S
import Data.String.CodePoints (take)
import Data.String.CodeUnits (fromCharArray, splitAt, toCharArray)
import Data.String.CodeUnits as SCU
import Data.String.Utils (startsWith)
import Data.Symbol (class IsSymbol)
import Data.Traversable (for, traverse, traverse_)
import Data.Tuple (Tuple(..), fst, snd)
import Data.Tuple.Nested ((/\), type (/\))
import Datum (KasxmFresx(..), PagxEnhav, PagxKasxm)
import Debug (class DebugWarning, trace)
import Effect (Effect)
import Effect.Aff (Aff, Fiber, error, joinFiber, killFiber, launchAff)
import Effect.Aff.Class (class MonadAff, liftAff)
import Effect.Class (liftEffect)
import Effect.Class.Console (log)
import Medio (AtendElektRealig, JsonHM(..), Konservata(..), UnitResp, akirK, alNom, caseV, erari, et_, injV, malpak, metK, onV, petKernSenAux)
import Pagx.Hered.Tipoj (Arangx'(..), KonsujInf(..), ListPagx'(..), PagxInf(..), PagxInfKon, PagxPart(..), TabelEl(..), TekstObj(..), TekstStil(..), TempAmpl(..), UnikId(..), PagxVoj, frTekst, krId)
import Parsing (ParseState(..), ParserT, Position(..), mapParserT, runParser)
import Parsing (fail, getParserT, runParserT) as P
import Parsing.Combinators (lookAhead, try) as P
import Parsing.String (consumeWith, eof, satisfy, string) as P
import Prelude (class Applicative, class Apply, class Bind, class Eq, class Functor, class Monad, class Ord, Unit, apply, bind, compare, discard, flip, identity, join, map, mempty, negate, not, otherwise, pure, show, unit, void, when, (#), ($), ($>), (&&), (*), (*>), (+), (-), (/=), (<#>), (<$), (<$>), (<*), (<*>), (<<<), (<=), (<=<), (<>), (<@>), (=<<), (==), (>>=), (>>>), (||))
import Prim.Row (class Cons) as R
import Record as Record
import Type.Proxy (Proxy)

registr ::
  ∀ m a nom r r'.
  Monad m =>
  IsSymbol nom =>
  R.Cons nom (Array a) r r' =>
  Proxy nom -> a -> StateT (Record r') m Int
registr nom x =
  zoom (prop nom) do
    ar <- get
    let
      ind = A.length ar
    put $ A.snoc ar x
    pure ind

-- FARENDE: Sxprucmesaĝon por kopiado de ligiloj por titoloj
-- FARENDE: Array PagxPart en Ligilojn
-- FARENDE: TESTI akirTekst
-- FARENDE: GC por trairilo de arboj?
-- Trairi PagxPart arbon, ne tusxante ListPages/subpagxojn
malprofundTrairiArbon ::
  ∀ m kom fin.
  Monad m =>
  (kom /\ PagxInfKon) ->
  ( (PagxPart -> (Array PagxPart -> StateT PagxInfKon m (Array PagxPart)) -> StateT PagxInfKon m PagxPart) ->
    kom ->
    StateT PagxInfKon m fin
  ) ->
  m (fin /\ PagxInfKon)
malprofundTrairiArbon = \(origArb /\ origInf) trairil ->
  runStateT
    (trairil apr origArb)
    origInf
    <#> \(pagxArb /\ novInf) ->
        pagxArb /\ novInf
  where
  --apr :: PagxPart -> ?kor -> StateT _ m PagxPart
  apr part postf = case part of
    Fiask x -> pure $ Fiask x
    Konsuj inf enhav -> Konsuj inf <$> postf enhav
    ListPagx x -> pure $ ListPagx x
    TekstObj obj ->
      TekstObj
        <$> case obj of
            Tekst x -> pure $ Tekst x
            Param x y -> pure $ Param x y
            Anst x p -> Anst x <$> traverse postf p
    Titol oldInd -> do
      orig <- get
      novInd <- case A.index orig.subtitol oldInd of
        Just old -> do
          novEnhav <- postf old.enhav
          if novEnhav == old.enhav then
            registr (et_ :: _ "subtitol") $ old { enhav = novEnhav }
          else
            pure oldInd
        Nothing -> pure 0
      pure $ Titol novInd
    Bild a b c -> pure $ Bild a b c
    Tabel ar ->
      Tabel
        <$> traverse
            (traverse \(TabelEl a b c enhav) -> TabelEl a b c <$> postf enhav)
            ar
    SupSub { sup, sub } -> ado
      novSup <- postf sup
      novSub <- postf sub
      in SupSub { sup: novSup, sub: novSub }
    Stilar x -> pure $ Stilar x
    Ligil old -> ado
      novNom <- postf old.nom
      in Ligil $ old { nom = novNom }
    Hr -> pure Hr
    Subpagx ind -> pure $ Subpagx ind
    Dat x y -> pure $ Dat x y
    Libro ind ar -> ado
      ar' <-
        for ar \{ nom, enhav } ->
          { nom: _, enhav: _ } <$> postf nom <*> postf enhav
      in Libro ind ar'

kolektTekstObj :: Array PagxPart -> Array TekstObj
kolektTekstObj enhav =
  execWriter
    $ malprofundTrairiArbon (enhav /\ malplenPagxInfKon) \apr ->
        fix \si ->
          traverse
            $ case _ of
                TekstObj v -> tell [ v ] $> TekstObj v
                v -> apr v si

kolektTekst :: Array TekstObj -> Writer (Array String) String
kolektTekst =
  (fold <$> _)
    <<< traverse case _ of
        Tekst x -> pure x
        Param x y -> tell [ "unset %%" <> x <> maybe "" ("|" <> _) y <> "%%" ] $> ""
        Anst v _ -> tell [ "unset {$" <> v <> "}" ] $> ""

-- Zorge konservas ĉiujn erarojn kaj sendas ilin al
-- profesia subtena teamo.
-- Aŭ ne sendas. Ĉiuokaze, la rezulto estas sama.
forsendEr :: ∀ m a. Writer m a -> a
forsendEr = fst <<< runWriter

type AnalizPI
  = StateT PagxInfKon (ParserT String Mezuril)

data Tekster
  = LitEr Int Int -- Enhavas renversan! indeksojn do komenco kaj fino
  | FinEr PagxPart

foreign import data Temp :: Type

foreign import akirNunMs :: Effect Temp

foreign import tempDif :: Temp -> Temp -> Number

newtype Malsxpar
  = Malsxpar (HM.HashMap String Number)

class MonadTemp (m :: Type -> Type) where
  tempM' :: ∀ a. String -> m a -> m a

newtype Mezuril a
  = Mezuril (StateT Malsxpar Effect a)

malMezuril :: ∀ a. Mezuril a -> StateT Malsxpar Effect a
malMezuril (Mezuril x) = x

instance monadTemp :: MonadTemp Mezuril where
  tempM' etiked (Mezuril ag) =
    Mezuril do
      a <- liftEffect akirNunMs
      rez <- ag
      b <- liftEffect akirNunMs
      modify_ \(Malsxpar h) ->
        Malsxpar $ HM.insertWith (+) etiked (tempDif a b) h
      pure rez

instance functorMezuril :: Functor Mezuril where
  map f (Mezuril a) = Mezuril $ map f a

instance applyMezuril :: Apply Mezuril where
  apply (Mezuril f) (Mezuril a) = Mezuril $ apply f a

instance applicativeMezuril :: Applicative Mezuril where
  pure = Mezuril <<< pure

instance bindMezuril :: Bind Mezuril where
  bind (Mezuril v) f = Mezuril $ v >>= malMezuril <<< f

instance monadMezuril :: Monad Mezuril

instance monadRecMezuril :: MonadRec Mezuril where
  tailRecM f = Mezuril <<< tailRecM (malMezuril <<< f)

instance stateMonadTemp :: MonadTemp m => MonadTemp (StateT s m) where
  tempM' = mapStateT <<< tempM'

instance parserMonadTemp :: (MonadTemp m, MonadRec m) => MonadTemp (ParserT s m) where
  tempM' = mapParserT <<< tempM'

tempM :: ∀ m a. MonadTemp m => String -> m a -> m a
--tempM = tempM'
tempM _ v = v

class
  (Monad m, Alternative m) <= MonadAn m where
  anStat :: m (ParseState String)
  tekst :: String -> m Unit
  satig :: (Char -> Boolean) -> m Unit
  fiask :: ∀ a. m a
  atend :: ∀ a. m a -> m a
  prov :: ∀ a. m a -> m a
  eof :: m Unit
  konsumPer :: ∀ a. (String -> Either String { consumed :: String, remainder :: String, value :: a }) -> m a

instance monadAnAnaliz :: MonadAn (ParserT String m) where
  anStat = P.getParserT
  tekst = void <<< P.string
  satig = void <<< P.satisfy
  fiask = P.fail ""
  atend = P.lookAhead
  prov = P.try
  eof = P.eof
  konsumPer = P.consumeWith

instance monadAnState :: MonadAn m => MonadAn (StateT s m) where
  anStat = lift anStat
  tekst = lift <<< tekst
  satig = lift <<< satig
  fiask = lift fiask
  atend = mapStateT atend
  prov = mapStateT prov
  eof = lift eof
  konsumPer f = lift $ konsumPer f

konsumit :: ∀ m a. MonadAn m => m a -> m (String /\ a)
konsumit an = ado
  ParseState e1 _ _ <- anStat
  x <- an
  ParseState e2 _ _ <- anStat
  in Tuple (take (SCU.length e1 - SCU.length e2) e1) x

slosxilVort :: ∀ m. MonadAn m => String -> m Unit
slosxilVort kion =
  konsumPer \s ->
    let
      { before, after } = SCU.splitAt (SCU.length kion) s
    in
      if toLower before == kion then
        Right { consumed: before, remainder: after, value: unit }
      else
        Left ""

-- https://github.com/purescript-contrib/purescript-parsing/blob/v10.0.0/src/Parsing/Combinators.purs#L433-L433
tekstGxis :: ∀ m. MonadAn m => MonadRec m => m Boolean -> m String
tekstGxis fin = do
  ParseState e1 _ _ <- anStat
  let
    f _ =
      do
        ParseState e2 _ _ <- anStat
        fin
          >>= case _ of
              true -> pure $ R.Done $ SCU.take (SCU.length e1 - SCU.length e2) e1
              false -> fiask
        <|> (satig (\_ -> true) $> R.Loop unit)
  tailRecM f unit

linKom :: ∀ m. MonadAn m => m Unit
linKom = do
  ParseState _ (Position { column }) _ <- anStat
  if column == 1 then
    pure unit
  else
    fiask

lit :: ∀ m. MonadAn m => Char -> m Unit
lit l = satig (_ == l)

linFin :: ∀ m. MonadAn m => m Unit
linFin = void (lit '\n') <|> eof

-- spac1?
spac :: ∀ m. CL.Lazy (m (Array Unit)) => MonadAn m => m Unit
spac = A.many (lit ' ') $> unit

spac1 :: ∀ m. CL.Lazy (m (Array Unit)) => MonadAn m => m Unit
spac1 = lit ' ' *> spac

eble :: ∀ m a. MonadAn m => m a -> m (Maybe a)
eble an = Just <$> (prov an) <|> pure Nothing

havas :: ∀ m a. MonadAn m => m a -> m Boolean
havas an = isJust <$> eble an

kalk1 :: ∀ m a. CL.Lazy (m (Array a)) => MonadAn m => m a -> m Int
kalk1 an = A.length <$> A.some an

-- anyChar
dum :: ∀ m. MonadAn m => (Char -> Boolean) -> m String
dum f =
  konsumPer \e ->
    let
      knt = SCU.countPrefix f e

      { after, before } = splitAt knt e
    in
      Right
        { consumed: before
        , remainder: after
        , value: before
        }

dum1 :: ∀ m. MonadAn m => (Char -> Boolean) -> m String
dum1 f =
  dum f
    >>= case _ of
        "" -> fiask
        x -> pure x

iuSpac :: ∀ m. MonadAn m => m Unit
iuSpac = void $ dum \x -> x == ' ' || x == '\n'

kMontrRest :: DebugWarning => ∀ m. MonadAn m => m Unit
kMontrRest = ado
  ParseState s _ _ <- anStat
  in trace (S.take 20 s) \_ -> unit

foreign import amplJar :: String -> { kom :: Int, fin :: Int }

foreign import amplJarMon :: String -> String -> { kom :: Int, fin :: Int }

foreign import kopii :: String -> Effect Unit

type StatePagxInfKon
  = StateT PagxInfKon Mezuril

malplenPagxInfKon :: PagxInfKon
malplenPagxInfKon = { piednot: [], subtitol: [], listPagx: [], subpagx: [], subvoj: [] }

-- AVERTO: PureScript ne tre sxatas rikurson analizile. Uzu CL.defer
analiz :: ∀ a. ((String -> StatePagxInfKon (Array PagxPart)) -> StatePagxInfKon a) -> Mezuril (a /\ PagxInfKon)
analiz f =
  runStateT
    ( f \font ->
        uzAnalizPI font [ Fiask font ]
          (analiz' $ eof $> true)
    )
    malplenPagxInfKon

uzAnalizPI :: ∀ a. String -> a -> AnalizPI a -> StatePagxInfKon a
uzAnalizPI font e an =
  StateT \kom -> do
    rez <- P.runParserT font $ runStateT an kom
    pure
      $ case rez of
          Left _ -> e /\ kom
          Right x -> x

analiz' :: AnalizPI Boolean -> AnalizPI (Array PagxPart)
analiz' finAn = do
  ParseState font _ _ <- anStat
  aM <- uncons tekster
  case aM of
    Nothing -> pure []
    Just (a /\ rest) -> partoj font (a /\ rest /\ [])
  where
  subAnaliz :: ∀ a. String -> a -> AnalizPI a -> AnalizPI a
  subAnaliz font er = mapStateT lift <<< uzAnalizPI font er

  subAnalizPartoj :: String -> AnalizPI (Array PagxPart)
  subAnalizPartoj font = subAnaliz font [ Fiask font ] (analiz' $ eof $> true)

  partoj :: String -> (Tekster /\ ListT AnalizPI Tekster /\ Array PagxPart) -> AnalizPI (Array PagxPart)
  partoj font =
    let
      alPart = case _ of
        LitEr a b -> frTekst $ SCU.slice (SCU.length font - a) (SCU.length font - b) font
        FinEr x -> x
    in
      tailRecM \(a /\ l1 /\ rez) -> ado
        bm <- uncons l1
        in case bm of
          Nothing -> R.Done $ (A.snoc rez (alPart a))
          Just (b /\ l2) -> case a /\ b of
            LitEr k mez1 /\ LitEr mez2 f
              | mez1 == mez2 -> R.Loop $ (LitEr k f) /\ l2 /\ rez
            _ -> R.Loop $ b /\ l2 /\ (A.snoc rez (alPart a))

  tekster :: ListT AnalizPI Tekster
  tekster =
    ListT
      $ ( prov finAn
            >>= case _ of
                true -> pure unit
                false -> fiask
        )
      $> Done
      <|> ((\x -> Yield x (DL.defer \_ -> tekster)) <$> teskter')
    where
    litAlSet :: String -> S.HashSet Char
    litAlSet = S.fromFoldable <<< toCharArray

    alfLitAlSet :: String -> S.HashSet Char
    alfLitAlSet = litAlSet <<< (\x -> x <> toUpper x)

    latin = alfLitAlSet "abcdefghijklmnopqrstuvwxyz"

    propPerm = latin <> litAlSet "_#" <> num

    num = litAlSet "0123456789"

    hex = num <> alfLitAlSet "abcdef"

    cssVal :: AnalizPI String
    cssVal = dum1 (_ `S.member` (latin <> num <> litAlSet "-.%#"))

    -- Analizas % kaj {, suspektataj kiel Param aŭ Anst
    antAliaTekstObj :: AnalizPI TekstObj
    antAliaTekstObj =
      ( lit '%'
          *> ( prov
                ( ado
                    tekst "%%"
                    param <- dum1 (_ `S.member` propPerm)
                    strukt <-
                      (Nothing <$ tekst "%%")
                        <|> Just
                        <$ lit '|'
                        <*> tekstGxis (tekst "%%" $> true)
                    in Param param strukt
                )
                <|> pure (Tekst "%")
            )
      )
        <|> ( lit '{'
              *> ( prov
                    ( ado
                        lit '$'
                        nom <- dum1 (_ `S.member` propPerm)
                        aprior <-
                          eble do
                            tekst "//"
                            t <- dum1 (_ /= '}')
                            subAnalizPartoj t
                        tekst "}"
                        in Anst nom aprior
                    )
                    <|> pure (Tekst "{")
                )
          )

    (urlTO /\ urlKrud) = (urlTO' /\ urlKrud')
      where
      url kunProc =
        dum1
          ( _
              `S.member`
                ( latin
                    <> num
                    <> litAlSet "-._~:/?#@!$&'()*+,;="
                    <> if kunProc then litAlSet "%" else mempty
                )
          )

      urlTO' = many $ Tekst <$> url false <|> antAliaTekstObj

      urlKrud' = url true

    tabel :: AnalizPI PagxPart
    tabel = Tabel <$> A.some (prov tabelLin)
      where
      tabelLin =
        tekst "||"
          *> do
              let
                konsumLin = do
                  lin <- trim <$> dum1 (_ /= '\n') <* eble (lit '\n')
                  case SCU.stripSuffix (Pattern "_") lin of
                    Just lin' -> (lin' <> _) <$> konsumLin
                    Nothing -> pure lin
              lin <- split (Pattern "||") <$> konsumLin
              case A.last lin of
                Just "" -> pure unit
                _ -> fiask
              let
                alCxel ind knt = case A.index lin ind of
                  Nothing -> L.Nil
                  Just "" -> alCxel (ind + 1) (knt + 1)
                  Just x -> (knt /\ x) : alCxel (ind + 1) 1

                alStilCxel :: _ -> AnalizPI TabelEl
                alStilCxel (longec /\ enhav) =
                  subAnaliz
                    enhav
                    (TabelEl 1 false Nothing [ Fiask enhav ])
                    ( tempM "tabel en" ado
                        kap <- havas (lit '~')
                        spac
                        ar <- eble (arangx { flos: false, alkadr: false })
                        spac
                        enhav' <- analiz' (eof $> true)
                        in TabelEl longec kap ar enhav'
                    )
              A.fromFoldable <$> traverse alStilCxel (alCxel 0 1)

    specLin :: AnalizPI PagxPart
    specLin =
      let
        anLinRest = CL.defer \_ -> analiz' (linFin $> true)

        citLinoj = joinWith "\n" <$> A.some (lit '>' *> spac *> (fromMaybe "" <$> eble (dum1 (_ /= '\n'))) <* linFin)
      in
        tempM "tabel" tabel
          <|> tempM "hr" (tekst "----" *> A.many (lit '-') $> Hr)
          <|> tempM "citajxoj"
              ( Konsuj Cit
                  <$> (citLinoj >>= subAnalizPartoj)
              )
          <|> tempM "=" (lit '=' *> spac *> (Konsuj (Arangx { flos: false, arangx: Mez }) <$> anLinRest))
          <|> tempM "titol"
              ( do
                  plusoj <- kalk1 (lit '+')
                  spac
                  rest <- anLinRest
                  ind <- registr (et_ :: _ "subtitol") { nivel: plusoj, enhav: rest }
                  pure $ Titol ind
              )

    propoj :: AnalizPI (JsonHM (Array TekstObj))
    propoj =
      JsonHM <<< HM.fromFoldable
        <$> A.many (prov $ spac *> prop)
      where
      prop =
        Tuple <$> (toLower <$> dum1 (_ `S.member` propPerm)) <* lit '='
          <*> ( (lit '"' *> valTO <* lit '"')
                <|> (A.singleton <<< Tekst)
                <$> dum1 \x -> x /= ' ' && x /= ']'
            )

      valTO =
        A.many
          ( Tekst <$> dum1 (\x -> x /= '"' && x /= '{' && x /= '%')
              <|> antAliaTekstObj
          )

    -- Analizas blokon sen komencaj `[[ `
    blok ::
      ∀ v.
      AnalizPI (AnalizPI (AnalizPI Boolean -> AnalizPI v)) ->
      AnalizPI v
    blok nomAn = do
      (nomTekst /\ aldAn) <- konsumit nomAn
      vAn <- aldAn
      spac <* tekst "]]"
      v <-
        vAn $ tekst "[[" *> spac
          *> lit '/'
          *> spac
          *> slosxilVort nomTekst
          *> spac
          *> tekst "]]"
          $> true
      pure v

    konsujBlok :: ∀ nom ald. (nom -> ald -> KonsujInf) -> AnalizPI nom -> AnalizPI ald -> AnalizPI PagxPart
    konsujBlok kreiInf nomAn argAn =
      blok ado
        nom <- nomAn
        in ado
          arg <- argAn
          in \fin -> ado
            v <- analiz' fin
            in Konsuj (kreiInf nom arg) v

    objFiltr :: String -> { and :: Array String, not :: Array String, or :: Maybe (NEA.NonEmptyArray String) }
    objFiltr obj =
      foldl
        ( \ak v -> case SCU.uncons v of
            Just { head: '+', tail } -> ak { and = A.cons tail ak.and }
            Just { head: '-', tail } -> ak { not = A.cons tail ak.not }
            _ -> ak { or = Just $ maybe (NEA.singleton v) (NEA.cons v) ak.or }
        )
        { or: Nothing, and: [], not: [] }
        ( A.filter
            (_ /= "")
            (split (Pattern ",") =<< split (Pattern " ") (toLower obj))
        )

    -- MODULOJ
    listPages :: JsonHM String -> AnalizPI Boolean -> AnalizPI PagxPart
    listPages (JsonHM aldInf) cxes = do
      enhav <- tekstGxis cxes
      let
        akir x = HM.lookup x aldInf

        as :: forall m a. MonadAn m => String -> a -> m a
        as a b = tekst a $> b

        analizDat x = do
          dat <- akir x
          hush
            $ runParser dat
                ( do
                    f <- as "last" ALast <|> as "later than" AAntau
                    -- <|> as "after" APost
                    -- Malkomentigu ^ kiam tempmaŝino estos inventita
                    spac1
                    kv <-
                      fromMaybe 1
                        <$> eble
                            ((fromMaybe 0 <<< alNom) <$> dum1 (_ `S.member` num) <* spac1)
                    let
                      minute = 60

                      hour = 60 * minute

                      day = 24 * hour

                      week = 7 * day

                      month = 31 * day

                      year = 365 * day
                    unit <-
                      as "minute" minute
                        <|> as "hour" hour
                        <|> as "day" day
                        <|> as "week" week
                        <|> as "month" month
                        <|> as "year" year
                    _ <- eble (lit 's')
                    pure $ f (kv * unit)
                    <|> do
                        f <-
                          as "<" (\{ kom } -> AMalpli kom)
                            <|> as "<=" (\{ fin } -> AMalpli fin)
                            <|> as ">=" (\{ kom } -> APli kom)
                            <|> as ">" (\{ fin } -> APli fin)
                            <|> eble (tekst "=")
                            $> \{ kom, fin } -> AInter kom fin
                        jar <- dum1 (_ `S.member` num)
                        monatM <- eble $ lit '.' *> dum1 (_ `S.member` num)
                        pure $ f
                          $ case monatM of
                              Just monat -> amplJarMon jar monat
                              Nothing -> amplJar jar
                )

        ald =
          { category: akir "category"
          , tags: objFiltr <$> akir "tags"
          , created_by: akir "created_by"
          , created_at: analizDat "created_at"
          , updated_at: analizDat "updated_at"
          , order:
              do
                ordT <- trim <$> akir "order"
                case A.filter (_ /= "") $ split (Pattern " ") $ toLower ordT of
                  [ by, "asc" ] -> Just { by, asc: true }
                  [ by, "desc" ] -> Just { by, asc: false }
                  [ by ] -> Just { by, asc: true }
                  _ -> Nothing
          , offset: alNom =<< akir "offset"
          , limit: alNom =<< akir "perpage"
          }
      sxablon <-
        lift $ lift
          $ analiz \an ->
              { ant: _, ripet: _, post: _ }
                <$> maybe (pure []) an (akir "prependline")
                <*> an enhav
                <*> maybe (pure []) an (akir "appendline")
      ListPagx <$> registr (et_ :: _ "listPagx") (ListPagx' { ald, sxablon })

    moduloj :: AnalizPI PagxPart
    moduloj =
      blok $ slosxilVort "module"
        $> ado
            spac1
            f <-
              ( slosxilVort "listpages" $> listPages
                  <|> slosxilVort "css"
                  $> (\_ cxes -> Stilar <$> tekstGxis cxes)
              )
            pKrud <- propoj
            -- FARENDE
          in \cxes -> iuSpac *> f (forsendEr <<< kolektTekst <$> pKrud) cxes

    oldFoliar :: AnalizPI PagxPart
    oldFoliar =
      blok $ slosxilVort "tabview"
        $> pure \cxesL -> ado
            pagxoj <-
              A.many $ prov $ iuSpac *> tekst "[["
                *> blok
                    ( slosxilVort "tab"
                        $> ado
                            nom <- subAnalizPartoj =<< tekstGxis (atend (tekst "]]") $> true)
                            in \cxesP -> { nom, enhav: _ } <$> analiz' cxesP
                    )
            iuSpac *> cxesL
            ind <- registr (et_ :: _ "subvoj") 0
            in Libro ind pagxoj

    -- FARENDE: Subtenu /
    subpagx :: AnalizPI PagxPart
    subpagx =
      tempM "subpagx" do
        slosxilVort "include" *> spac1
        wikidotFont /\ voj <-
          urlKrud
            <#> \u -> case split (Pattern ":") $ toLower u of
                [ "", f, a, b ] -> Just f /\ [ a, b ]
                [ "", f, a ] -> Just f /\ [ a ]
                [ "", a ] -> Nothing /\ [ a ] -- eraro de Wikidot
                r -> Nothing /\ r
        anst <-
          (JsonHM <<< HM.fromFoldable)
            <$> many do
                iuSpac *> eble (lit '|') *> iuSpac
                nom <- toLower <$> dum1 (_ `S.member` propPerm)
                spac *> lit '=' *> spac
                krudVal <- tekstGxis $ lit '|' $> true <|> atend (tekst "]]") $> true
                val <- subAnalizPartoj krudVal
                pure $ nom /\ val
        tekst "]]"
        Subpagx
          <$> registr (et_ :: _ "subpagx")
              { wikidotFont
              , voj
              , anst
              }

    bild :: AnalizPI PagxPart
    bild = do
      Bild
        <$> eble (arangx { flos: true, alkadr: false })
        <* spac
        <* tekst "image"
        <* spac1
        <*> urlTO
        <*> propoj
        <* tekst "]]"

    arangx { alkadr, flos } =
      tempM "arangx"
        $ { flos: _, arangx: _ }
        <$> (if flos then havas (lit 'f') else pure false)
        <*> ( (lit '<' $> Liv)
              <|> (lit '=' *> ((if alkadr then ((lit '=' $> Alkadr) <|> _) else identity) $ pure Mez))
              <|> (lit '>' $> Dek)
          )

    specLit :: AnalizPI PagxPart
    specLit =
      frTekst
        <$> ( prov (spac *> lit '_' *> spac *> linFin $> "\n")
              <|> (tekst "-- " $> "— ")
          )

    cirkauit :: Char -> AnalizPI (Array PagxPart)
    cirkauit l =
      let
        lim = tekst $ SCU.singleton l <> SCU.singleton l
      in
        lim
          *> atend (satig \x -> x /= ' ')
          *> CL.defer \_ -> analiz' (lim $> true <|> linFin $> false)

    stilu l kiel = Konsuj (KunStil kiel) <$> cirkauit l

    konsumitLit :: ∀ a. AnalizPI a -> AnalizPI Tekster
    konsumitLit an = do
      ParseState s1 _ _ <- anStat
      _ <- an
      ParseState s2 _ _ <- anStat
      let
        (k /\ f) = (SCU.length s1 /\ SCU.length s2)
      when (k == f) fiask
      pure $ LitEr k f

    alKolor :: String -> String
    alKolor x =
      if A.length xl `elem` [ 3, 4, 6, 8 ] && all (_ `S.member` hex) xl then
        "#" <> x
      else
        x
      where
      xl = toCharArray x

    blokoj =
      konsujBlok
        Div
        -- FARENDE: Subtenu _
        (slosxilVort "div" $> { lin: false } <|> slosxilVort "span" $> { lin: true })
        (eble (lit '_') *> propoj)
        <|> prov (konsujBlok (\n -> \_ -> Arangx n) (arangx { flos: true, alkadr: true }) (pure unit))
        <|> konsujBlok (\_ -> TekstLargx) (slosxilVort "size") (spac1 *> cssVal)
        <|> konsujBlok
            ( \_ arg ->
                let
                  { not, and, or } = objFiltr arg
                in
                  SeEt { not, and: and <> fromMaybe [] (NEA.toArray <$> or) }
            )
            (slosxilVort "iftags")
            (spac1 *> dum1 (_ /= ']'))
        <|> moduloj
        <|> oldFoliar
        <|> subpagx
        <|> bild

    httpLigil =
      (\(a /\ _) -> Ligil { cel: injV (et_ :: _ "plen") a, nom: [ frTekst a ] })
        <$> konsumit (tekst "http" *> eble (lit 's') *> tekst "://" *> urlKrud)

    ligil simpl = do
      spac
      cel <- urlKrud
      spac *> eble (lit '|') *> spac
      let
        finTekst = if simpl then "]" else "]]]"
      nomKrud <- tekstGxis (prov (spac *> tekst finTekst) $> true <|> linFin $> false)
      nom <- case nomKrud of
        ""
          | simpl -> fiask
          | otherwise -> pure [ frTekst cel ]
        x -> subAnalizPartoj x
      pure
        $ Ligil
            { cel:
                if startsWith "http://" cel || startsWith "https://" cel then
                  injV (et_ :: _ "plen") cel
                else
                  injV (et_ :: _ "space")
                    $ replace (Pattern ":") (Replacement "/")
                    $ fromMaybe cel
                    $ SCU.stripPrefix (Pattern "/") cel
            , nom
            }

    teskter' =
      (tempM "neSintaks" $ prov $ konsumitLit $ dum1 (\x -> not $ x `S.member` ebleSintaks))
        <|> FinEr
        <$> tempM "ligil" (prov httpLigil)
        <|> (tempM "sintaks" $ prov $ FinEr <$> sintaks)
        <|> ( tempM "eskapi" $ prov
              $ (tekst "@@" *> (FinEr <<< frTekst <$> tekstGxis (tekst "@@" $> true <|> linFin $> false)))
          )
        <|> (tempM "konsumi" $ konsumitLit (satig \_ -> true))
      where
      ebleSintaks = litAlSet "{h/*_,^>+=|@[-\n#%"

    sintaks =
      (tempM "specLin" $ prov $ linKom *> specLin)
        <|> tempM "blok"
            ( lit '['
                *> ( ligil true
                      <|> ( lit '['
                            *> ( (lit '[' *> ligil false)
                                  <|> blokoj
                              )
                        )
                  )
            )
        <|> tempM "stiloj"
            ( '/' `stilu` Kursiv
                <|> '*' `stilu` Dik
                <|> ( let
                      kr a b = SupSub { sup: fromMaybe [] a, sub: fromMaybe [] b }

                      a2 f a b = f <$> (Just <$> a) <*> eble b

                      sup = cirkauit '^'

                      sub = cirkauit ','
                    in
                      a2 kr sup sub <|> a2 (flip kr) sub sup
                  )
                <|> ( tekst "##" *> spac
                      *> ( (\a b -> Konsuj a b)
                            <$> (TekstKolor <<< alKolor <$> cssVal)
                            <* spac
                            <* lit '|'
                            <* spac
                            <*> CL.defer \_ -> analiz' (linFin $> false <|> tekst "##" $> true)
                        )
                  )
                <|> '_' `stilu` Sublin
                <|> TekstObj
                <$> antAliaTekstObj
                <|> prov ('-' `stilu` Trastrek) -- Konfliktas kun specLit
            )
        <|> tempM "spec" specLit

foreign import akirNun :: Effect Int

type Revid a m
  = (a -> a) -> m Unit

type AnstFList
  = List { param :: String -> Maybe String -> PagxPart, anst :: String -> Maybe (Array PagxPart) -> PagxPart }

-- Donite funkcion por konstrui parton de paĝo, konstruu la tutan paĝon.
-- Konstruilo procezu la tutan paĝon, aŭ subpaĝoj ne montriĝos.
newtype PagxKonstruil
  = MalsekurPagxKonstruil (∀ m. Monad m => (AnstFList -> PagxPart -> m PagxPart) -> m (Array PagxPart))

postprocezPagx ::
  AnalizKnt ->
  PagxInfKon ->
  PagxKonstruil ->
  ((Array PagxPart /\ PagxInf) /\ (Revid PagxInf Aff -> Aff Unit))
postprocezPagx knt = \krudPagxInfKon (MalsekurPagxKonstruil pagxKonstruil) ->
  let
    (pagxArb /\ pagxInfKon) /\ { listPagx, subpagx, fj } =
      flip runState { listPagx: [], subpagx: [], fj: [] }
        $ malprofundTrairiArbon (pagxKonstruil /\ krudPagxInfKon) \apr ->
            let
              kunAnstList kernAnstList = case _ of
                ListPagx origInd -> do
                  { listPagx } <- get
                  novInd <-
                    for (A.index listPagx origInd) \origVal ->
                      lift do
                        i <- registr (et_ :: _ "listPagx") (Nothing /\ origVal)
                        modify_ \x -> x { fj = A.snoc x.fj $ procezListPagx i origVal kernAnstList }
                        pure i
                  pure $ ListPagx $ fromMaybe origInd novInd
                Subpagx origInd -> do
                  { subpagx } <- get
                  novInd <-
                    for (A.index subpagx origInd) \origVal ->
                      lift do
                        i <- registr (et_ :: _ "subpagx") $ Right Nothing
                        modify_ \x -> x { fj = A.snoc x.fj $ procezSubpagx i origVal kernAnstList }
                        pure i
                  pure $ Subpagx $ fromMaybe origInd novInd
                kernEl ->
                  kernEl
                    # ( kernAnstList
                          # fix \si nunAnstList el ->
                              let
                                toListAlPart anstFList lis = traverse (si anstFList) (TekstObj <$> lis)

                                toPropj anstFList propj =
                                  for propj \prop ->
                                    kolektTekstObj <$> toListAlPart anstFList prop
                              in
                                case (el /\ nunAnstList) of
                                  (TekstObj (Param a b) /\ { param } : anstFRest) -> si anstFRest $ param a b
                                  (TekstObj (Anst a b) /\ { anst } : anstFRest) -> si anstFRest $ anst a b
                                  (Bild ar objj propj /\ anstFList) -> ado
                                    objj' <- toListAlPart anstFList objj
                                    propj' <- toPropj anstFList propj
                                    in Bild ar (kolektTekstObj objj') propj'
                                  (Konsuj (Div ald propj) en /\ anstFList) -> do
                                    propj' <- toPropj anstFList propj
                                    -- en' <- for en \enS apr en (traverse $ kunAnstList anstFList)
                                    -- in Konsuj (Div ald propj') en'
                                    apr (Konsuj (Div ald propj') en) (traverse $ kunAnstList anstFList)
                                  (_ /\ anstFRest) -> apr el (traverse $ kunAnstList anstFRest)
                      )
            in
              (_ $ kunAnstList)
  in
    ( pagxArb
        /\ PagxInf
            { piednot: pagxInfKon.piednot
            , subtitol:
                ( \v ->
                    Record.insert
                      (et_ :: _ "id")
                      (krId $ abs $ hash $ forsendEr $ kolektTekst $ kolektTekstObj v.enhav)
                      v
                )
                  <$> pagxInfKon.subtitol
            , listPagx
            , subpagx
            , subvoj: pagxInfKon.subvoj
            }
    )
      /\ \revid -> for_ fj (_ $ revid)
  where
  procezListPagx :: Int -> ListPagx' -> AnstFList -> Revid PagxInf Aff -> Aff Unit
  procezListPagx ind (ListPagx' modul) anstFList revid = do
    nun <- liftEffect akirNun
    listM <-
      petKernSenAux ("/" <> knt.spac <> "/pagx/list")
        let
          f = modul.ald

          alAmpl = case _ of
            ALast x -> (nun - x) /\ nun
            AAntau x -> 0 /\ (nun - x)
            AMalpli x -> 0 /\ x
            APli x -> x /\ nun
            AInter x y -> x /\ y
        in
          { selector:
              { category: f.category
              , tags: f.tags
              , created_by: f.created_by
              , order: f.order
              , offset: f.offset
              , limit: f.limit
              , created_at: alAmpl <$> f.created_at
              , updated_at: alAmpl <$> f.updated_at
              }
          }
    listoDePagxoj ::
      Array
        { relative_path :: Array String
        , title :: String
        , tags :: Array String
        , created_by :: String
        , created_at :: Int
        , updated_by :: String
        , updated_at :: Int
        } <-
      fromMaybe [] <$> runMaybeT (malpak listM)
    let
      { ant, ripet, post } /\ sxablonInf = modul.sxablon

      sxargKom /\ sxargPost =
        postprocezPagx knt sxablonInf
          $ MalsekurPagxKonstruil \p -> do
              a' <- for ant $ p anstFList
              b' <-
                join
                  <$> for listoDePagxoj \anstdat ->
                      for ripet $ p $ (_ : anstFList)
                        $ let
                            renvIndeks l ind = A.index l (A.length l - ind - 1)

                            nom = fromMaybe "-" $ renvIndeks anstdat.relative_path 0

                            titol =
                              if anstdat.title /= "" then
                                anstdat.title
                              else
                                nom

                            ligil =
                              maybe "" (fold <<< A.intersperse "/" <<< _.tail)
                                $ A.uncons
                                $ anstdat.relative_path
                          in
                            { anst: \x -> TekstObj <<< Anst x
                            , param:
                                \val strukt -> case val of
                                  "created_at" -> Dat anstdat.created_at strukt
                                  "created_by" -> frTekst anstdat.created_by
                                  "created_by_linked" -> frTekst anstdat.created_by
                                  -- _unix, _id, _linked
                                  "updated_at" -> Dat anstdat.updated_at strukt
                                  "updated_by" -> frTekst anstdat.updated_by
                                  "updated_by_linked" -> frTekst anstdat.created_by
                                  -- commented
                                  "name" -> frTekst nom
                                  "category" -> frTekst $ fromMaybe "-" $ renvIndeks anstdat.relative_path 1
                                  "fullname" -> frTekst $ fold $ A.intersperse "/" anstdat.relative_path
                                  "title" -> frTekst titol
                                  "title_linked" ->
                                    Ligil
                                      { cel: injV (et_ :: _ "space") ligil
                                      , nom: [ frTekst titol ]
                                      }
                                  -- parent
                                  "%%link%%" -> frTekst ligil
                                  -- content, rating
                                  _ -> TekstObj $ Param val strukt
                            }
              c' <- for post $ p anstFList
              let
                unig = case _ of
                  L.Cons (Tabel a) (L.Cons (Tabel b) rest) -> unig (L.Cons (Tabel $ a <> b) rest)
                  L.Cons (Konsuj Cit a) (L.Cons (Konsuj Cit b) rest) -> unig (L.Cons (Konsuj Cit $ a <> b) rest)
                  L.Cons x rest -> L.Cons x (unig rest)
                  L.Nil -> L.Nil

                rez = A.fromFoldable $ unig $ L.fromFoldable (a' <> b' <> c')
              pure rez

      tr = _Newtype <<< prop (et_ :: _ "listPagx") <<< ix ind <<< _1
    revid $ tr .~ Just sxargKom
    sxargPost \gxis -> revid $ tr <<< _Just <<< _2 %~ gxis

  -- FARENDE: Aldoni kaŝmemoron
  procezSubpagx :: Int -> { wikidotFont :: _, voj :: _, anst :: _ } -> AnstFList -> Revid PagxInf Aff -> Aff Unit
  procezSubpagx ind { wikidotFont, voj, anst: JsonHM anst } oldAnstFList revid =
    let
      revid' f = revid $ _Newtype <<< prop (et_ :: _ "subpagx") <<< ix ind %~ f

      inj v = revid' \_ -> v

      voj' = case wikidotFont of
        Just f -> { spac: "wikidot", voj: A.cons f voj }
        Nothing -> { spac: knt.spac, voj }

      anstFList =
        { param: \x -> TekstObj <<< Param x
        , anst:
            \x apr ->
              Konsuj (Div { lin: true } (JsonHM $ mempty))
                $ fromMaybe (fromMaybe [ TekstObj $ Anst x apr ] apr)
                $ HM.lookup x anst
        }
          : oldAnstFList
    -- FARENDE: ERARON
    in
      void $ akirPagx knt { voj: voj', bezonatFresx: KasxmRapida } anstFList $ snd
        >>> case _ of
            Nothing -> inj $ Left $ (fromMaybe "" (wikidotFont <#> \wF -> "/wikidot/" <> wF <> "/")) <> joinWith "/" voj
            Just ({ analizit } /\ sxarg) -> do
              inj $ Right $ Just analizit
              sxarg \f -> revid' $ _Right <<< _Just <<< _2 %~ f

analizPagxMez ::
  ∀ m.
  MonadAff m =>
  String ->
  m (Array PagxPart /\ PagxInfKon)
analizPagxMez kod = do
  rez /\ (Malsxpar tempinf) <-
    liftEffect
      $ runStateT
          (malMezuril $ tempM' "All" $ analiz (_ $ kod))
          (Malsxpar HM.empty)
  log
    $ show
    $ A.sortBy (\(_ /\ a) (_ /\ b) -> b `compare` a)
    $ HM.toArrayBy Tuple tempinf
  pure rez

-- FARENDE: Krei type AnalizKnt
type AnalizKnt
  = { revidKasxm :: Revid PagxKasxm Aff, kasxm :: PagxKasxm, spac :: String }

postprocezKernaPagx ::
  AnalizKnt ->
  (Array PagxPart /\ PagxInfKon) ->
  AnstFList ->
  ((Array PagxPart /\ PagxInf) /\ (Revid PagxInf Aff -> Aff Unit))
postprocezKernaPagx knt (pagxArb /\ pagxInfKon) anstFList =
  postprocezPagx knt pagxInfKon
    $ MalsekurPagxKonstruil \f ->
        traverse (f anstFList) pagxArb

analizVer :: Int
analizVer = 0

type PagxKasxmDat
  = Int /\ Maybe Json /\ Maybe (PagxEnhav PagxInfKon)

cxuStila :: ∀ r. { voj :: Array String | r } -> Boolean
cxuStila { voj } = maybe false (startsWith "obs-theme-") $ A.last voj

-- | Petu paĝon el multaj fontoj kaj postprocezu.
-- Ĉi tiu funkcio inkludas logikon por postprocezado de la paĝo.
-- Mi nur esperas, ke ĉi tio ne estas granda eraro.
akirPagx ::
  ∀ m.
  MonadAff m =>
  AnalizKnt ->
  { voj :: PagxVoj, bezonatFresx :: KasxmFresx } ->
  AnstFList ->
  (Maybe KasxmFresx /\ Maybe (PagxEnhav PagxInf /\ (Revid PagxInf Aff -> Aff Unit)) -> Aff Unit) ->
  m Boolean
akirPagx knt pet anstFList procezf = do
  rez /\ f <- runStateT loka Nothing
  liftAff $ traverse_ joinFiber f
  pure rez
  where
  kasxmK = Konservata $ "PAGX:" <> intercalate "/" ([ pet.voj.spac ] <> pet.voj.voj)

  send :: Maybe KasxmFresx -> Maybe (PagxEnhav PagxInfKon) -> StateT (Maybe (Fiber Unit)) m Unit
  send fresx nov = do
    old <- get
    liftAff $ traverse_ (killFiber $ error "aborted") old
    let
      nov' =
        nov
          <#> \{ titol, etikedoj, kod, analizit } ->
              let
                pagx /\ gxisil = postprocezKernaPagx knt analizit anstFList
              in
                { titol, etikedoj, kod, analizit: pagx } /\ gxisil
    put =<< liftEffect (Just <$> launchAff (procezf $ fresx /\ nov'))

  loka = do
    kasxmVal :: Maybe PagxKasxmDat <- maybe Nothing ((hush <<< decodeJson) <=< (hush <<< jsonParser)) <$> akirK kasxmK
    case kasxmVal of
      Just (oldAnVer /\ kasxmId /\ rez)
        | oldAnVer == analizVer ->
          let
            sekvFresx = maybe (Just bottom) succ $ HM.lookup pet.voj knt.kasxm
          in
            send Nothing rez *> pasx sekvFresx kasxmId
      _ -> pasx (Just bottom) Nothing

  pasx :: Maybe KasxmFresx -> Maybe Json -> StateT (Maybe (Fiber Unit)) m Boolean
  pasx (Just fresx) oldKasxmId
    | fresx <= pet.bezonatFresx = do
      valM :: Either _ { continue :: _, page :: _ } <-
        petKernSenAux
          ("/" <> pet.voj.spac <> "/pagx/pet" <> guard (fresx == KasxmLonga) "/long")
          { path: pet.voj.voj, cacheId: oldKasxmId }
      case valM of
        Left e -> erari (show e) $> false
        Right { continue, page } -> do
          kasxmId <-
            page
              # ( caseV
                    # onV (et_ :: _ "keep") (\(_ :: UnitResp) -> pure Nothing)
                    # onV (et_ :: _ "new") \( pagxM ::
                          Maybe { cacheId :: _, title :: _, tags :: _, text :: _ }
                      ) -> do
                        let
                          kasxmId = _.cacheId <$> pagxM
                        pagx <-
                          for pagxM \{ title, tags, text } -> ado
                            analizit <-
                              if cxuStila pet.voj then
                                pure $ [ frTekst text ] /\ malplenPagxInfKon
                              else
                                analizPagxMez text
                            in { titol: fromMaybe "" title
                            , etikedoj: tags
                            , kod: text
                            , analizit: analizit
                            }
                        metK (stringify $ encodeJson $ (analizVer /\ kasxmId /\ pagx :: PagxKasxmDat)) kasxmK
                        send (Just fresx) pagx
                        pure kasxmId
                )
          liftAff $ knt.revidKasxm $ HM.insert pet.voj fresx
          if continue then
            pasx (succ fresx) kasxmId
          else
            pure true

  pasx _ _ = pure true

--
malpakUnikId :: ∀ r. { id :: UnikId | r } -> LL.List Char
malpakUnikId = _.id >>> \(UnikId _ x) -> x

mallongUnikId :: ∀ r. Int -> Array { id :: UnikId | r } -> String
mallongUnikId = \ind ar ->
  let
    enhavId = do
      interesat <- A.index ar ind
      rest <- L.deleteAt ind $ L.fromFoldable ar
      pasx (malpakUnikId interesat) (malpakUnikId <$> rest)
  in
    ( case enhavId of
        Just x -> \r ->
          (fromCharArray $ A.fromFoldable x)
            <> "-"
            <> r
        Nothing -> identity
    )
      $ "st"
      <> (show ind)
  where
  pasx :: LL.List Char -> List (LL.List Char) -> Maybe (List Char)
  pasx font aliaj = case LL.uncons font of
    Just { head, tail } ->
      let
        konflikt =
          L.filter (\alia -> alia.head == head)
            $ L.catMaybes (LL.uncons <$> aliaj)
      in
        if L.length konflikt == 0 then
          Just (head : L.fromFoldable (LL.take 2 tail))
        else
          (head : _) <$> pasx tail (_.tail <$> konflikt)
    Nothing -> Nothing

newtype Konj
  = Konj { ind :: Int, cert :: Int }

instance konjEq :: Eq Konj where
  eq (Konj a) (Konj b) = a.cert == b.cert

instance konjOrd :: Ord Konj where
  compare (Konj a) (Konj b) = a.cert `compare` b.cert

unikIdAlLok :: ∀ r. String -> Array { id :: UnikId | r } -> Maybe Int
unikIdAlLok plenId = \titoloj -> do
  taksKon <- taksKonjektM
  Max (Konj { ind }) <-
    foldlWithIndex
      (\ind ak titolMia -> ak <> Just (Max $ Konj { ind, cert: taksKon (malpakUnikId titolMia) ind }))
      Nothing
      titoloj
  pure ind
  where
  taksKonjektM :: Maybe (LL.List Char -> Int -> Int)
  taksKonjektM = case split (Pattern "-") plenId of
    [ hashLia, indLia ] -> Just \hash ind -> kmpHash hashLia hash + fromMaybe 0 (kmpInd indLia <@> ind)
    [ un ] ->
      Just
        $ case kmpInd un of
            Just f -> \_ -> f
            Nothing -> \hash -> \_ -> kmpHash un hash
    _ -> Nothing

  kmpHash :: String -> LL.List Char -> Int
  kmpHash lia mia
    | Just _ <- LL.stripPrefix (LL.Pattern $ LL.fromFoldable $ toCharArray lia) mia = 2
    | otherwise = 0

  kmpInd :: String -> Maybe (Int -> Int)
  kmpInd lia = ado
    lia' <- strAlInt 0 =<< SCU.stripPrefix (Pattern "st") lia
    in \mia -> if lia' == mia then 1 else 0

  strAlInt :: Int -> String -> Maybe Int
  strAlInt ak str = case SCU.uncons str of
    Just { head, tail } -> SCU.indexOf (Pattern $ SCU.singleton head) "0123456789" >>= \v -> strAlInt (ak * 10 + v) tail
    Nothing -> Just ak

foreign import rulumAlTitolId :: AtendElektRealig -> String -> Int -> Effect Unit
