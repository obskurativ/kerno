export function akirLigilCel(ev) {
  return function() {
    let ligil = ev.target;
    return { host: ligil.host, pathname: ligil.pathname, hash: ligil.hash };
  }
}

export function akirArtRulum() {
  return document.querySelector("#rulumej").scrollTop;
}

export function rulumArtAl(atendElekt) {
  return function(t) {
    return function() {
      document.body.classList.add("senlumig");
      function fin() {
        document.body.classList.remove("senlumig");
      }
      try {
        atendElekt("#rulumej").then(function(x) {
          x.scrollTop = t;
          fin();
        });
      }
      catch(e) { console.error(e); }
      finally { fin(); }
    }
  }
}
