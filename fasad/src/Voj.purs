module Voj where

import Prelude hiding ((/))
import Control.Monad.Maybe.Trans (runMaybeT)
import Data.Argonaut (Json, decodeJson, encodeJson)
import Data.Array (filter)
import Data.Either (Either(..))
import Data.Foldable (for_)
import Data.Generic.Rep (class Generic)
import Data.Lazy (force)
import Data.Lens (_Just, _2)
import Data.Lens.Record (prop)
import Data.Lens.Setter ((%~))
import Data.List (List(..))
import Data.Maybe (Maybe(..), fromMaybe, isJust, maybe)
import Data.String (Pattern(..), stripPrefix)
import Data.Tuple (fst)
import Data.Tuple.Nested ((/\))
import Datum (GxisKunt, KasxmFresx(..), Model, Mondo, Msg(..), Pagx, varPrism)
import Effect (Effect)
import Effect.Aff (Aff, error, joinFiber, killFiber, suspendAff)
import Effect.Aff.Class (class MonadAff, liftAff)
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Ref as Ref
import Flame (Html)
import Flame.Html.Attribute as HA
import Flame.Html.Element as HE
import Foreign (unsafeToForeign)
import Medio (AtendElektRealig, akirK, atendElektRealig, caseV, erari, et_, injV, jxetonK, lastVizK, malpak, metK, nunLok, onV, petKernAux, petKernSenAux, pord)
import Pagx.Hered.Analiz (akirNunMs, akirPagx, rulumAlTitolId, tempDif, unikIdAlLok)
import Pagx.Hered.Tipoj (PagxInf(..))
import Routing.Duplex (RouteDuplex', many, parse, root, segment)
import Routing.Duplex.Generic as G
import Routing.Duplex.Generic.Syntax ((/))
import Supr.KreiKont (auxStatus)
import Web.Event.Event (preventDefault)
import Web.Event.Internal.Types (Event)
import Web.HTML (window)
import Web.HTML.HTMLDocument (setTitle)
import Web.HTML.History (DocumentTitle(..), URL(..), pushState, replaceState)
import Web.HTML.Location (hash, host, pathname)
import Web.HTML.Window (document, history)

data Voj
  = VEnsalutKod String
  | VPagx String (Array String)

derive instance vojGeneric :: Generic Voj _

vojR :: RouteDuplex' Voj
vojR =
  root
    $ G.sum
        { "VEnsalutKod": "-" / "aux" / "kod" / segment
        , "VPagx": segment / (many segment :: RouteDuplex' (Array String))
        }

foreign import akirArtRulum :: Effect Int

foreign import rulumArtAl :: AtendElektRealig -> Number -> Effect Unit

-- FARENDE: alHash ŝanĝu staton de la paĝo (ekzemple, ŝanĝante langeton)
pagxKondut ::
  ∀ m.
  MonadEffect m =>
  Pagx ->
  { alHash :: { tuj :: Boolean, neNulHash :: String } -> m Unit
  , stat :: Maybe { sav :: m Json, leg :: Maybe Json -> m Unit }
  , titol :: String
  }
pagxKondut =
  caseV
    # onV (et_ :: _ "regPagx")
        ( \p -> case ( caseV
                # onV (et_ :: _ "montr") identity
                # onV (et_ :: _ "redakt") (Just <<< _.enhav)
            )
              p.regxim of
            Just val -> regPagx val
            Nothing -> malpl "404"
        )
    # onV (et_ :: _ "erarPagx") (\_ -> malpl "500")
    # onV (et_ :: _ "sxalt") (\_ -> malpl "...")
  where
  malpl titol = { alHash: \_ -> pure unit, stat: Nothing, titol: "[" <> titol <> "] | Obs" }

  regPagx p =
    { alHash:
        \{ tuj, neNulHash } ->
          let
            titoloj = (\{ analizit: (_ /\ PagxInf v) } -> v.subtitol) p
          in
            for_
              (unikIdAlLok neNulHash $ titoloj)
              (liftEffect <<< rulumAlTitolId atendElektRealig (if tuj then "auto" else "smooth"))
    , stat:
        Just
          { sav:
              do
                prevRulum <- liftEffect akirArtRulum
                pure $ encodeJson { rulum: prevRulum }
          , leg:
              \datMM ->
                unit
                  <$ runMaybeT do
                      { rulum } <- maybe (pure { rulum: 0.0 }) (malpak <<< decodeJson) datMM
                      liftEffect $ rulumArtAl atendElektRealig rulum
          }
    , titol: p.titol
    }

type SavStat
  = { ald :: Maybe Json, pagx :: Pagx }

cxesSxarg :: ∀ m. MonadAff m => GxisKunt Model -> m Unit
cxesSxarg { model, revid } = do
  liftAff $ for_ model.sxarg \sx -> killFiber (error "aborted") sx.f
  revid _ { sxarg = Nothing }

alVoj ::
  ∀ m.
  MonadAff m =>
  GxisKunt Model ->
  { voj :: String, plenHash :: String } ->
  m Unit
alVoj gxisKnt@{ model: origModel, revid } = \p ->
  liftAff do
    cxesSxarg gxisKnt
    f <-
      suspendAff do
        komenca <- liftEffect $ Ref.new true
        alPagx' { voj: p.voj, hash: stripPrefix (Pattern "#") $ p.plenHash, komenca }
    revid _ { sxarg = Just { f, msg: Nothing, finit: Nothing } }
    joinFiber f
    -- statusFin \x -> Just $ fromMaybe { gracie: false } x
    pure unit
  where
  histStat metod (ald /\ pagx) voj =
    liftEffect do
      hist <- window >>= history
      metod
        (unsafeToForeign $ encodeJson ({ ald, pagx } :: SavStat))
        (DocumentTitle "")
        -- ^ Ĉi tiu kampo estas ignorata far kroziloj.
        (URL voj)
        hist

  sxang :: { voj :: _, hash :: _, komenca :: _ } -> Pagx -> Aff Unit
  sxang { voj: novVoj, hash: novHash, komenca } al = do
    komenca' <- liftEffect $ Ref.read komenca
    let
      savSxangxit = pord komenca' (pagxKondut origModel.pagx).stat
    for_ savSxangxit \{ sav } -> do
      ald <- sav
      lok <- nunLok
      voj <- liftEffect $ pathname lok
      h <- liftEffect $ hash lok
      histStat replaceState (Just ald /\ origModel.pagx) (voj <> h)
    histStat
      (if isJust savSxangxit then pushState else replaceState)
      (Nothing /\ al)
      ( novVoj
          <> case novHash of
              Just h -> "#" <> h
              Nothing -> ""
      )
    liftEffect $ Ref.write false komenca
    let
      novKondut = pagxKondut al
    liftEffect (window >>= document >>= setTitle novKondut.titol)
    case novKondut.stat of
      Just s -> do
        metK novVoj lastVizK
        s.leg Nothing
      Nothing -> pure unit
    for_ novHash \h -> novKondut.alHash { tuj: true, neNulHash: h }
    revid _ { pagx = al }

  status sim val =
    revid
      $ statusSxarg
      %~ _ { msg = Just $ sim /\ ("pagx.status." <> val) }

  statusSxarg = prop (et_ :: _ "sxarg") <<< _Just

  -- FARENDE: Anstataŭigi Maybe a per a?
  statusFin :: ∀ m1. MonadAff m1 => _ -> m1 Unit
  statusFin f = revid $ statusSxarg %~ \x -> x { finit = f x.finit }

  regPagx = prop (et_ :: _ "pagx") <<< varPrism (et_ :: _ "regPagx")

  -- Pasi "komenca" kun voj kaj hash ne estas la plej bona ideo
  alPagx' :: { voj :: String, hash :: Maybe String, komenca :: Ref.Ref Boolean } -> Aff Unit
  alPagx' pet =
    let
      sxang' = sxang pet

      erarPagx = sxang' <<< injV (et_ :: _ "erarPagx")
    in
      case parse vojR pet.voj of
        Left e -> erari (show e) *> erarPagx "erar.klient.404"
        Right x -> case x of
          VPagx spac krudVoj -> do
            let
              voj = filter (_ /= "") krudVoj
            {-
          Sekva kodo estas implika pro uzado de imperativa Aff.

          Oni komprenu, ke Aff pasata al akirPagx estas rompebla.
          "komenca" priskribas, ĉu ĥistorio jam estis preskribita.
          Ni faras ĉi tion nur post kiam nova paĝo ŝargiĝis.
          "skribo" priskribas, kiom da fojoj ena funkcio sukcesis (ne estis rompita) kaj reskribis la paĝon.

          Ekzistas strebo priskribi "komenca" kaj "skribo" pure en akiPagx, sed ĉi tio verŝajne ne estas bona ideo,
          ĉar ŝaltado de la paĝo el servilo ne signifas tion, ke ni estis sufiĉe rapidaj por reksribi URLon kaj ne
          signifas, ke ni jam reskribis la montratan paĝon.
          -}
            status "search" "pet"
            skriboj <- liftEffect $ Ref.new 0
            komTemp <- liftEffect akirNunMs
            sukces <-
              akirPagx
                { kasxm: origModel.pagxKasxm, revidKasxm: \f -> revid $ (prop (et_ :: _ "pagxKasxm") %~ f), spac }
                { voj: { spac, voj }, bezonatFresx: KasxmLonga }
                Nil \(_ /\ enhavM) -> do
                status "autorenew" "pet.long"
                -- FARENDE: ^ Ĉi tie ni ne uzas freŝecon. Ni uzu?
                sxang'
                  $ injV (et_ :: _ "regPagx")
                      { malfermUzInf: false
                      , spac
                      , voj
                      , regxim: injV (et_ :: _ "montr") (fst <$> enhavM)
                      , redaktebla: false
                      }
                liftEffect $ Ref.modify_ (_ + 1) skriboj
                for_ enhavM \(_ /\ post) ->
                  post \f ->
                    revid
                      $ regPagx
                      <<< prop (et_ :: _ "regxim")
                      <<< varPrism (et_ :: _ "montr")
                      <<< _Just
                      <<< prop (et_ :: _ "analizit")
                      <<< _2
                      %~ f
            finTemp <- liftEffect akirNunMs
            status "settings" "agord"
            cxuM <- runMaybeT (malpak =<< petKernAux ("/" <> spac <> "/agant") { path: voj })
            for_ cxuM \cxu -> revid $ regPagx %~ _ { redaktebla = cxu }
            gracie <-
              if sukces then do
                skriboj' <- liftEffect $ Ref.read skriboj
                if skriboj' > 1 then
                  status "breaking_news_alt_1" "pet.long.nov"
                else
                  status "published_with_changes" "pet.long.nul"
                pure $ tempDif komTemp finTemp > 750.0
              else do
                status "sync_problem" "pet.long.erar"
                pure false
            statusFin \_ -> Just { gracie }
          VEnsalutKod kod -> do
            jxetonM <- petKernSenAux "/-/aux/kod" { code: kod }
            case jxetonM of
              Left e ->
                erarPagx
                  =<< onV
                      (et_ :: _ "invalidCode")
                      (\_ -> pure $ "aux.erar.malfresxkod")
                      (\e' -> erari (show e') $> "erar.klient")
                      e
              Right x' -> do
                metK x' jxetonK
                auxM <- liftAff auxStatus
                for_ auxM \aux -> revid _ { auxStatus = aux }
                lastViz <- akirK lastVizK
                alPagx' { voj: fromMaybe "/" lastViz, hash: Nothing, komenca: pet.komenca }

sekurasForlasi :: Pagx -> Boolean
sekurasForlasi =
  onV
    (et_ :: _ "regPagx")
    ( \rp ->
        onV
          (et_ :: _ "redakt")
          (\_ -> false)
          (\_ -> true)
          rp.regxim
    )
    (\_ -> true)

foreign import akirLigilCel :: Event -> Effect { host :: String, pathname :: String, hash :: String }

-- FARENDE: Akcepti ne nur Array (Html Msg)
-- | Saĝa ligilo al URL kun ena teksto.
-- | Se la ligilo ne estas dosiera kaj apartenas al sama fonto,
-- | je klako okazas senreŝarĝa redirektigo.
sligil :: Mondo -> { nom :: Array (Html Msg), voj :: String } -> Html Msg
sligil mond { nom, voj } =
  HE.a
    ( [ HA.href voj ]
        <> ( if force mond.sekurasForlasi then
              [ HA.onClick' \ev ->
                  Gxis \g -> do
                    cel <- liftEffect $ akirLigilCel ev
                    miaHost <- liftEffect $ nunLok >>= host
                    when (cel.host == miaHost)
                      $ liftEffect (preventDefault ev)
                      *> alVoj g { voj: cel.pathname, plenHash: cel.hash }
              ]
            else
              [ HA.target "_blank" ]
          )
    )
    nom
