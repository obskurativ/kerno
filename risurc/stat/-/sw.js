let orig = (new URL(self.registration.scope)).origin;

self.addEventListener("activate", (event) => {
  event.waitUntil(
    caches
      .open("v1")
      .then((kasx) => kasx.add("/index.html"))
  );
});

// FARENDE: Informu sw.js pri ĝisdatigo de risurcoj (ekzemple, uzantbildoj)
// FARENDE: Eble ni ne kaŝmemoru nebezonatajn bildojn (ekz. ekster-risurc)
self.addEventListener("fetch", (event) => {
  if (event.request.method != "GET") return;
  let url = new URL(event.request.url);
  if (url.origin != orig || url.pathname.startsWith("/-/ekster-risurc/") || url.pathname.startsWith("/-/uzantbild/")) return;

  return event.respondWith(async function() {
    let pet = event.request.url.split("/").at(-1).includes(".") ? event.request : new Request("/index.html", { cache: event.request.cache });
    let respAtend = fetch(pet);

    let kasx = await caches.open("v1");
    event.waitUntil(respAtend.then((resp) => {
      if (resp.ok)
        return kasx.put(pet, resp.clone());
    }))

    let kasxVal = await kasx.match(pet);
    if (event.request.cache != "reload" && kasxVal)
      return kasxVal;
    else
      return await respAtend;
  }());
});

