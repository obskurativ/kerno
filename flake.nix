{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    pre-commit.url = "github:cachix/pre-commit-hooks.nix";
    pre-commit.inputs.nixpkgs.follows = "nixpkgs";
    wikidot-evakuilo.url = "gitlab:obskurativ/wikidot-evakuilo";
    wikidot-evakuilo.inputs.nixpkgs.follows = "nixpkgs";
    safe-webp = {
      url = "gitlab:obskurativ/safe-webp";
      flake = false;
    };
    purs-nix = {
      url = "github:purs-nix/purs-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    ps-tools.follows = "purs-nix/ps-tools";
  };

  outputs =
    inputs@{ self, nixpkgs, pre-commit, wikidot-evakuilo, safe-webp, ... }:
    let
      systems = [ "x86_64-linux" "aarch64-linux" ];
      forEverySystem = nixpkgs.lib.genAttrs systems;
      dev-nixpkgs = import nixpkgs { system = "x86_64-linux"; };
      ghcVer = "94";
      haskPkg = haskell: haskell.packages."ghc${ghcVer}";

      purs-nix = inputs.purs-nix { system = "x86_64-linux"; };
      ps = purs-nix.purs {
        srcs = [ "src" ];
        dependencies = with purs-nix.ps-pkgs; [
          aff
          aff-promise
          affjax
          affjax-web
          argonaut
          argonaut-codecs
          argonaut-generic
          arrays
          bifunctors
          console
          control
          debug
          effect
          either
          email-validate
          flame
          foldable-traversable
          foreign
          foreign-object
          functions
          http-methods
          identity
          integers
          lazy
          lists
          maybe
          newtype
          ordered-collections
          orders
          parsing
          prelude
          profunctor-lenses
          record
          refs
          routing-duplex
          strings
          stringutils
          tailrec
          transformers
          tuples
          unordered-collections
          unsafe-coerce
          variant
          web-dom
          web-events
          web-file
          web-html
          web-storage
        ];
        dir = ./fasad;
      };
    in rec {
      checks."x86_64-linux".pre-commit = with dev-nixpkgs;
        pre-commit.lib."x86_64-linux".run {
          src = ./.;
          settings.ormolu.cabalDefaultExtensions = true;
          hooks = {
            ormolu = {
              enable = true;
              entry = lib.mkForce
                "${ormolu}/bin/ormolu --mode inplace --ghc-opt -XArrows";
            };
            purty.enable = true;
            nixfmt.enable = true;
          };
        };

      devShells."x86_64-linux".default = with dev-nixpkgs;
        mkShell {
          inherit (self.checks."x86_64-linux".pre-commit) shellHook;
          nativeBuildInputs = [
            stack
            (haskPkg haskell).ghc
            icu
            zlib
            rocksdb
            pkg-config
            (pkgs.haskell-language-server.override {
              supportedGhcVersions = [ ghcVer ];
            })
            haskellPackages.ormolu
            llvmPackages_13.llvm
            libwebp
            cabal2nix
            esbuild
            purs-nix.purescript
            nodePackages.purescript-language-server
            spago
            nodePackages.purty
            nodePackages.sass
            jq
            (ps.command { bundle.esbuild.format = "iife"; })
            (pkgs.writeShellScriptBin "fasad" ''
              set -e
              ordejo=$(mktemp)
              untranslated=$(mktemp)
              risurc="../risurc"
              echo '{' > $untranslated
              kom=true
              rm $risurc/tradukoj/untranslated.json || true
              for d in $risurc/tradukoj/*.json; do
                jq 'to_entries | sort | from_entries' $d > $ordejo
                mv $ordejo $d
                if [ $kom = false ]; then
                  echo ',' >> $untranslated
                fi
                kom=false
                echo -n "\"$(basename $d .json)\": " >> $untranslated
                echo "[$(cat $risurc/tradukoj/eo.json),$(cat $d)]" | jq '(.[0] | keys)-(.[1] | keys)' -r >> $untranslated
                truncate -s -1 $untranslated # Forigu finan `\n`
              done
              echo >> $untranslated
              echo "}" >> $untranslated
              sed -i '/^[{}]/! s/^/  /g' $untranslated
              cp $untranslated $risurc/tradukoj/untranslated.json

              purs-nix bundle
              mv main.js $risurc/stat/-/kern.js
              sass ../stil/stil.sass > $risurc/stat/-/stil.css
            '')
            (pkgs.writeShellScriptBin "sendmail" "cat")
          ];
        };

      packages = nixpkgs.lib.recursiveUpdate {
        x86_64-linux = rec {
          client = ps.bundle { esbuild.format = "iife"; };
          stat-shared = with dev-nixpkgs;
            stdenv.mkDerivation {
              name = "obs-stat-shared";
              nativeBuildInputs = [ nodePackages.sass ];
              phases = [ "installPhase" ];
              installPhase = ''
                mkdir $out
                cp -r ${./risurc}/stat/-/* $out
                cp ${client} $out/kern.js
                sass ${./stil}/stil.sass > $out/stil.css
              '';
            };
        };
      } (forEverySystem (system:
        let pkgs = import nixpkgs { inherit system; };
        in rec {
          # FARENDE: Disigi risurcojn disde kodo!
          server = with pkgs.haskell.lib.compose;
            overrideCabal (old: {
              executableHaskellDepends = old.executableHaskellDepends
                ++ [ pkgs.llvm pkgs.system-sendmail ];
            }) (with (haskPkg pkgs.haskell).override {
              overrides = nov: old: { retry = dontCheck old.retry; };
            };
              callPackage ./servil/servil.nix {
                wikidot-evakuilo =
                  wikidot-evakuilo.packages.${system}.for."ghc${ghcVer}";
                webp = dontCheck (callPackage "${safe-webp}/webp.nix" { });
              });
        }));

      nixosModules = {
        obs = { pkgs, config, ... }:
          let
            system = config.nixpkgs.system;
            obs = packages."${system}";
          in {
            systemd.tmpfiles.rules = [
              "d  /var/lib/obskurativ 755 obskurativ obskurativ"
              "L+ /var/lib/obskurativ/uzantnom-anst.json - - - - ${
                ./risurc/uzantnom-anst.json
              }"
              "L+ /var/lib/obskurativ/tradukoj - - - - ${./risurc/tradukoj}"
              "d  /var/lib/obskurativ/stat 0755 obskurativ obskurativ"
              "L+ /var/lib/obskurativ/stat/- - - - - ${packages.x86_64-linux.stat-shared}"
            ];

            systemd.services.obskurativ = {
              description = "Obskurativ server";
              after = [
                "network.target"
                "janusgraph.service"
                "systemd-tmpfiles-setup.service"
              ];
              wantedBy = [ "multi-user.target" ];
              serviceConfig = {
                ExecStart =
                  "${obs.server}/bin/obskurativ server --db=127.0.0.1 --proxy=127.0.0.1:8118";
                User = "obskurativ";
                WorkingDirectory = "/var/lib/obskurativ";
              };
            };

            users.users.obskurativ = {
              isSystemUser = true;
              group = "obskurativ";
              description = "Obskurativ server user";
            };
            users.groups.obskurativ = { };
          };

        obs-db = { pkgs, config, ... }: {
          services.cassandra = {
            package = pkgs.cassandra_4;
            enable = true;
          };

          systemd.services.janusgraph = {
            description = "JanusGraph server";
            wantedBy = [ "multi-user.target" ];
            after = [ "network.target" "cassandra.service" ];
            requires = [ "cassandra.service" ];
            serviceConfig = {
              ExecStart = "${pkgs.janusgraph}/bin/janusgraph-server";
              User = "janusgraph";
              WorkingDirectory = "${./nix/janusgraph}";
            };
          };
          systemd.services.cassandra.serviceConfig = {
            ExecStartPost = "${pkgs.coreutils}/bin/sleep 10";
          };
          users.users.janusgraph = {
            isSystemUser = true;
            group = "janusgraph";
            description = "JanusGraph server user";
          };
          users.groups.janusgraph = { };
        };
      };

      nixosConfigurations.dev-obs-db = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          nixosModules.obs-db
          (_: {
            boot.isContainer = true;
            networking.firewall.allowedTCPPorts = [
              8182 # gremlin
            ];
            system.stateVersion = "23.05";
          })
        ];
      };
    };
}
