module Db where

import Control.Applicative.Free (Ap, liftAp, runAp, runAp_)
import Control.Carrier.Reader
import Crypto.Hash.SHA256
import Data.Aeson (Object)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Base64.URL as B64Url
import qualified Data.ByteString.Lazy as BSL
import Data.Functor.Compose
import Data.Greskell (key)
import Data.Greskell.AsLabel
import Data.Greskell.GTraversal.Gen
import Data.Pool
import Datum
import Network.Greskell.WebSocket.Client
import qualified RIO.Partial as P
import qualified RIO.Text as T
import SPrelude
import System.Directory
import System.FilePath
import System.IO (openBinaryFile)
import Unsafe.Coerce (unsafeCoerce)

tW :: Walk Transform a b -> Walk Transform a b
tW = id

sW :: Walk SideEffect a b -> Walk SideEffect a b
sW = id

-- grafa kerno
gFont :: Greskell GraphTraversalSource
gFont = source "g"

greskellNun :: Greskell Int
greskellNun = unsafeGreskell "nun"

greskellNull :: Greskell a
greskellNull = unsafeGreskell "null"

gMalkovr :: Greskell (Maybe a) -> Greskell a
gMalkovr = unsafeGreskell . toGremlin

gKontrolFinTemp :: forall c s. (WalkType c, Split c c, Element s) => Walk c s s
gKontrolFinTemp = gOr @_ @c [gHas2P skFinTemp $ pGt greskellNun, unsafeWalk "hasNot" [toGremlin $ skFinTemp @s]]

gFontNun :: Greskell GraphTraversalSource
gFontNun = source $ "g.withStrategies(new SubgraphStrategy(edges: " <> kon <> ", vertices: " <> kon <> "))"
  where
    kon = toGremlin $ gKontrolFinTemp @Filter @AVertex

gKern :: WalkType s => Walk s AVertex AVertex
gKern = gHasLabel skVKern >>> gHas2 skTip "kern"

-- Efika coalesce
gCoalesceEf :: (ToGTraversal g) => [g SideEffect s e] -> Walk SideEffect s e
gCoalesceEf = gCoalesce

sInject :: Greskell a -> Greskell GraphTraversalSource -> GTraversal SideEffect () a
sInject obj font = GTraversal $ unsafeGreskellLazy $ toGremlinLazy font <> ".inject(" <> toGremlinLazy obj <> ")"

sInject0 :: Greskell GraphTraversalSource -> GTraversal SideEffect () Int
sInject0 = sInject 0

gToEf :: forall g c s e. ToGTraversal g => g c s e -> AddAnchor s e
gToEf = gTo . (unsafeCoerce @_ @(g Transform s e))

gFromEf :: forall g c s e. ToGTraversal g => g c s e -> AddAnchor s e
gFromEf = gFrom . (unsafeCoerce @_ @(g Transform s e))

gPorCxiu ::
  WalkType v =>
  Maybe (RepeatPos, RepeatEmit v a) ->
  ((forall iu. Walk Transform iu i) -> Walk v a a) ->
  Greskell [i] ->
  Binder (Walk v a a)
gPorCxiu jet f l = do
  et <- newAsLabel
  pure $
    gRepeat
      (Just $ RepeatLabel $ unAsLabel et)
      (gTimes $ unsafeGreskell $ toGremlin l <> ".size")
      jet
      (f $ unsafeWalk "map" ["{ it -> " <> toGremlin l <> "[it.loops(" <> toGremlin et <> ")]}"])

dbTr :: Has ServilKnt sig m => ReaderC Client IO a -> m (Maybe a)
dbTr f = do
  gr <- asks akirGr
  sekuraION "db" $ withResource gr (`runReader` f)

gPropertyW :: Element e => Key e v -> Walk Transform e v -> Walk SideEffect e e
gPropertyW k v = unsafeWalk "property" [toGremlin k, toGremlin v]

gHasIntId :: (WalkType c, Lift Transform c) => Greskell Int -> Walk c s s
gHasIntId t = unsafeWalk "hasId" [toGremlin t]

gAgg :: AsLabel [a] -> Walk Transform a a
gAgg t = unsafeWalk "aggregate" [toGremlin t]

gWhereW :: WalkType c => Walk Transform s e -> Walk c s s
gWhereW v = unsafeWalk "where" [toGremlin v]

gSelectMap :: Text -> Walk Transform (PMap Single GValue) a
gSelectMap x = unsafeWalk "select" ["\"" <> x <> "\""]

gSend ::
  (ToGreskell g, r ~ GreskellReturn g, AsIterator r, v ~ IteratorItem r, FromGraphSON v) =>
  Client ->
  g ->
  Maybe Object ->
  IO (ResultHandle v)
gSend kl g a = f (toGremlin g) $ submit kl g a
  where
    f = const id

-- {-# DEPRECATED #-}
gSendM ::
  forall g r v sig m.
  (ToGreskell g, r ~ GreskellReturn g, AsIterator r, v ~ IteratorItem r, FromGraphSON v, Has (Reader Client :+: LiftIO) sig m) =>
  Binder g ->
  m (ResultHandle v)
gSendM b = do
  kl <- ask
  let (g, ald) = runBinder b
  sendIO $ gSend kl (unsafeGreskell @r $ "def nun = Instant.now().getEpochSecond(); " <> toGremlin g) (Just ald)

gSendM' ::
  forall g r v sig m finr.
  (ToGreskell g, r ~ GreskellReturn g, AsIterator r, v ~ IteratorItem r, FromGraphSON v, Has (Reader Client :+: LiftIO) sig m) =>
  Binder (g, ReaderC v Maybe finr) ->
  m (Maybe finr)
gSendM' b = do
  kl <- ask
  let ((g, rezf), ald) = runBinder b
  rezM <-
    sendIO $
      gUnRez
        =<< gSend kl (unsafeGreskell @r $ "def nun = Instant.now().getEpochSecond(); " <> toGremlin g) (Just ald)
  pure do
    rez <- rezM
    runReader rez rezf

gUnRez :: Has LiftIO sig m => ResultHandle v -> m (Maybe v)
gUnRez x = sendIO $ nextResult x

gMultRez :: Has LiftIO sig m => ResultHandle v -> m (Vector v)
gMultRez = sendIO . slurpResults

gIgnorRez :: Has LiftIO sig m => ResultHandle v -> m ()
gIgnorRez = sendIO . drainResults

alLabel :: Key s e -> AsLabel e
alLabel = AsLabel . unKey

gTextFuzzy :: Element e => Key e Text -> Greskell Text -> Walk Transform e e
gTextFuzzy nom arg = gHas2P nom $ unsafeFunCall "textFuzzy" [toGremlin arg]

gTextRegex :: Element e => Key e Text -> Greskell Text -> Walk Transform e e
gTextRegex nom arg = gHas2P nom $ unsafeFunCall "textRegex" [toGremlin arg]

gSe :: forall c v s. (Split c c, WalkType c, Lift Transform c) => Greskell v -> Walk c s s -> Walk c s s
gSe x f = gChoose3 @_ @c (gConstant x >>> gNot @_ @Filter (gIs $ unsafeGreskell "null")) f gIdentity

-- FARENDE: TemplateHaskell
-- SKEMO
skFinTemp :: Element e => Key e Int
skFinTemp = "fin-temp"

skTip :: Element e => Key e Text
skTip = "_tip"

skEEnsalutisKiel :: Greskell Text
skEEnsalutisKiel = "ensalutis-kiel"

skVEnsalutKonfirm :: Greskell Text
skVEnsalutKonfirm = "ensalut-konfirm"

skRetposxt :: Key AVertex Text
skRetposxt = "retposxt"

skKod :: Key AVertex Text
skKod = "kod"

skVUzant :: Greskell Text
skVUzant = "uzant"

skEUzantbild :: Greskell Text
skEUzantbild = "uzantbild"

skEFontbild :: Greskell Text
skEFontbild = "fontbild"

skVUzantnom :: Greskell Text
skVUzantnom = "uzantnom"

skEIndikas :: Greskell Text
skEIndikas = "indikas"

skSuprX :: Key AEdge Int
skSuprX = "suprx"

skSuprY :: Key AEdge Int
skSuprY = "supry"

skMalsuprX :: Key AEdge Int
skMalsuprX = "malsuprx"

skMalsuprY :: Key AEdge Int
skMalsuprY = "malsupry"

skNom :: Key AVertex Text
skNom = "nom"

skPlenNom :: Key AVertex Text
skPlenNom = "plen-nom"

skPri :: Key AVertex Text
skPri = "pri"

skVEnsalut :: Greskell Text
skVEnsalut = "ensalut"

skJxeton :: Key AVertex Text
skJxeton = "jxeton"

skVKern :: Greskell Text
skVKern = "kern"

skVDos :: Greskell Text
skVDos = "dos"

skEEnhavas :: Greskell Text
skEEnhavas = "enhavas"

skVojNom :: Key AEdge Text
skVojNom = "voj-nom"

skEAgant :: Greskell Text
skEAgant = "agant"

skHeredasAgant :: Key AEdge Bool
skHeredasAgant = "heredas-agant"

skEKreis :: Greskell Text
skEKreis = "kreis"

skKreiTemp :: Key AVertex Int
skKreiTemp = "krei-temp"

skEEnhavasVersion :: Greskell Text
skEEnhavasVersion = "enhavas-version"

skVArtikolVersio :: Greskell Text
skVArtikolVersio = "artikol-versio"

skTekst :: Key AVertex Text
skTekst = "tekst"

skVEtiked :: Greskell Text
skVEtiked = "etiked"

skEEtikedas :: Greskell Text
skEEtikedas = "etikedas"

skTitol :: Key AVertex Text
skTitol = "titol"

skVDosier :: Greskell Text
skVDosier = "dosier"

skDosid :: Key AVertex Text
skDosid = "dosid"

skDostip :: Key AVertex Text
skDostip = "dostip"

skDosFontRetej :: Key AVertex Text
skDosFontRetej = "dos-font-retej"

skDosFontVoj :: Key AVertex Text
skDosFontVoj = "dos-font-voj"

skWikidotVer :: Key AVertex Int
skWikidotVer = "wikidot-ver"

class MontrGremlinTip a where
  montrGremlinTip :: Text

instance MontrGremlinTip Text where
  montrGremlinTip = "String"

instance MontrGremlinTip Int where
  montrGremlinTip = "Integer"

instance MontrGremlinTip Bool where
  montrGremlinTip = "Boolean"

datumbazSkemKreist :: Text
datumbazSkemKreist =
  malfermi
    <> property @AVertex skFinTemp
    <> property @AVertex skTip
    -- DOSIEROJ
    <> edge skVDosier
    <> property skDosid
    <> property skDostip
    <> property skDosFontRetej
    <> property skDosFontVoj
    -- UZANTOJ
    <> vertex skVEnsalutKonfirm
    <> property skRetposxt
    <> property skKod
    <> vertex skVEnsalut
    <> edge skEEnsalutisKiel
    <> property skJxeton
    <> vertex skVUzant
    <> property skNom
    <> property skPlenNom
    <> property skPri
    <> edge skEUzantbild
    <> vertex skVUzantnom
    <> edge skEIndikas
    <> edge skEFontbild
    <> property skSuprX
    <> property skSuprY
    <> property skMalsuprX
    <> property skMalsuprY
    -- VOJOJ
    <> vertex skVKern
    <> vertex skVDos
    <> edge skEEnhavas
    <> edge skEAgant
    <> property skHeredasAgant
    <> property skVojNom
    -- ARTIKOLOJ
    <> edge skEKreis
    <> edge skEEnhavasVersion
    <> vertex skVArtikolVersio
    <> property skWikidotVer
    -- <> property "tekst" "String"
    -- ETIKEDOJ
    <> vertex skVEtiked
    <> edge skEEtikedas
    <> property skTitol -- ARTIKOLOJ kaj ETIKEDOJ
    <> fermi
    <> malfermi
    <> "try { mgmt.buildIndex('perNom', Vertex.class).addKey(mgmt.getPropertyKey('nom'), Mapping.STRING.asParameter()).buildMixedIndex('search') } catch(e) {}\n"
    <> indeksV skJxeton
    <> indeksV skTip
    <> indeksV skDosid
    <> fermi
  where
    indeksV :: Key AVertex a -> Text
    indeksV = indeks "Vertex"
    indeks tip k = "try { mgmt.buildIndex('per" <> t <> "', " <> tip <> ".class).addKey(mgmt.getPropertyKey('" <> t <> "')).buildCompositeIndex() } catch(e) {}\n"
      where
        t = unKey k
    malfermi = "mgmt = graph.openManagement()\n"
    fermi = "mgmt.commit()\n"
    fr t rest n =
      let op = t <> "(" <> n <> ")"
       in "if (mgmt.get" <> op <> " == null) mgmt.make" <> op <> rest <> ".make()\n"
    vertex = fr "VertexLabel" "" . toGremlin
    edge = fr "EdgeLabel" "" . toGremlin
    property :: forall e v. MontrGremlinTip v => Key e v -> Text
    property = fr "PropertyKey" (".dataType(" <> (montrGremlinTip @v) <> ".class)") . toGremlin

-- | sequence kaj .==
serMet :: (Applicative f, KnownSymbol l) => Label l -> f a -> f (Rec (l .== a))
serMet l ag = (l .==) <$> ag

(<+) :: (FreeForall r1, Applicative f) => f (Rec r1) -> f (Rec r2) -> f (Rec (r1 .+ r2))
(<+) = liftA2 (.+)

-- newtype GrafRis = GraphRis (forall o. FromGraphSON o => Risurc o)
data GrafRis s a where
  GrafRis ::
    (ProjectionLike p, ToGreskell p, FromGraphSON (ProjectionLikeEnd p)) =>
    AsLabel (ProjectionLikeEnd p) ->
    p ->
    GrafRis (ProjectionLikeStart p) (ProjectionLikeEnd p)

type ProjRez s a = Compose Binder (Ap (GrafRis s)) a

prjLeg ::
  (ProjectionLike p, ToGreskell p, FromGraphSON (ProjectionLikeEnd p)) =>
  p ->
  ProjRez (ProjectionLikeStart p) (ProjectionLikeEnd p)
prjLeg p = Compose $ newAsLabel <&> \et -> liftAp (GrafRis et p)

-- Uzi <.= anstatau <==
(<.=) ::
  (KnownSymbol l, ProjectionLike p, ToGreskell p, FromGraphSON (ProjectionLikeEnd p)) =>
  Label l ->
  p ->
  ProjRez (ProjectionLikeStart p) (Rec (l .== ProjectionLikeEnd p))
(<.=) l p = (l .==) <$> prjLeg p

-- PARTIAL: Fiaskas je manko de akirataj kampoj
partialProjRez ::
  (Lift Transform w, WalkType w) =>
  GTraversal w () s ->
  ProjRez s a ->
  Binder (GTraversal w () (PMap Single GValue), ReaderC (PMap Single GValue) Maybe a)
partialProjRez origVoj projRez = do
  rezE <- getCompose projRez
  pure $
    let (projListK, projListR) = case runAp_ (\(GrafRis l p) -> [gByL l p]) rezE of
          (x : xs) -> (x, xs)
          _ -> error "Invalid projRez"
     in ( origVoj &. gProject projListK projListR,
          runAp (\(GrafRis l _) -> pMapLeg $ key $ unAsLabel l) rezE
        )

(<==) ::
  (Has (Reader (PMap Single GValue) :+: Empty) sig m, KnownSymbol l, FromGraphSON a) =>
  Label l ->
  Key e a ->
  m (Rec (l .== a))
(<==) l v = (l .==) <$> pMapLeg v

pMapLeg ::
  (Has (Reader (PMap Single GValue) :+: Empty) sig m, FromGraphSON a) =>
  Key e a ->
  m a
pMapLeg v = do
  rez <- ask @(PMap Single GValue)
  either (const empty) pure $ lookupAs v rez

infixl 4 <+

infixl 7 <==

infixl 7 <.=

savDos :: Has ServilKnt sig m => Text -> ByteString -> Binder (Walk SideEffect AVertex AVertex) -> m (Maybe (ElementID AVertex))
savDos dostip enhav tr = runMaybeT do
  let dosid = B64Url.encodeBase64 $ hash enhav
      voj = "konservujo" </> T.unpack dosid
  MaybeT $ sekuraIO do
    createDirectoryIfMissing False "konservujo"
    h <- openBinaryFile voj WriteMode
    BS.hPut h enhav
    hClose h
  tranRez <- dbTr do
    gUnRez =<< gSendM do
      id' <- newBind dosid
      tip' <- newBind dostip
      tr' <- tr
      pure
        ( gFontNun
            & sInject0
            &. gCoalesceEf
              [ gV' [] >>> gHasLabel skVDosier >>> gHas2 skDosid id',
                gAddV' skVDosier >>> gProperty skDosid id' >>> gProperty skDostip tip' >>> tr'
              ]
            &. gElementMap mempty
        )
  case tranRez of
    Just (Just rez) -> pure $ P.fromJust $ P.fromJust $ alMaybe $ lookupAs @(AsLabel (Maybe (ElementID AVertex))) "id" rez
    _ -> sekuraIO (removeFile voj) *> empty

akirDos ::
  Has ServilKnt sig m =>
  Binder (GTraversal Transform () AVertex, ProjRez AVertex ald) ->
  m (Either () (Maybe ((Text, BSL.ByteString), ald)))
akirDos pet = runError $ runMaybeT do
  (rez, ald) <-
    MaybeT $
      levM () =<< dbTr do
        gSendM' do
          (tr', aldPrj) <- pet
          partialProjRez
            tr'
            ( (,)
                <$> ( #dosid
                        <.= skDosid
                        <+ #dostip
                        <.= skDostip
                    )
                <*> aldPrj
            )

  d <- levM () =<< sekuraIO (BSL.readFile $ "konservujo" </> T.unpack (rez .! #dosid))
  pure ((rez .! #dostip, d), ald)

akirDos' ::
  Has ServilKnt sig m =>
  Binder (GTraversal Transform () AVertex) ->
  m (Either () (Maybe (Text, BSL.ByteString)))
akirDos' tr = ((fst <$>) <$>) <$> akirDos ((,pure ()) <$> tr)
