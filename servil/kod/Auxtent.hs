module Auxtent where

import Bild
import Control.Applicative
import Control.Carrier.Error.Either
import Crypto.Random
import Data.Aeson
import qualified Data.ByteString.Base64 as B64
import qualified Data.ByteString.Base64.URL as B64Url
import qualified Data.ByteString.Lazy as BSL
import qualified Data.ByteString.Lazy.Base64 as BL64
import Data.Greskell.GTraversal.Gen
import Data.Vec.DataFamily.SpineStrict (Vec (VNil, (:::)))
import Datum
import Db
import Event
import Lingvar
import Network.DNS (Domain, ResolvSeed, lookupMX, withResolver)
import Network.HTTP.Types.Status
import Network.Mail.Mime
import qualified RIO.HashMap as HM
import qualified RIO.HashSet as HS
import qualified RIO.List as L
import qualified RIO.Partial as P
import qualified RIO.Text as T
import qualified RIO.Text.Lazy as TL
import SPrelude
import Text.Email.Validate

-- FARENDE: Klarigi enErar'ojn
-- FARENDE: iuFunkcio = levM?
-- FARENDE: Uzu DbPet

cxuServiloEkzist :: ResolvSeed -> Domain -> IO Bool
cxuServiloEkzist sem dom =
  withResolver sem $ \r -> do
    m <- lookupMX r dom
    return $
      case m of
        Right x
          | any ((/= ".") . fst) x -> True
        _ -> False

posxtu :: Has ServilKnt sig m => Text -> Text -> (Mail -> Mail) -> m (Maybe ())
posxtu host subjekto msgPlenigilo = do
  vojAlSm <- asks akirSendmail
  let senTema =
        msgPlenigilo $
          emptyMail $
            Address
              { addressName = Just "Obskurativ",
                addressEmail = "obs@" <> host
              }
      msg =
        senTema {mailHeaders = ("Subject", subjekto) : mailHeaders senTema}
  (P.fromJust -> finmsg) <- sekuraIO $ renderMail' msg
  sekuraIO $ sendmailCustom vojAlSm ["-t"] finmsg

auxtent :: EventTraktil RequestEvent
auxtent = EventTraktil ["aux", "pet"] \pet -> apiRespond do
  retposxt <- akirKamp pet "email"
  vRetposxt <- case validate (encodeUtf8 retposxt) of
    Left _ -> throwError $ petErar #invalidEmail ()
    Right x -> pure x
  servil <- ask
  (fromMaybe True -> cxuEkz) <- sekuraIO $ cxuServiloEkzist (akirDNSSem servil) (domainPart vRetposxt)
  unless cxuEkz $ throwError $ petErar #invalidEmail ()
  kod <- registriSalut retposxt
  (tsal, (tsubj, t0 ::: t1 ::: t2 ::: t3 ::: VNil)) <-
    traduki (getLanguages pet) $
      tKuntDe "servil.retmsg" $
        tpet "salut" `kajtpet` tKuntDe "ensaluti" (tpet "temo" `kajtpet` tpetPartoj)
  let ligil = "https://" <> getHost pet <> "/-/aux/kod/" <> kod
  levM enErar
    =<< posxtu
      (getHost pet)
      tsubj
      ( \x ->
          x
            { mailTo =
                [Address {addressName = Nothing, addressEmail = retposxt}],
              mailParts =
                [ [ htmlPart $
                      TL.fromStrict $
                        T.concat
                          [ tsal,
                            " ",
                            t0,
                            "<br><br>",
                            t1,
                            " <a href=\"",
                            ligil,
                            "\">",
                            ligil,
                            "</a>",
                            "<br>",
                            t2,
                            "<br><br>",
                            t3
                          ]
                  ]
                ]
            }
      )
  where
    registriSalut :: Has (ServilKnt :+: Error RequestErr) sig m => Text -> m Text
    registriSalut retposxt = do
      (B64Url.encodeBase64 . P.fromJust -> kod) <- sekuraIO $ getRandomBytes 192
      _ <-
        levM enErar =<< dbTr do
          gUnRez =<< gSendM do
            retposxt' <- newBind retposxt
            kod' <- newBind kod
            pure
              ( gFontNun
                  & sInject0
                  &. gCoalesceEf
                    [ gV' [] >>> gHasLabel skVEnsalutKonfirm >>> gHas2 skRetposxt retposxt',
                      gAddV' skVEnsalutKonfirm >>> gProperty skRetposxt retposxt'
                    ]
                  &. gProperty skKod kod'
                  &. gProperty skFinTemp (unsafeGreskell "Instant.now().getEpochSecond()+22*60")
              )
      pure kod

auxFinTemp :: Greskell Int
auxFinTemp = unsafeGreskell "Instant.now().getEpochSecond()+45*24*60*60"

ensalut :: EventTraktil RequestEvent
ensalut = EventTraktil ["aux", "kod"] \pet -> apiRespond do
  kod <- akirKamp pet "code"
  (B64Url.encodeBase64 . P.fromJust -> jxeton) <- sekuraIO $ getRandomBytes 320
  resp <-
    levM enErar =<< dbTr do
      gUnRez =<< gSendM do
        kod' <- newBind kod
        jxeton' <- newBind jxeton
        let konf = "konfirm"
            kr =
              gAddV' skVEnsalut
                >>> gProperty skFinTemp auxFinTemp
                >>> gProperty skJxeton jxeton'
                >>> gAs "ensalut"
                >>> gCoalesceEf
                  [ gV' [] >>> gHasLabel skVUzant >>> gWhereP1 (pEq konf) (Just "retposxt"),
                    gAddV' skVUzant >>> gPropertyW skRetposxt (gSelect1 konf >>> gValues ["retposxt"])
                  ]
                >>> gAddE' skEEnsalutisKiel (gFrom $ tW $ gSelect1 "ensalut")
        pure
          ( gFontNun
              & sV' []
              &. gHasLabel skVEnsalutKonfirm
              &. gHas2 skKod kod'
              &. gChoose3 @_ @Transform
                gId
                (gAs konf >>> kr >>> gConstant true)
                (gConstant false)
          )
  if resp == Just True
    then pure jxeton
    else throwError $ petErar #invalidCode ()

type AuthProfile = Rec ("name" .== Text .+ "fullName" .== Text .+ "about" .== Text)

type Auth = Rec ("id" .== ElementID AVertex .+ "profile" .== Maybe AuthProfile)

auxStatus :: Has (ServilKnt :+: Error RequestErr) sig m => RequestEvent -> m (Maybe Auth)
auxStatus pet = do
  jxeton <- akirKamp pet "token"
  resp <-
    levM enErar =<< dbTr do
      gUnRez =<< gSendM do
        jxeton' <- newBind jxeton
        pure
          ( gFontNun
              & sV' @SideEffect []
              &. gHasLabel skVEnsalut
              &. gHas2 skJxeton jxeton'
              &. gProperty skFinTemp auxFinTemp
              &. gOutE' [skEEnsalutisKiel]
              &. gInV'
              &. gProject
                (gByL "id" $ tW gId)
                [ gByL "fullName" skPlenNom,
                  gByL "about" skPri,
                  gByL "name" (tW $ gInE' [skEIndikas] >>> gOutV' >>> gValues [skNom])
                ]
          )
  pure do
    rez <- resp
    runReader rez $
      (#id <== "id")
        <+ ( (#profile .==)
               <$> runEmpty
                 ( #name
                     <== "name"
                     <+ #fullName
                     <== "fullName"
                     <+ #about
                     <== "about"
                 )
           )

akirAuxStatus :: EventTraktil RequestEvent
akirAuxStatus = EventTraktil ["aux", "status"] (apiRespond <<< auxStatus)

plenNomLim :: Int
plenNomLim = 30

nomMin :: Int
nomMin = 3

priLim :: Int
priLim = 512

avatarLimMB :: Int
avatarLimMB = 5

uzAnstDos ::
  Has (IIO :+: Error RequestErr) sig m =>
  m (HashMap Text Text)
uzAnstDos = do
  dos <- levM enErar =<< sekuraIO (BSL.readFile "uzantnom-anst.json")
  levE $ alEither enErar $ decode' dos

type AuxAgord =
  Rec
    ( "replaces" .== HashMap Text Text
        .+ "fullNameLimit" .== Int
        .+ "nameMin" .== Int
        .+ "aboutLimit" .== Int
        .+ "avatarLimitMB" .== Int
        .+ "imageMIME" .== [Text]
    )

auxAgord :: EventTraktil RequestEvent
auxAgord = EventTraktil ["aux", "agord"] \_ -> apiRespond @AuxAgord do
  anst <- uzAnstDos
  pure
    ( #replaces .== anst
        .+ #fullNameLimit .== plenNomLim
        .+ #aboutLimit .== priLim
        .+ #avatarLimitMB .== avatarLimMB
        .+ #imageMIME .== permBildoj
        .+ #nameMin .== nomMin
    )

type BildFont = Rec ("base64" .== Text .+ "mime" .== Text)

type Bild =
  Var
    ( "new" .== (BildFont, BildPart)
        .+ "newPart" .== BildPart
        .+ "keep" .== ()
        .+ "null" .== ()
    )

uzBildPlen ::
  Has (ServilKnt :+: Error RequestErr) sig m =>
  Binder (GTraversal Transform () AVertex) ->
  m (Maybe ((Text, BSL.ByteString), (ElementID AVertex, BildPart)))
uzBildPlen uzId =
  (levM enErar . alMaybe) =<< akirDos do
    uzId' <- uzId
    fb <- newAsLabel
    pure
      ( uzId'
          &. gHasLabel skVUzant
          &. gOutE' [skEUzantbild]
          &. gInV'
          &. gOutE' [skEFontbild]
          &. gAs fb
          &. gInV',
        let a kampNom = tW $ gSelect1 fb >>> gValues [kampNom]
         in (,)
              <$> prjLeg (tW gId)
              <*> ( #topx
                      <.= a skSuprX
                      <+ #topy
                      <.= a skSuprY
                      <+ #bottomx
                      <.= a skMalsuprX
                      <+ #bottomy
                      <.= a skMalsuprY
                  )
      )

type Anst = [([Char], [Char])]

-- FARENDE: Ĉi tiu funkcio konektas al datumbazo multaj fojoj.
gxisKont :: EventTraktil RequestEvent
gxisKont = EventTraktil ["aux", "redakt"] pro
  where
    ordonitAnst :: HashMap Text Text -> Anst
    ordonitAnst en =
      bimap T.unpack T.unpack
        <$> L.sortBy (\(a, _) (b, _) -> compare (T.length b) (T.length a)) (HM.toList en)
    aplAnst :: (?anst :: Anst) => [Char] -> [Char]
    aplAnst = aplAnst' ?anst
      where
        aplAnst' [] [] = []
        aplAnst' [] (x : xs) = x : aplAnst xs
        aplAnst' ((prov, sukc) : provr) v = case L.stripPrefix prov v of
          Just x -> sukc <> aplAnst x
          Nothing -> aplAnst' provr v
    perm :: HashSet Char
    perm = HS.fromList "abcdefghijklmnopqrstuvwxyz0123456789"

    nomPart :: [Char] -> [[Char]]
    nomPart = p' []
      where
        p' nun = \case
          [] -> [L.reverse nun]
          (x : xs)
            | x `HS.member` perm -> p' (x : nun) xs
            | otherwise -> L.reverse nun : nomPart xs
    kreiId x =
      uzAnstDos <&> \anst ->
        let ?anst = ordonitAnst anst
         in x & T.toLower & T.unpack & aplAnst & nomPart & L.filter (/= []) & L.intercalate "-" & T.pack

    k kampAkiril nom taugas =
      kampAkiril nom
        ^| \rez -> unless (taugas rez) $ throwError $ petErar #limitNotSatisfied nom
    avatarLimB = ((avatarLimMB * 1024 * 1024 * 4) `div` 3) + 100
    pro :: Has SpacKnt sig m => RequestEvent -> m RequestSignal
    pro pet = apiRespond do
      aux <-
        auxStatus pet >>= \case
          Nothing -> throwError $ petErar #noAuth ()
          Just x -> pure x
      plenNom <- k (akirKamp pet) "fullName" \x ->
        T.length x <= plenNomLim
      nom <- kreiId plenNom
      unless (T.length nom >= nomMin) $
        throwError $
          petErar #limitNotSatisfied "name"
      pri <- k (akirKamp pet) "about" \x -> T.length x <= priLim

      ekzistas <-
        levM enErar =<< dbTr do
          gUnRez =<< gSendM do
            nom' <- newBind nom
            id' <- newBind (aux .! #id)
            pure
              ( gFontNun
                  & sV' []
                  &. gHasLabel skVUzantnom
                  &. gTextFuzzy skNom nom'
                  &. gOutE' [skEIndikas]
                  &. gInV'
                  &. gNot (tW $ gHasId id')
              )
      when (isJust ekzistas) $ throwError $ petErar #nonUniqueName ()

      bildM :: Bild <- akirKamp pet "avatar"

      -- Left (nuligi?)
      bildId :: Either Bool (ElementID AVertex) <-
        sequence $
          switch bildM $
            let erari = throwError $ petErar #limitNotSatisfied "avatar"
                akirBildet mime enhav bildpart fb = do
                  bildet <- levM (petErar #corruptedFile ()) $ webpBildet mime enhav bildpart
                  levM enErar =<< savDos "image/webp" bildet do
                    fb' <- newBind fb
                    suprX' <- newBind (bildpart .! #topx)
                    suprY' <- newBind (bildpart .! #topy)
                    malsuprX' <- newBind (bildpart .! #bottomx)
                    malsuprY' <- newBind (bildpart .! #bottomy)
                    pure $
                      gSideEffect
                        ( sW $
                            gAddE' skEFontbild (gTo (tW $ gV' [fb']))
                              >>> gProperty skSuprX suprX'
                              >>> gProperty skSuprY suprY'
                              >>> gProperty skMalsuprX malsuprX'
                              >>> gProperty skMalsuprY malsuprY'
                        )
             in #new
                  .== ( \(font :: BildFont, bildpart) -> Right do
                          unless
                            ((font .! #mime) `elem` permBildoj && T.length (font .! #base64) <= avatarLimB)
                            erari
                          case B64.decodeBase64 $ encodeUtf8 $ font .! #base64 of
                            Left _ -> erari
                            Right enhav -> do
                              fb <- levM enErar =<< savDos (font .! #mime) enhav (pure gIdentity)
                              akirBildet (font .! #mime) enhav bildpart fb
                      )
                  .+ #newPart
                    .== ( \bildpart -> Right do
                            ((dostip, dos), (fb, _)) <-
                              levM enErar =<< uzBildPlen do
                                id' <- newBind (aux .! #id)
                                pure (gFontNun & sV' [id'])
                            akirBildet dostip (BSL.toStrict dos) bildpart fb
                        )
                  .+ #keep .== (\_ -> Left False)
                  .+ #null .== (\_ -> Left True)

      levM enErar =<< dbTr do
        gIgnorRez =<< gSendM do
          nom' <- newBind nom
          plenNom' <- newBind plenNom
          pri' <- newBind pri
          uzId' <- newBind (aux .! #id)
          pure
            ( gFontNun
                & sV' @SideEffect [uzId']
                &. gProperty skPlenNom plenNom'
                &. gProperty skPri pri'
                &. gSideEffect (sW $ gInE' [skEIndikas] >>> gProperty skFinTemp greskellNun)
                &. gAddE' skEIndikas (gFromEf (gCoalesceEf [gV' [] >>> gHas2 skNom nom', gAddV' skVUzantnom >>> gProperty skNom nom']))
            )
        gIgnorRez =<< gSendM do
          uzId' <- newBind (aux .! #id)
          nuligBild <- newBind $ fromLeft True bildId
          bildAldTr <- case alMaybe bildId of
            Nothing -> pure id
            Just bildId' -> do
              bildId'' <- newBind bildId'
              pure (&. gSideEffect (sW $ gAddE' skEUzantbild (gTo (tW $ gV' [bildId'']))))
          pure
            ( gFontNun
                & sV' @SideEffect [uzId']
                &. gChoose3
                  (tW $ gConstant nuligBild >>> gIs true)
                  (gSideEffect $ sW $ gOutE' [skEUzantbild] >>> gProperty skFinTemp greskellNun)
                  gIdentity
                & bildAldTr
            )

gUzantPerNom :: Text -> Binder (GTraversal Transform () AVertex)
gUzantPerNom nom = do
  nom' <- newBind nom
  pure
    ( gFontNun
        & sV' []
        &. gHasLabel skVUzantnom
        &. gHas2 skNom nom'
        &. gOutE' [skEIndikas]
        &. gInV'
    )

akirUzBildPlen :: EventTraktil RequestEvent
akirUzBildPlen = EventTraktil ["uzantbild"] \pet -> apiRespond do
  nom <- akirKamp pet "name"
  r <- uzBildPlen $ gUzantPerNom nom
  pure $
    r <&> \((t, v), (_, part)) ->
      #mime .== t
        .+ #base64 .== BL64.encodeBase64 v
        .+ #part .== part

uzAvatar :: EventTraktil AssetEvent
uzAvatar = EventTraktil ["uzantbild"] \(AssetEvent voj) -> case voj of
  [nom] -> do
    r <- akirDos' do
      uz' <- gUzantPerNom nom
      pure
        ( uz'
            &. gOutE' [skEUzantbild]
            &. gInV'
        )
    pure $ case r of
      Left () -> Left status400
      Right Nothing -> Left status404
      Right (Just (t, v)) -> Right (encodeUtf8 t, v)
  _ -> pure $ Left status500
