module Lingvar where

import qualified Control.Carrier.State.Strict as E
import qualified Control.Carrier.Writer.Strict as E
import Control.Effect.Empty
import Data.Aeson (Result (Success), eitherDecodeStrict', fromJSON)
import qualified Data.Attoparsec.Text as AP
import Data.Type.Nat (SNatI)
import qualified Data.Vec.DataFamily.SpineStrict as Vec
import Datum
import Event
import RIO.ByteString (readFile)
import qualified RIO.HashMap as HM
import qualified RIO.HashSet as HS
import qualified RIO.List as L
import qualified RIO.Text as T
import qualified RIO.Text.Partial as TP
import SPrelude
import qualified StmContainers.Map as SM
import System.Directory

setTranslations :: Has FunkKnt sig m => HashMap Lingvo Tradukoj -> m ()
setTranslations tradukoj = do
  servil <- ask
  ini <- ask
  stm $ SM.insert tradukoj ini (akirLingvDat servil)

kernLingv :: EventTraktil InitEvent
kernLingv = EventTraktil () $ \InitEvent -> do
  trd <- E.execWriter $ blok @"fatal" do
    dosuj <- sekuraIOAu @"fatal" (listDirectory "tradukoj")
    forM_ (L.filter (/= "untranslated.json") dosuj) \dos -> blok @"preterdos" do
      l <- sekuraIOAu @"preterdos" $ readFile $ "tradukoj/" <> dos
      let linNom = T.dropSuffix ".json" $ T.pack dos
      case eitherDecodeStrict' l of
        Right (x' :: Tradukoj) ->
          E.tell $ HM.singleton linNom x'
        Left e ->
          inf IErar $ "Error parsing language file: " <> fromString e
  setTranslations trd

traduk :: Has SpacKnt sig m => (Tradukoj -> m ()) -> [Lingvo] -> m ()
traduk f linj = do
  datum <- asks akirLingvDat
  etj <- spacEtend
  forM_ etj \etendajx -> blok @"etendajx" do
    asociitaj <- eti @"etendajx" . levM () =<< stm (SM.lookup etendajx datum) -- UZI NUR ELEKTITAJN LINGVOJn
    forM_ linj \lin -> blok @"lin" do
      trd <- eti @"lin" $ levM () $ HM.lookup lin asociitaj
      lift $ lift $ f trd

tradukCxiuj :: Has SpacKnt sig m => [Lingvo] -> m Tradukoj
tradukCxiuj = E.execWriter . traduk E.tell

tradukNur :: Has SpacKnt sig m => [Netradukita] -> [Lingvo] -> m Tradukoj
tradukNur orPet lin =
  E.execWriter $
    blok @"fin" $
      E.runState orPet $
        traduk
          ( \trdj -> do
              pet <- E.get
              restas <- flip filterM pet \p ->
                case HM.lookup p trdj of
                  Just trd -> E.tell (HM.singleton p trd) >> pure False
                  Nothing -> pure True
              when (null restas) $ eti @"fin" $ throwError ()
              E.put restas
          )
          lin

lingvar :: EventTraktil RequestEvent
lingvar = EventTraktil ["lingvar"] (apiRespond <<< tradukCxiuj <<< getLanguages)

data Interlingv a where
  IPur :: !a -> Interlingv a
  IPet :: !(HashSet Netradukita) -> !((Netradukita -> Maybe Text) -> a) -> Interlingv a
  IApl :: !(Interlingv (a -> b)) -> !(Interlingv a) -> Interlingv b

instance Functor Interlingv where
  fmap f (IPur a) = IPur $ f a
  fmap f (IPet pet akir) = IPet pet $ f . akir
  fmap f (IApl prevf val) = IApl (fmap (f .) prevf) val

instance Applicative Interlingv where
  pure = IPur
  (<*>) = IApl

tKuntDe :: Text -> Interlingv a -> Interlingv a
tKuntDe kunt = tKuntDe'
  where
    tKuntDe' :: Interlingv a -> Interlingv a
    tKuntDe' i@(IPur _) = i
    tKuntDe' (IPet pet kreilo) =
      IPet (HS.map alKuntSpaco pet) (\vrt -> kreilo $ vrt . alKuntSpaco)
    tKuntDe' (IApl a b) = IApl (tKuntDe' a) (tKuntDe' b)
    alKuntSpaco = ((kunt <> ".") <>)

akirVrt :: Text -> (Text -> Maybe Text) -> Text
akirVrt nom vrt = fromMaybe ("[" <> nom <> "]") $ vrt nom

-- | Peti traduko por unu simbolo.
tpet :: Text -> Interlingv Text
tpet nom = IPet (HS.singleton nom) (akirVrt nom)

-- | Peti tradukon por listo de simboloj.
tpetList :: SNatI n => Vec.Vec n Text -> Interlingv (Vec.Vec n Text)
tpetList list =
  IPet (HS.fromList $ Vec.toList list) (\vrt -> (`akirVrt` vrt) <$> list)

-- | Krei liston de tradukoj por elnumeritaj partoj de mesaĝo.
-- Oni prenu nur komenca parto de ĉi tiu listo.
-- @tpetPartoj = tpetList ["0", "1", "2", "3", "4", "5", ...]@
-- @tKuntDe "abc" tpetPartoj = ["abc.0", "abc.1", "abc.2", "abc.3", "abc.4", "abc.5", ...]@
tpetPartoj :: SNatI n => Interlingv (Vec.Vec n Text)
tpetPartoj = tpetList $ Vec.tabulate (tshow . toInteger)

-- | Kombini du petojn
kajtpet :: Interlingv a -> Interlingv b -> Interlingv (a, b)
kajtpet = liftA2 (,)

traduki :: Has SpacKnt sig m => [Lingvo] -> Interlingv a -> m a
traduki linj origPet = do
  let p :: Interlingv b -> (HashSet Netradukita, (Netradukita -> Maybe Text) -> b)
      p = \case
        IPur x -> (HS.empty, const x)
        IPet pet f -> (pet, f)
        IApl f a -> (p f, p a) & (\((p1, f1), (p2, f2)) -> (p1 <> p2, \vortar -> f1 vortar $ f2 vortar))
      (bez, rezf) = p origPet
  trdj <- tradukNur (HS.toList bez) linj
  pure $ rezf \x -> do
    r1 <- x `HM.lookup` trdj
    Success r2 <- pure $ fromJSON r1
    pure r2

linAnaliz :: AP.Parser [Lingvo]
linAnaliz = (>>= malkonkret) <$> AP.sepBy un (AP.char ',')
  where
    un = AP.skipSpace *> AP.takeWhile (\x -> x /= ',' && x /= ';') <* AP.takeWhile (/= ',')
    malkonkret :: Lingvo -> [Lingvo]
    malkonkret l =
      [l] <> case TP.splitOn "-" l of
        x : _ -> [x]
        _ -> []
