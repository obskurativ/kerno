module Pagx where

import Control.Applicative
import Control.Carrier.Error.Either
import qualified Control.Concurrent.STM.TQueue as STM
import qualified Data.ByteString as BS
import Data.ByteString.Builder (Builder)
import Data.Conduit
import qualified Data.Conduit as C
import qualified Data.Conduit.Combinators as C
import Data.Greskell (peObject, unsafeCastKey)
import Data.Greskell.GTraversal.Gen
import Data.Greskell.Graph (Path)
import Data.Pool (withResource)
import Data.Row hiding (empty)
import qualified Data.XML.Types as X
import Datum
import Db
import qualified Evakuil as Ev
import Event
import Network.Greskell.WebSocket.Client
import Network.HTTP.Types.Status
import qualified RIO.HashMap as HM
import RIO.List (lastMaybe, sortBy)
import qualified RIO.Text as T
import SPrelude
import qualified Text.XML.Stream.Render as X

cxesigi :: WalkType c => Walk c a b
cxesigi = unsafeWalk "constant" ["[]"] >>> unsafeWalk "unfold" []

gTrairiVoj ::
  forall v a.
  (Lift Transform v, WalkType v, Split v v) =>
  [Text] ->
  ((forall iu. Walk Transform iu Text) -> Walk v AVertex AEdge) ->
  Maybe (RepeatPos, RepeatEmit v AVertex) ->
  Binder (Walk v a AVertex)
gTrairiVoj voj krei jet = do
  kern <- newAsLabel @AVertex
  dezir <- newAsLabel
  r <-
    newBind voj
      >>= gPorCxiu
        jet
        ( \ind ->
            gCoalesce
              [ liftWalk
                  ( gAs kern
                      >>> ind
                      >>> gProject (gByL (alLabel skVojNom) $ tW gIdentity) []
                      >>> gAs dezir
                      >>> gSelect1 kern
                      >>> gOutE' [skEEnhavas]
                      >>> gWhereP1 (pEq $ AsLabel $ unAsLabel dezir) (Just $ gBy skVojNom)
                  ),
                krei ind
              ]
              >>> gInV
        )
  pure $ gV [] >>> gKern >>> r

-- FARENDE: Movi al datumbazo?
uzantPerJxeton :: Text -> Binder (GTraversal Transform () AVertex)
uzantPerJxeton jxeton = do
  jxeton' <- newBind jxeton
  pure
    ( gFontNun
        & sV' []
        &. gHasLabel skVEnsalut
        &. gHas2 skJxeton jxeton'
        &. gOutE' [skEEnsalutisKiel]
        &. gInV'
    )

cxuAgant :: Has (Reader Client :+: LiftIO) sig m => [Text] -> Binder (GTraversal Transform () AVertex) -> m (Maybe (ElementID AVertex))
cxuAgant voj uzTr =
  gUnRez =<< gSendM do
    uzTr' <- uzTr
    trairitVoj <- gTrairiVoj voj (const cxesigi) gEmitHead
    let uz = "uzant"
    pure
      ( uzTr'
          &. gAs uz
          &. gWhereW
            ( gV' []
                >>> gKern
                >>> trairitVoj
                >>> gTail 1
                >>> gChoose3
                  ( tW $
                      gRepeat
                        Nothing
                        (gUntilHead (tW $ gInE' [skEAgant] >>> gOutV' >>> gWhereP1 (pEq uz) Nothing))
                        Nothing
                        (gInE [skEEnhavas] >>> gHas2 skHeredasAgant true >>> gOutV')
                  )
                  gIdentity
                  cxesigi
            )
          &. gId
      )

akirCxuAgant :: EventTraktil RequestEvent
akirCxuAgant = EventTraktil ["agant"] \pet -> apiRespond do
  jxeton :: Text <- akirKamp pet "token"
  voj :: [Text] <- akirKamp pet "path"
  Spac spac <- ask
  levM enErar =<< dbTr (isJust <$> cxuAgant (spac : voj) (uzantPerJxeton jxeton))

type ArtikolVersio = "title" .== Maybe Text .+ "text" .== Text .+ "tags" .== [Text]

type ArtikolVersio' = ArtikolVersio .+ "wikidotVer" .== Maybe Int

dosLimB :: Int
dosLimB = 5 * 1024 * 1024

-- FARENDE: Elporti kreinton, dosierojn
evakui :: Has (ServilKnt :+: Error RequestErr) sig m => (Text, Text, Int) -> m ([Rec ArtikolVersio'], [((Text, Text), ByteString, Text)])
evakui (retej, artikol, komInd) = nomspac "evakui" do
  konekt <- levM (petErar #badGateway ()) =<< (asks akirWikidotKonekt >>= (\x -> malPigraVal x))
  gr <- asks akirGr
  levM (petErar #internalError ()) =<< sekuraIO do
    versioj <- STM.newTQueueIO
    dosoj <- STM.newTQueueIO
    let kons :: Ev.KonservujTrk
        kons = \case
          (Ev.KonsArtikolon _ meta) ->
            pure
              ( komInd,
                \ver ->
                  atomically $
                    writeTQueue versioj $
                      #title .== Ev.artTitol meta
                        .+ #tags .== Ev.artEtikedoj meta
                        .+ #text .== Ev.verEnhav ver
                        .+ #wikidotVer .== Just (Ev.verId ver)
              )
          Ev.KonsDosieron (dosRetej, dosVoj) -> do
            (fromMaybe False -> konservi) <- withResource gr $ flip runReader do
              gUnRez =<< gSendM do
                dosRetej' <- newBind dosRetej
                dosVoj' <- newBind dosVoj
                pure $
                  gFontNun
                    & sInject0
                    &. gChoose3
                      ( tW $
                          gV' []
                            >>> gHasLabel skVDosier
                            >>> gHas2 skDosFontRetej dosRetej'
                            >>> gHas2 skDosFontVoj dosVoj'
                      )
                      (gConstant false)
                      (gConstant true)
            pure $
              if konservi
                then Just \val tip ->
                  let val' = BS.toStrict val
                   in when (BS.length val' <= dosLimB) $
                        atomically $
                          writeTQueue
                            dosoj
                            ((dosRetej, dosVoj), BS.toStrict val, decodeUtf8Lenient tip)
                else Nothing
          Ev.KonsFadenon _ -> pure \_ _ -> pure ()
    Ev.evakuiPagx kons konekt retej artikol
    l1 <- atomically $ STM.flushTQueue versioj
    l2 <- atomically $ STM.flushTQueue dosoj
    pure (sortBy (\a b -> (a .! #wikidotVer) `compare` (b .! #wikidotVer)) l1, l2)

type PetiPagxVal = Maybe (Rec (ArtikolVersio .+ "cacheId" .== Maybe (ElementID AVertex)))

petiPagx :: (Text -> Maybe Text) -> [EventTraktil RequestEvent]
petiPagx wikidotFontPor = [petiPagxRapid, petiPagxLong]
  where
    akirRapid ::
      Has (SpacKnt :+: Error RequestErr) sig m =>
      RequestEvent ->
      m (Maybe (Rec (ArtikolVersio .+ "cacheId" .== Maybe (ElementID AVertex))), Maybe ([Text], (Text, Text, Int)))
    akirRapid pet = do
      voj <- akirKamp pet "path"
      pM <- petiPagx' voj
      Spac spac <- ask
      pure
        ( (.- #wikidotVer) <$> pM,
          do
            font <- wikidotFontPor spac
            wikidotVoj <- case voj of
              [p1, p2] -> Just $ p1 <> ":" <> p2
              [p1] -> Just p1
              _ -> Nothing
            r <- case pM of
              Nothing -> pure (-1)
              Just p -> p .! #wikidotVer
            pure (voj, (font, wikidotVoj, r))
        )

    rezult ::
      Has (SpacKnt :+: Error RequestErr) sig m =>
      RequestEvent ->
      Bool ->
      (Maybe (ElementID AVertex) -> Maybe PetiPagxVal) ->
      m (Rec ("continue" .== Bool .+ "page" .== Var ("keep" .== () .+ "new" .== PetiPagxVal)))
    rezult pet continue f = do
      petKasxmId <- akirKamp pet "cacheId"
      let rezPagx = case f petKasxmId of
            Nothing -> IsJust #keep ()
            Just page -> IsJust #new page
      pure (#continue .== continue .+ #page .== rezPagx)

    petiPagxRapid = EventTraktil ["pagx", "pet"] \pet -> apiRespond do
      (pM, long) <- akirRapid pet
      rezult pet (isJust long) \petKasxmId ->
        if fromMaybe False ((==) <$> petKasxmId <*> (pM >>= (.! #cacheId)))
          then Nothing
          else Just pM

    petiPagxLong = EventTraktil ["pagx", "pet", "long"] \pet -> apiRespond do
      (voj, font) <- do
        (_, longM) <- akirRapid pet
        levM (petErar #unapplicable ()) longM
      (versioj, dosoj) <- evakui font
      savit <- for versioj $ redaktPagx' (Right "iko") voj
      for_ dosoj \((dosRetej, dosVoj), enhav, tip) ->
        savDos tip enhav do
          dosRetej' <- newBind dosRetej
          dosVoj' <- newBind dosVoj
          pure $ gProperty skDosFontRetej dosRetej' >>> gProperty skDosFontVoj dosVoj'
      let rez = (\x y -> x .- #wikidotVer .+ #cacheId .== y) <$> lastMaybe versioj <*> lastMaybe savit
      rezult pet False \_ -> Just <$> rez

petiPagx' ::
  Has (SpacKnt :+: Error RequestErr) sig m =>
  [Text] ->
  m (Maybe (Rec (ArtikolVersio' .+ "cacheId" .== Maybe (ElementID AVertex)))) -- .+ "creators" .== [(Bool, Text)]))
petiPagx' lokvoj = do
  Spac spac <- ask
  respM <-
    levM enErar =<< dbTr do
      gUnRez =<< gSendM do
        trairitVoj <- gTrairiVoj (spac : lokvoj) (const cxesigi) Nothing
        pure $
          gFontNun
            & sInject0
            &. trairitVoj
            &. gOutE' [skEEnhavasVersion]
            &. gInV
            &. gProject
              (gByL "title" skTitol)
              [ gByL "text" skTekst,
                gByL "tags" (tW $ gInE' [skEEtikedas] >>> gOutV' >>> gValues [skNom] >>> gFold),
                gByL "wikidotVer" skWikidotVer,
                gByL "cacheId" (tW gId)
              ]
  pure do
    resp <- respM
    runReader resp $
      (#title <== "title")
        <+ (#text <== "text")
        <+ (#tags <== "tags")
        <+ (#wikidotVer <== "wikidotVer")
        <+ (#cacheId <== "cacheId")

-- | FARENDE: Nuntempe nur redaktado far Agantoj estas permesata. Subtenendas pli fleksebla sistemo.
redaktPagx :: EventTraktil RequestEvent
redaktPagx = EventTraktil ["pagx", "redakt"] \pet -> apiRespond do
  jxeton <- akirKamp pet "token"
  lokvoj <- akirKamp pet "path"
  enhav :: Rec ArtikolVersio <- akirKamp pet "content"
  -- FARENDE: Servilo forsendis ID de nova artikola versio. Verŝajne ni ne bezonas tion ĉi.
  void $ redaktPagx' (Left jxeton) lokvoj (enhav .+ #wikidotVer .== Nothing)

redaktPagx' ::
  Has (SpacKnt :+: Error RequestErr) sig m =>
  Either Text Text ->
  [Text] ->
  Rec ArtikolVersio' ->
  m (Maybe (ElementID AVertex))
redaktPagx' krudUzant lokvoj enhav = do
  Spac spac <- ask
  let plenvoj = spac : lokvoj
  levM (petErar #noAuth ()) =<< levM enErar =<< (dbTr . runMaybeT) do
    uzant <- MaybeT $ case krudUzant of
      Left jxeton ->
        cxuAgant plenvoj $ uzantPerJxeton jxeton
      Right nom ->
        gUnRez =<< gSendM do
          nom' <- newBind nom
          pure
            ( gFontNun
                & sInject0
                &. gCoalesceEf
                  [ gV' [] >>> gHasLabel skVUzant >>> gHas2 skNom nom',
                    gAddV skVUzant >>> gProperty skNom nom'
                  ]
                &. gId
            )
    gUnRez =<< gSendM do
      uzant' <- newBind uzant
      (gMalkovr -> title') <- newBind $ enhav .! #title
      text' <- newBind $ enhav .! #text
      (gMalkovr -> wikidotVer') <- newBind $ enhav .! #wikidotVer
      etj' <- newBind $ T.toLower <$> enhav .! #tags
      let jeKreo = gProperty skKreiTemp greskellNun >>> gAddE' skEKreis (gFrom $ tW $ gSelect1 "uz")
      trairitVoj <-
        gTrairiVoj
          plenvoj
          ( \ind ->
              gAddE' skEEnhavas (gToEf $ sW $ gAddV skVDos) >>> gProperty skHeredasAgant true >>> gPropertyW skVojNom ind
          )
          Nothing
      pure
        ( gFontNun
            & sV' [uzant']
            &. gAs "uz"
            &. trairitVoj
            &. gAs "art"
            &. gSelect1 "art"
            &. gSideEffect
              (gCoalesceEf [gValues [skKreiTemp], gProperty skWikidotVer (-1) >>> jeKreo >>> gConstant 0])
            &. gSe wikidotVer' (gHas2P skWikidotVer (pLt wikidotVer') >>> gProperty skWikidotVer wikidotVer')
            &. gSideEffect (sW $ gOutE' [skEEnhavasVersion] >>> gProperty skFinTemp greskellNun)
            &. gAddE'
              skEEnhavasVersion
              ( gToEf $
                  sW $
                    gAddV' skVArtikolVersio
                      >>> gProperty skTitol title'
                      >>> gProperty skTekst text'
                      >>> gProperty skWikidotVer wikidotVer'
                      >>> gSideEffect jeKreo
                      >>> gSideEffect
                        ( gAs "versio"
                            >>> gConstant etj'
                            >>> gUnfold
                            >>> gAs "et"
                            >>> gProject (gByL (alLabel skNom) $ tW gIdentity) []
                            >>> gAs "sercx"
                            >>> gCoalesceEf
                              [ gV [] >>> gHasLabel skVEtiked >>> gWhereP1 (pEq "sercx") (Just $ gBy skNom),
                                gAddV skVEtiked >>> gPropertyW skNom (gSelect1 "et")
                              ]
                            >>> gAddE' skEEtikedas (gTo $ tW $ gSelect1 "versio")
                        )
              )
            &. gInV'
            &. gId
        )

gEtikedUzoj :: Walk Transform AVertex Int
gEtikedUzoj = gOutE' [skEEtikedas] >>> gInV' >>> gInE' [skEEnhavasVersion] >>> gOutV' >>> gDedup Nothing >>> gCount

sercxEtj :: EventTraktil RequestEvent
sercxEtj = EventTraktil ["pagx", "etiked", "sercx"] \pet -> apiRespond @[Rec ("name" .== Text .+ "uses" .== Int)] do
  en :: Text <- akirKamp pet "query"
  r <-
    levM enErar =<< dbTr do
      gMultRez =<< gSendM do
        simpl <- newBind en
        let part = T.take ((T.length en `div` 2) + 1) en
        regeks <- newBind $ ".*(" <> en <> "|" <> part <> ").*" -- FARENDE: *teorie* ebligas uzanton uzi regeksojn por serĉi.
        pure
          ( gFontNun
              & sV' @Transform []
              &. gHasLabel skVEtiked
              &. gOr [gTextFuzzy skNom simpl, gTextRegex skNom regeks]
              &. gProject
                (gByL "nom" skNom)
                [ gByL
                    "uzoj"
                    gEtikedUzoj
                ]
              &. gWhereW (gSelectMap "uzoj" >>> gIsP (pNeq @(P Int) 0))
              &. gOrder [gBy2 @(Key _ Int) "uzoj" oDesc]
          )
  levM enErar $
    for (SPrelude.toList r) $
      flip runReader $
        (#name <== "nom")
          <+ (#uses <== "uzoj")

uzojDeEtj :: EventTraktil RequestEvent
uzojDeEtj = EventTraktil ["pagx", "etiked", "uzoj"] \pet -> apiRespond @(HashMap Text Int) do
  en :: [Text] <- akirKamp pet "tags"
  r <-
    levM enErar =<< dbTr do
      gMultRez =<< gSendM do
        en' <- newBind en
        let noml = alLabel skNom
            et = "et"
        pure $
          gFontNun
            & sInject en'
            &. gUnfold
            &. gAs et
            &. gProject (gByL noml $ tW gIdentity) []
            &. gAs "sercx"
            &. gProject
              (gByL "nom" (tW $ gSelect1 et))
              [ gByL
                  "uzoj"
                  ( tW $
                      gV' []
                        >>> gHasLabel skVEtiked
                        >>> gWhereP1 (pEq "sercx") (Just $ gBy skNom)
                        >>> gEtikedUzoj
                  )
              ]
  levM enErar $
    HM.fromList <$> do
      for (SPrelude.toList r) $
        flip runReader ((,) <$> pMapLeg "nom" <*> pMapLeg "uzoj")

type ListPagxElektilR =
  ( "category" .== Maybe Text
      .+ "tags" .== Maybe (Rec ("or" .== Maybe (NonEmpty Text) .+ "and" .== [Text] .+ "not" .== [Text]))
      .+ "created_by" .== Maybe Text
      .+ "created_at" .== Maybe (Int, Int)
      .+ "updated_at" .== Maybe (Int, Int)
      .+ "order" .== Maybe (Rec ("by" .== Text .+ "asc" .== Bool))
      .+ "offset" .== Maybe Int
      .+ "limit" .== Maybe Int
  )

type ListPagxRez =
  Rec
    ( "relative_path" .== [Text]
        .+ "title" .== Text
        .+ "tags" .== [Text]
        .+ "created_by" .== Text
        .+ "created_at" .== Int
        .+ "updated_by" .== Text
        .+ "updated_at" .== Int
    )

listPagx :: EventTraktil RequestEvent
listPagx = EventTraktil ["pagx", "list"] \pet -> apiRespond @[ListPagxRez] do
  elektil :: Rec ListPagxElektilR <- akirKamp pet "selector"
  let unSekv = \case
        Just (x, y) -> (Just x, Just y)
        Nothing -> (Nothing, Nothing)
      (kreiT1, kreiT2) = unSekv $ elektil .! #created_at
      (gxisT1, gxisT2) = unSekv $ elektil .! #updated_at
  Spac spac <- ask
  resp <-
    levM enErar =<< dbTr do
      gMultRez =<< gSendM do
        spac' <- newBind spac
        (gMalkovr -> kat') <- newBind $ elektil .! #category
        (gMalkovr -> etiAu', etiKaj', etiNe') <-
          maybe
            (pure (greskellNull, greskellNull, greskellNull))
            (\etl -> (,,) <$> newBind (etl .! #or) <*> newBind (etl .! #and) <*> newBind (etl .! #not))
            (elektil .! #tags)
        (gMalkovr -> kreitDe') <- newBind $ elektil .! #created_by
        (kreiT1', kreiT2') <- (,) <$> newBind kreiT1 <*> newBind kreiT2
        (gxisT1', gxisT2') <- (,) <$> newBind gxisT1 <*> newBind gxisT2
        (ordLau', ordSupr') <-
          maybe
            (pure (greskellNull, greskellNull))
            (\ord -> (,) <$> newBind (ord .! #by) <*> newBind (ord .! #asc))
            (elektil .! #order)
        desx' <- newBind $ fromMaybe 0 $ elektil .! #offset
        lim' <- newBind $ min 250 (fromMaybe 20 $ elektil .! #offset)
        let lastVersio = gOutE' [skEEnhavasVersion] >>> gInV'
        -- FARENDE: Inspekti rapidecon.
        pure $
          gFontNun
            & sV' @Transform []
            &. gKern
            &. gOutE' [skEEnhavas]
            &. gHas2 skVojNom spac'
            &. gInV'
            &. gRepeat
              Nothing
              Nothing
              (gEmitTailT $ gWhereW @Filter (gOutE' [skEEnhavasVersion]))
              (gOutE' [skEEnhavas] >>> gInV)
            &. gProject
              (gByL "relative_path" $ tW $ gPathBy (gBy skVojNom) [] >>> unsafeWalk "map" ["{x -> x.get().objects().findAll({y -> y != null})}"])
              [ gByL "title" $ tW $ lastVersio >>> gValues [skTitol],
                gByL "tags" $ tW $ lastVersio >>> gInE' [skEEtikedas] >>> gOutV >>> gValues [skNom] >>> gFold,
                gByL "created_at" $ tW $ gValues [skKreiTemp],
                gByL "updated_at" $ tW $ lastVersio >>> gValues [skKreiTemp],
                gByL "created_by" $ tW $ gInE' [skEKreis] >>> gOutV >>> gValues [skNom],
                gByL "updated_by" $ tW $ lastVersio >>> gInE' [skEKreis] >>> gOutV' >>> gValues [skNom]
              ]
            &. gSe kat' (gWhereW $ gSelectMap "relative_path" >>> unsafeWalk "map" ["{x -> x.get()[-2]}"] >>> gIs kat')
            &. gSe
              etiAu'
              ( gWhereW $
                  gSelectMap "tags"
                    >>> unsafeWalk
                      "map"
                      ["{x -> x.get().any({y -> y in " <> toGremlin etiAu' <> "})}"]
                    >>> gIs true
              )
            &. gSe
              etiKaj'
              ( gWhereW $
                  gSelectMap "tags"
                    >>> unsafeWalk
                      "map"
                      ["{x -> " <> toGremlin etiKaj' <> ".every({y -> y in x.get()}) && " <> toGremlin etiNe' <> ".every({y -> !(y in x.get())})}"]
                    >>> gIs true
              )
            &. gSe kreitDe' (gWhereW $ gSelectMap "created_by" >>> gIs kreitDe')
            &. gSe kreiT1' (gWhereW $ gSelectMap "created_at" >>> gIsP (pInside kreiT1' kreiT2'))
            &. gSe gxisT1' (gWhereW $ gSelectMap "updated_at" >>> gIsP (pInside gxisT1' gxisT2'))
            &. gSe
              ordLau'
              ( unsafeWalk "order" []
                  >>> unsafeWalk
                    "by"
                    [ toGremlin ordLau' <> "?: \"id\"",
                      toGremlin ordSupr' <> " ? Order.asc : Order.desc"
                    ]
              )
            &. gSkip desx'
            &. gLimit lim'
  levM enErar do
    for (SPrelude.toList resp) $
      flip runReader $
        (#relative_path <== "relative_path")
          <+ (#title <== "title")
          <+ (#tags <== "tags")
          <+ (#created_by <== "created_by")
          <+ (#created_at <== "created_at")
          <+ (#updated_by <== "updated_by")
          <+ (#updated_at <== "updated_at")

sxargEksterRis :: EventTraktil AssetEvent
sxargEksterRis = EventTraktil ["ekster-risurc"] \(AssetEvent nom) -> case nom of
  (retej : (T.intercalate "/" -> voj)) -> do
    r <- akirDos' do
      retej' <- newBind retej
      voj' <- newBind voj
      pure
        ( gFontNun
            & sV' []
            &. gHasLabel skVDosier
            &. gHas2 skDosFontRetej retej'
            &. gHas2 skDosFontVoj voj'
        )
    pure $ case r of
      Left () -> Left status400
      Right Nothing -> Left status404
      Right (Just (t, v)) -> Right (encodeUtf8 t, v)
  _ -> pure $ Left status500

sitemap :: Has (ServilKnt :+: Error ()) sig m => Text -> m Builder
sitemap host = do
  -- FARENDE: redaktempon
  levM () =<< dbTr do
    rez :: ResultHandle (Path (Maybe Text)) <-
      gSendM $
        pure $
          gFontNun
            & sV @_ @Transform []
            &. gKern
            &. gRepeat Nothing Nothing gEmitTail (gOutE' [skEEnhavas] >>> gInV)
            &. gWhereW (gOutE' [skEEnhavasVersion])
            &. gPathBy (gBy (unsafeCastKey skVojNom)) []
    let vojoj =
          lift (gUnRez rez) >>= \case
            Nothing -> pure ()
            Just x -> C.yield (peObject <$> unPath x) *> vojoj
    let eventoj = do
          yield X.EventBeginDocument
          X.tag "urlset" (X.attr "xmlns" "http://www.sitemaps.org/schemas/sitemap/0.9") $
            C.awaitForever \voj ->
              X.tag "url" mempty $
                X.tag "loc" mempty (X.content $ "https://" <> host <> "/" <> T.intercalate "/" (catMaybes voj))

          yield X.EventEndDocument
    runConduit (vojoj .| eventoj .| X.renderBuilder def .| C.fold)
