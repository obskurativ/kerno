{-# OPTIONS_GHC -Wno-orphans #-}

module SPrelude
  ( module RIO,
    module Control.Algebra,
    module Control.Arrow,
    module Control.Category,
    module Control.Effect.Empty,
    module Control.Effect.Reader,
    module Control.Effect.Error,
    module Control.Carrier.Error.Either,
    module Control.Effect.Labelled,
    module Control.Monad.Trans.Maybe,
    module Data.Row,
    module Data.Greskell,
    alEither,
    alMaybe,
    ByteStringLazy,
    blok,
    def,
    EmptyRow,
    eti,
    LiftIO,
    levE,
    levM,
    retry,
    (^|),
    (<<$>>),
    runEmpty,
    runReader,
    sendIO,
    RenvList(akirRenv),
    rl1,
    rlAlList
  )
where

import Control.Algebra
import Control.Arrow
import Control.Carrier.Empty.Maybe
import Control.Carrier.Error.Either
import Control.Carrier.Reader
import Control.Category
import Control.Effect.Empty hiding (guard)
import Control.Effect.Error
import Control.Effect.Labelled
import Control.Effect.Lift
import Control.Effect.Reader
import Control.Monad.STM
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.Aeson.KeyMap (fromHashMap)
import Data.Aeson.Types
import Data.Default
import Data.Greskell (AEdge, AVertex, AsIterator (IteratorItem), AsLabel (..), Binder, Element, ElementID, FromGraphSON, GValue, Greskell, Key (unKey), P, PMap, Single, ToGreskell (GreskellReturn), false, gBy2, newAsLabel, newBind, oDesc, pEq, pGt, pInside, pLt, pNeq, runBinder, toGremlin, toGremlinLazy, true, unPath, unsafeFunCall, unsafeGreskell, unsafeGreskellLazy, (-:))
import Data.List (intercalate)
import Data.Row hiding (Empty, empty)
import qualified Data.Row as R
import qualified Data.Row.Records as RR
import qualified Data.Row.Variants as RV
import RIO hiding (Builder, Handler, Reader, ask, asks, first, id, local, runReader, second, (.), lookup)
import qualified RIO.ByteString.Lazy as BSL

type EmptyRow = R.Empty

type LiftIO = Lift IO

type ByteStringLazy = BSL.ByteString

newtype RenvList a = RenvList { akirRenv :: [a] }

rl1 :: a -> RenvList a
rl1 x = RenvList [x]

rlAlList :: RenvList a -> [a]
rlAlList = reverse <<< akirRenv

instance Semigroup (RenvList a) where
  (RenvList oldL) <> (RenvList novL) = RenvList (novL <> oldL)
instance Monoid (RenvList a) where
  mempty = RenvList mempty


(^|) :: Monad m => m a -> (a -> m ()) -> m a
(^|) ag f = ag >>= (\x -> f x $> x)

infix 2 ^|

(<<$>>) :: (Functor f, Functor g) => (a -> b) -> f (g a) -> f (g b)
(<<$>>) f = ((f <$>) <$>)

infixl 4 <<$>>

blok :: forall label m a. Applicative m => Labelled label (ErrorC ()) m a -> m ()
blok = void . runError . runLabelled

eti :: forall label sub m a. UnderLabel label sub m a -> m a
eti = runUnderLabel

levE :: Has (Throw e) sig m => Either e a -> m a
levE = \case
  Left e -> throwError e
  Right x -> pure x

levM :: Has (Throw e) sig m => e -> Maybe a -> m a
levM e = maybe (throwError e) pure

alEither :: a -> Maybe b -> Either a b
alEither x = \case
  Just y -> Right y
  Nothing -> Left x

alMaybe :: Either a b -> Maybe b
alMaybe = \case
  Left _ -> Nothing
  Right x -> Just x

-- -- https://hackage.haskell.org/package/row-types-aeson-1.0.0.0/docs/src/Data.Row.Aeson.html#line-47
-- instance R.Forall r ToJSON => ToJSON (R.Var r) where
--   toJSON x = object $ foo l
--     where
--       (l, foo) = RV.eraseWithLabels @ToJSON (\v et -> ["tag" .= (et :: Text), "value" .= v]) x

-- instance R.Forall r ToJSON => ToJSON (R.Rec r) where
--   toJSON = Object . fromHashMap . RR.eraseToHashMap @ToJSON toJSON

--   toEncoding = pairs . foldMap (uncurry (&)) . RR.eraseWithLabels @ToJSON (flip (.=))

-- instance (RV.AllUniqueLabels r, RV.Forall r FromJSON) => FromJSON (R.Rec r) where
--   parseJSON (Object o) = do
--     r <- RR.fromLabelsA @FromJSON $ \l -> do
--       x <- o .: fromString (show l)
--       x `seq` pure x
--     r `seq` pure r
--   parseJSON v = typeMismatch msg v
--     where
--       msg = "REC: {" ++ intercalate "," (RR.labels @r @FromJSON) ++ "}"

-- https://hackage.haskell.org/package/row-types-aeson-1.0.0.0/docs/src/Data.Row.Aeson.html#line-47
instance R.Forall r ToJSON => ToJSON (R.Rec r) where
  toJSON = Object . fromHashMap . RR.eraseToHashMap @ToJSON toJSON

  toEncoding = pairs . foldMap (uncurry (&)) . RR.eraseWithLabels @ToJSON (flip (.=))

instance (RV.AllUniqueLabels r, RV.Forall r FromJSON) => FromJSON (R.Rec r) where
  parseJSON (Object o) = do
    r <- RR.fromLabelsA @FromJSON $ \l -> do
      x <- o .: fromString (show l)
      x `seq` pure x
    r `seq` pure r
  parseJSON v = typeMismatch msg v
    where
      msg = "REC: {" ++ intercalate "," (RR.labels @r @FromJSON) ++ "}"

instance R.Forall r ToJSON => ToJSON (R.Var r) where
  toJSON x = object $ foo l
    where
      (l, foo) = RV.eraseWithLabels @ToJSON (\v et -> ["tag" .= (et :: Text), "value" .= v]) x

instance (AllUniqueLabels r, Forall r FromJSON) => FromJSON (Var r) where
  parseJSON (Object o) = RV.fromLabels @FromJSON $ \l -> do
    True <- (show l ==) <$> o .: "tag"
    x <- o .: "value"
    x `seq` pure x
  parseJSON v = typeMismatch msg v
    where
      msg = "VAR: {" ++ intercalate "," (labels @r @FromJSON) ++ "}"

{-
instance Forall r ToJSON => ToJSON (Var r) where
  toJSON v = object [foo l]
    where (l, foo) = Var.eraseWithLabels @ToJSON (\v l -> l .= v) v

instance (AllUniqueLabels r, Forall r FromJSON) => FromJSON (Var r) where
  parseJSON (Object o) = Var.fromLabels @FromJSON $ \ l -> o .: (show' l)
  parseJSON v = typeMismatch msg v
    where msg = "VAR: {" ++ intercalate "," (labels @r @FromJSON) ++ "}"
-}
