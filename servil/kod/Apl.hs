module Main (main, konzol, dev) where

import Auxtent
import Control.Carrier.Reader
import Data.Aeson
import qualified Data.Aeson as Aeson
import Data.Attoparsec.Text
import qualified Data.Binary.Builder as B
import qualified Data.CaseInsensitive as CI
import Data.Greskell
import Data.Pool
import Datum
import Db
import Db.RHM
import Db.RocksDb
import Evakuil (analizPonto, konekti)
import qualified Evakuil as Ev
import Event
import Lingvar
import qualified Network.DNS as DNS
import Network.Greskell.WebSocket as G
import Network.HTTP.Types.Header
import Network.HTTP.Types.Status
import Network.Wai
import Network.Wai.Application.Static
import qualified Network.Wai.Handler.Warp as W
import Pagx
import qualified RIO.HashMap as HM
import qualified RIO.List as L
import qualified RIO.Partial as P
import qualified RIO.Text as T
import SPrelude
import qualified StmContainers.Map as SM
import System.Console.CmdArgs hiding (def)
import System.Directory (setCurrentDirectory)
import System.Which (staticWhichNix)
import WaiAppStatic.Types
import Control.Carrier.Lift (runM)
import Db.Tipoj (Det(..), Statik (..), unsafeSt, DbEvent (..), SText (..), RHM (RHMEmpty), FaldF (..))
import qualified Db.RHM as RHM

-- FARENDE: requestSizeLimitMiddleware
-- FARENDE: Relarĝigi uzantbildojn.

kreiDNSSem :: IO DNS.ResolvSeed
kreiDNSSem = DNS.makeResolvSeed DNS.defaultResolvConf {DNS.resolvTimeout = 1000000}

postuli :: (Has IIO sig m, LabelledMember "fatal" (Error ()) sig) => IO a -> m a
postuli = sekuraIOAu @"fatal"

data PigraValKons a = PigraAkirit a | PigraAkirat | PigraAkirot

pigraVal :: Has UzSTM sig m => IO a -> m (PigraVal a)
pigraVal f = do
  pigr <- stm $ newTVar PigraAkirot
  pure $ PigraVal do
    join $ stm do
      status <- readTVar pigr
      case status of
        PigraAkirit a -> pure $ pure $ Just a
        PigraAkirat -> retry
        PigraAkirot -> do
          writeTVar pigr PigraAkirat
          pure $ nomspac "lazy" do
            rezM <- sekuraIO f
            rez <- case rezM of
              Nothing -> do
                inf IAvert "Error executing lazy IO"
                pure PigraAkirot
              Just x -> pure $ PigraAkirit x
            stm $ writeTVar pigr rez
            pure rezM

kreiServil :: (Has (UzSTM :+: IIO) sig m, LabelledMember "fatal" (Error ()) sig) => String -> Maybe Text -> m Servil
kreiServil dbhost ponto = do
  gr <-
    sekuraIOAu @"fatal" $
      newPool $
        defaultPoolConfig
          (G.connect dbhost 8182)
          G.close
          30
          30
  _ <- sekuraION "create-scheme" $
    withResource gr \s -> do
      G.drainResults
        =<< gSend
          s
          ( gFont
              & sInject0
              &. gCoalesceEf [liftWalk (gV' [] >>> gKern), gAddV' "kern" >>> gProperty skTip "kern"]
          )
          Nothing
      G.drainResults
        =<< G.submitRaw
          s
          datumbazSkemKreist
          Nothing
      -- FARENDE: Indeksoj
      pure ()
  sem <- postuli kreiDNSSem
  lingvDat <- postuli SM.newIO
  traktiloj <- postuli SM.newIO
  let vojAlSm = $(staticWhichNix "sendmail")
  wikidotKonekt <- postuli (traverse analizPonto ponto) >>= pigraVal . konekti
  pure $
    Servil
      { akirGr = gr,
        akirDNSSem = sem,
        akirLingvDat = lingvDat,
        akirSpac =
          HM.fromList
            [ (Spac "-", [Etend "kern", Etend "aprior"]),
              (Spac "obs", [Etend "aprior"]),
              (Spac "backrooms", [Etend "aprior"]),
              (Spac "alliance", [Etend "aprior"]),
              (Spac "wikidot", [Etend "aprior"])
            ],
        akirSendmail = vojAlSm,
        akirTraktil = traktiloj,
        akirWikidotKonekt = wikidotKonekt
      }

dev :: String -> IO ()
dev x = do
  setCurrentDirectory "../risurc"
  lancx (Server x $ Just "127.0.0.1:8118")

konzol :: String -> InfC (ReaderC Servil (STMC SIO)) a -> IO a
konzol db f = do
  setCurrentDirectory "../risurc"
  malSIO $ runM $ malSTMC do
    s <- medio do
      P.fromJust . alMaybe <$> runError do
        runLabelled @"fatal" (kreiServil db $ Just "127.0.0.1:8118")
    medioS s f

data Obskurativ
  = Server {db :: String, proxy :: Maybe Text}
  | WikidotImport {db :: String, website :: Text, threads :: Int, proxy :: Maybe Text}
  deriving (Data, Typeable, Show)

obsDb :: String
obsDb = "127.0.0.1" &= opt ("127.0.0.1" :: Text) &= help "Database address"

obsWikidotImport :: Obskurativ
obsWikidotImport =
  WikidotImport
    { db = obsDb,
      website = Ev.websiteArg,
      threads = Ev.threadsArg,
      proxy = Ev.proxyArg
    }

testDb :: IO ()
testDb = do
  traceShowM =<< testM do
    for_ [1..100] \i -> do
      traceShowM i
      sendEvent $ EvMetHM i $ SText $ tshow i
  void $ testM $ region \(Proxy @s) _ -> do
    for_ [101..1000] \i ->
      sendEvent $ EvMetHM i $ SText $ tshow i
    traceM "fald"
    hm1 <- uzFald @s (Fald (RHMEmpty @() @Int @SText) FaldF)
    traceM "uz"
    traceShowM =<< RHM.lookup 59 hm1
    traceShowM =<< RHM.lookup 156 hm1
  void $ testM $ region \(Proxy @s) _ -> do
    hm1 <- uzFald @s (Fald (RHMEmpty @() @Int @SText) FaldF)
    traceShowM =<< RHM.lookup 956 hm1
    traceShowM =<< RHM.lookup 1000 hm1
    
testM :: ErrorC DbErar (ReaderC DbKonekt (InfC (STMC SIO))) a -> IO ()
testM ag = malSIO $ runM $ malSTMC $ medio $ dbKonekt (UzDet uzDet) uzFaldF \db ->
    void $ runReader db $ runError ag
  where
  uzDet :: forall s sig m a. Has (DbKnt :+: Region s) sig m => Det s a -> m (Din s a)
  uzDet = \case
    DPur a -> pure $ unsafeMalSt @s a
    DDu f -> uzDet @s f <&> \f' -> f' *** f'
    Fib x -> case x of
      0 -> pure 0
      1 -> pure 1
      _ -> traceShowM x *> do
        (+) <$> uzKasxDet @s (Fib (x - 1)) <*> uzKasxDet @s (Fib (x - 2))
    DMet k v hm -> do
      k' <- uzDet k
      v' <- uzDet v
      hm' <- uzDet hm
      RHM.insert (unsafeSt @s k') v' hm'
  uzFaldF = UzFaldF \FaldF en -> \case
    EvMetHM k v -> RHM.insert k v en
    _ -> pure en

main :: IO ()
main = pure ()

-- main =
--   cmdArgs
--     ( modes
--         [ Server
--             { db = obsDb,
--               proxy = Ev.proxyArg
--             },
--           WikidotImport
--             { db = obsDb,
--               website = Ev.websiteArg,
--               threads = Ev.threadsArg,
--               proxy = Ev.proxyArg
--             }
--         ]
--         &= program "obskurativ"
--     )
--     >>= lancx

lancx :: Obskurativ -> IO ()
lancx cmd = malSIO $ runM $ malSTMC $ medio $
  nomspac "init" $
    blok @"fatal" $
      case cmd of
        Server {db, proxy} -> do
          s <- kreiServil db proxy
          runReader s do
            enmetTraktil @InitEvent (Etend "aprior") [kernLingv]
            enmetTraktil @RequestEvent (Etend "aprior") $
              [lingvar, akirCxuAgant, redaktPagx, sercxEtj, uzojDeEtj, listPagx] <> petiPagx \case
                "obs" -> Just "obscurative"
                "backrooms" -> Just "ru-backrooms-wiki"
                "alliance" -> Just "scientific-alliance"
                _ -> Nothing
            enmetTraktil @RequestEvent (Etend "kern") [auxtent, ensalut, akirAuxStatus, auxAgord, gxisKont, akirUzBildPlen]
            enmetTraktil @AssetEvent (Etend "kern") [uzAvatar, sxargEksterRis]

            i <- traktilCxiuj @InitEvent
            runReader (Spac "-") $ forM_ i \(etend, EventTraktil () f) -> runReader etend $ f InitEvent
          inf IInf "Server launched"
          postuli $ W.run 8080 (apl s)
        WikidotImport {db, website, threads, proxy} -> do
          s <- kreiServil db proxy
          inf IInf "Redirecting import request to wikidot-evakuilo..."
          inf IErar "to be filled"

-- FARENDE: cache
-- FARENDE: sendFile?..
apl :: Servil -> Application
apl servil pet respondil = malSIO $ runM $ malSTMC $ medioS servil do
  korpVal <- pigraVal $ mapLeft T.pack . eitherDecode <$> consumeRequestBodyLazy pet
  let kapo = HM.fromList $ requestHeaders pet
      lingv = (<> ["en"]) $ fromMaybe [] do
        krud <- HM.lookup hAcceptLanguage kapo
        Right lin <- pure $ parseOnly linAnaliz (decodeUtf8Lenient krud) -- FARENDE
        pure lin
      host = maybe "example.com" decodeUtf8Lenient $ HM.lookup hHost kapo
      aplFont = HM.lookup (CI.mk "Obs-Apl") kapo == Just "1"
      event =
        RequestEvent
          { getLanguages = lingv,
            getBodyField = \kamp -> do
              (P.fromJust -> korpM) <- malPigraVal korpVal
              case korpM of
                Left e -> throwError $ petErar #parseError e
                Right korp ->
                  case HM.lookup kamp korp of
                    Nothing -> throwError $ petErar #missingField kamp
                    Just x -> pure x,
            getHost = host
          }
  if aplFont
    then do
      r <-
        trkPet
          (\voj voj' -> if voj == voj' then Just event else Nothing)
          (\(RequestSignal' enhav) -> Right ("text/json", Aeson.encode enhav))
      case r of
        Just r' -> pure r'
        Nothing -> nurStatus status500
    else case pathInfo pet of
      ["robots.txt"] ->
        sekuraIOP $
          respondil $
            responseLBS status200 [(hContentEncoding, "text/plain")] $
              fromStrictBytes $
                encodeUtf8 $
                  "User-agent: *\n\
                  \Allow: /\n\n\
                  \Sitemap: https://"
                    <> host
                    <> "/sitemap.xml"
      ["sitemap.xml"] -> do
        rez <- runError $ sitemap host
        sekuraIOP $ respondil $ case rez of
          Left () -> responseBuilder status500 [] B.empty
          Right x -> responseBuilder status200 [(hContentEncoding, "text/xml")] x
      _ -> do
        r <-
          trkPet @AssetEvent
            (\vojPlen knt -> AssetEvent <$> L.stripPrefix knt vojPlen)
            id
        case r of
          Just r' -> pure r'
          Nothing ->
            sekuraIOP $
              staticApp
                dosServilAgord
                pet
                respondil
  where
    sekuraIOP r = P.fromJust <$> sekuraIO r
    -- MaybeT?
    trkPet ::
      forall e sig m.
      (Has ServilKnt sig m, Event e) =>
      ([Text] -> EUzKnt e -> Maybe e) ->
      (ESignal e -> Either Status (ByteString, LByteString)) ->
      m (Maybe ResponseReceived)
    trkPet evKreil resp =
      case pathInfo pet of
        (ns : voj) ->
          runReader (Spac ns) do
            trkj <- traktilSpac @e
            case L.headMaybe $ mapMaybe (\(etend, EventTraktil uzKond f) -> (etend,f,) <$> evKreil voj uzKond) trkj of
              Just (etend, f, event) -> do
                rez <- resp <$> runReader etend (f event)
                case rez of
                  Left status -> lift $ Just <$> nurStatus status
                  Right (tip, enhav) ->
                    sekuraIO $
                      respondil $
                        responseLBS status200 [(hContentEncoding, tip)] enhav
              Nothing -> pure Nothing
        [] -> pure Nothing

    nurStatus :: Has IIO sig m => Status -> m ResponseReceived
    nurStatus status = sekuraIOP (respondil $ responseBuilder status [] B.empty)

    dosServilAgord = defaultWebAppSettings "stat" & agordi

    agordi agord =
      agord
        { ssLookupFile = \n ->
            let piece t = (P.fromJust $ toPiece t)
                p prov sekv = do
                  rez <- ssLookupFile agord prov
                  case rez of
                    LRNotFound -> sekv
                    au -> pure au
             in p n $
                  p (piece "-" : n) $
                    p (piece <$> ["-", "index.html"]) $
                      pure LRNotFound,
          ssMaxAge = NoMaxAge
        }
