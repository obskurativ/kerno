{-# LANGUAGE UnboxedTuples, BangPatterns #-}
module Db.RHM where
import Db.RocksDb
import Data.Binary ()
import qualified Data.HashMap.Internal as HM
import qualified Data.HashMap.Internal.Array as A
import Data.Bits
import SPrelude hiding (mask)
import Data.Constraint (withDict)
import Db.Tipoj
import qualified Data.Hashable as H

{-
https://github.com/haskell-unordered-containers/unordered-containers

Copyright (c) 2010, Johan Tibell

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.

    * Neither the name of Johan Tibell nor the names of other
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-}
type Shift = Int

insert :: forall k v sig0 m0 s0. (KonataTip k, Typeable k, KonataTip v, Typeable v, Has (DbKnt :+: Region s0) sig0 m0) => k -> Din s0 v -> RHM s0 k v -> m0 (RHM s0 k v)
insert k = insert' (hash k) k
{-# SCC insert #-}

insert' :: forall k v sig0 m0 s0. (KonataTip k, Typeable k, KonataTip v, Typeable v, Has (DbKnt :+: Region s0) sig0 m0)
  => Hash -> k -> Din s0 v -> RHM s0 k v -> m0 (RHM s0 k v)
insert' h0 k0 v0 = go h0 k0 v0 0 where
  go :: forall s sig m. Has (DbKnt :+: Region s) sig m
    => Hash -> k -> Din s v -> Shift -> RHM s k v -> m (RHM s k v)
  go !h !k x !_ RHMEmpty = pure $ RHMLeaf h (RHMLeaf' k x)
  go h k x s t@(RHMLeaf hy l@(RHMLeaf' ky _))
      | hy == h = if ky == k
                  then pure $ RHMLeaf h (RHMLeaf' k x)
                  else pure $ collision h l (RHMLeaf' k x)
      | otherwise = two s h k x hy t
  go h k x s (RHMBitmapIndexed b ary)
      | b .&. m == 0 = do
        !ary' <- A.insert ary i <$> skrIndTen (dPur @s $ RHMLeaf h (RHMLeaf' k x))
        pure $ bitmapIndexedOrFull (b .|. m) ary'
      | otherwise = do
          !st <- legIndD @_ @s (A.index ary i)
          !st' <- skrInd \(Proxy @s') lokalig -> withDict (lokalig @s) $
            dPur <$> go @s' h k (lokSt @v @s @s' x) (s+bitsPerSubkey) (lokSt @(RHM () k v) st)
          pure $ RHMBitmapIndexed b (A.update ary i st')
    where m = mask h s
          i = sparseIndex b m
  go h k x s (RHMFull ary) = do
      !st <- legIndD @_ @s $ A.index ary i
      !st' <- skrInd \(Proxy @s') lokalig -> withDict (lokalig @s) $
        dPur <$> go @s' h k (lokSt @v @s @s' x) (s+bitsPerSubkey) (lokSt @(RHM () k v) st)
      pure $ RHMFull (HM.update32 ary i st')
    where i = index h s
  go h k x s t@(RHMCollision hy v)
      | h == hy   = pure $ RHMCollision h (updateOrSnocWith (\a _ -> (# a #)) k x v)
      | otherwise = do
        t' <- skrIndTen $ dPur t
        go h k x s $ RHMBitmapIndexed (mask hy s) (A.singleton t')

lookup ::
     (Statik k, Hashable k, Typeable k, Typeable v, Statik v, Has (DbKnt :+: Region s) sig m)
  => k
  -> RHM s k v -> m (Maybe (Din s v))
lookup k = lookup' (hash k) k 0

lookup' ::
     (Statik k, Eq k, Typeable k, Typeable v, Statik v, Has (DbKnt :+: Region s) sig m)
  => Hash -- The hash of the key
  -> k
  -> Int -- The offset of the subkey in the hash.
  -> RHM s k v -> m (Maybe (Din s v))
lookup' !h0 !k0 !s0 !m0 = go h0 k0 s0 m0
  where
    go !_ !_ !_ RHMEmpty = pure Nothing
    go h k _ (RHMLeaf hx (RHMLeaf' kx x))
        | h == hx && k == kx = pure $ Just x
        | otherwise          = pure Nothing
    go h k s (RHMBitmapIndexed b v)
        | b .&. m == 0 = pure Nothing
        | otherwise    = legIndD (A.index v (sparseIndex b m)) >>= go h k (s+bitsPerSubkey)
      where m = mask h s
    go h k s (RHMFull v) = legIndD (A.index v (index h s)) >>= go h k (s+bitsPerSubkey)
    go h k _ (RHMCollision hx v)
        | h == hx   = pure $ lookupInArray k v
        | otherwise = pure Nothing

collision :: Hash -> RHMLeaf' s k v -> RHMLeaf' s k v -> RHM s k v
collision h !e1 !e2 =
  let v = A.run $ do mary <- A.new 2 e1
                     A.write mary 1 e2
                     return mary
  in RHMCollision h v

bitsPerSubkey :: Int
bitsPerSubkey = 5

maxChildren :: Int
maxChildren = 1 `unsafeShiftL` bitsPerSubkey

subkeyMask :: Word
subkeyMask = 1 `unsafeShiftL` bitsPerSubkey - 1

two :: forall s sig m k v. (KonataTip k, Typeable k, KonataTip v, Typeable v, Has (DbKnt :+: Region s) sig m)
  => Shift -> Hash -> k -> Din s v -> Hash -> RHM s k v -> m (RHM s k v)
two = go
  where
    go :: forall s2 sig2 m2. Has (DbKnt :+: Region s2) sig2 m2 => Shift -> Hash -> k -> Din s2 v -> Hash -> RHM s2 k v -> m2 (RHM s2 k v)
    go s h1 k1 v1 h2 t2
        | bp1 == bp2 = do
            !st <- skrInd \(Proxy @s') lokalig -> withDict (lokalig @s2) $
              dPur <$> go (s+bitsPerSubkey) h1 k1 (lokSt @v @s2 @s' v1) h2 (lokSt @(RHM () k v) t2)
            pure $ RHMBitmapIndexed bp1 $ runST $ A.singletonM st
        | otherwise = do
            l1 <- skrIndTen $ dPur @s2 $ RHMLeaf h1 (RHMLeaf' k1 v1)
            l2 <- skrIndTen $ dPur t2
            let ary = runST do
                  mary <- A.new 2 $! l1
                  A.write mary idx2 l2
                  A.unsafeFreeze mary
            pure $ RHMBitmapIndexed (bp1 .|. bp2) ary
      where
        bp1  = mask h1 s
        bp2  = mask h2 s
        idx2 | index h1 s < index h2 s = 1
             | otherwise               = 0

index :: Hash -> Shift -> Int
index w s = fromIntegral $ unsafeShiftR w s .&. subkeyMask
{-# INLINE index #-}

mask :: Hash -> Shift -> Bitmap
mask w s = 1 `unsafeShiftL` index w s

bitmapIndexedOrFull :: Bitmap -> A.Array (IndDet s (RHM () k v)) -> RHM s k v
bitmapIndexedOrFull b !ary
    | b == fullNodeMask = RHMFull ary
    | otherwise         = RHMBitmapIndexed b ary

sparseIndex
    :: Bitmap
    -> Bitmap
    -> Int
sparseIndex b m = popCount (b .&. (m - 1))

fullNodeMask :: Bitmap
fullNodeMask = complement (complement 0 `shiftL` maxChildren)


updateOrSnocWith :: Eq k => (Din s v -> Din s v -> (# Din s v #)) -> k -> Din s v -> A.Array (RHMLeaf' s k v)
                 -> A.Array (RHMLeaf' s k v)
updateOrSnocWith f = updateOrSnocWithKey (const f)

updateOrSnocWithKey :: Eq k => (k -> Din s v -> Din s v -> (# Din s v #)) -> k -> Din s v -> A.Array (RHMLeaf' s k v)
                 -> A.Array (RHMLeaf' s k v)
updateOrSnocWithKey f k0 v0 ary0 = go k0 v0 ary0 0 (A.length ary0)
  where
    go !k v !ary !i !n
        -- Not found, append to the end.
        | i >= n = A.snoc ary $ RHMLeaf' k v
        | RHMLeaf' kx y <- A.index ary i
        , k == kx
        , (# v2 #) <- f k v y
            = A.update ary i (RHMLeaf' k v2)
        | otherwise
            = go k v ary (i+1) n

lookupInArray ::
  Eq k => k -> A.Array (RHMLeaf' s k v) -> Maybe (Din s v)
lookupInArray k0 ary0 = go k0 ary0 0 (A.length ary0)
  where
    go !k !ary !i !n
        | i >= n    = Nothing
        | otherwise = case A.index ary i of
            (RHMLeaf' kx v)
                | k == kx   -> Just v
                | otherwise -> go k ary (i+1) n

hash :: Hashable k => k -> Hash
hash = fromIntegral <<< H.hash
 
