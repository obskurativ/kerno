module Db.Tipoj where

import SPrelude hiding (mask)
import Data.Kind
import Data.Serialize
import Data.Hashable
import Type.Reflection
import Data.Typeable (eqT)
import qualified Data.HashMap.Internal.Array as A
import Data.HashMap.Internal.List (isPermutationBy)
import Data.List (sort)
import Data.Constraint
import Control.Carrier.Writer.Strict (Writer, execWriter, tell)
import Control.Carrier.Lift (runM, Lift)
import Control.Carrier.Interpret (runInterpret)
import Control.Effect.Lift (sendM)
import Control.Carrier.State.Strict (State, evalState)
import qualified Control.Carrier.State.Strict as State
import Unsafe.Coerce (unsafeCoerce)
import Control.Applicative ((<**>))

data DbEvent =
  EvKreiUzant !Int !SText
  | EvPetEnsalut !Int !SText
  | EvMet !SText !SText
  | EvMetHM !Int !SText
  deriving (Generic, Serialize)

data Det (s :: Type) (a :: Type) where
  DPur :: KonataTip a => a -> Det s a
  DDu :: !(Det s (a -> b)) -> Det s ((a, a) -> (b, b))
  Fib :: !Int -> Det s Int
  DMet :: (KonataTip k, Typeable k, KonataTip v, Typeable v) => Det s k -> Det s v -> Det s (RHM () k v) -> Det s (RHM () k v)

data FaldF a where
  FaldF :: FaldF (RHM () Int SText)

newtype SText = SText { malSText :: Text }
  deriving newtype (Show, Eq, Hashable)
instance Serialize SText where
  put = put <<< encodeUtf8 <<< malSText
  get = (SText <<< decodeUtf8Lenient) <$> get

newtype Ind (s :: Type) (a :: Type) = Ind { malInd :: Int64 }
  deriving newtype (Eq, Hashable)
newtype EventInd = EventInd { malEventInd :: Int64 }
  deriving newtype (Serialize, Eq, Ord)

type IndDet s a = Ind s (Det () a)

-- Statik
data Met m a where
  Met :: Put -> Met m ()
lMet :: (Has Met sig m) => Put -> m ()
lMet = send <<< Met
{-# INLINE lMet #-}

lAkir :: (Has (Lift Get) sig m) => Get a -> m a
lAkir = sendM
{-# INLINE lAkir #-}

class Din () a ~ a => Statik a where
  type Din s a

unsafeSt :: forall s a. Statik a => Din s a -> a
unsafeSt = unsafeCoerce

unsafeMalSt :: forall s a. Statik a => a -> Din s a
unsafeMalSt = unsafeCoerce

dPur :: forall s a. KonataTip a => Din s a -> Det s a
dPur = DPur <<< unsafeSt @s

-- Statik estas malplena limigo, de ni povu lAkiri ĝin el io.
unsafeEstasStatik :: Dict (Statik b)
unsafeEstasStatik = unsafeCoerce $ Dict @()

class Statik a => KonsStatik a where
  metS :: Has (Writer (RenvList Int64) :+: Met) sig m => a -> m ()
  akirS :: Has (State [Int64] :+: Lift Get) sig m => m a

  default metS :: (Serialize a, Has (Writer (RenvList Int64) :+: Met) sig m) => a -> m ()
  metS = lMet <<< put
  default akirS :: (Serialize a, Has (State [Int64] :+: Lift Get) sig m) => m a
  akirS = lAkir get

konsStatik :: KonsStatik a => a -> ByteString
konsStatik a =
  let (indj, val) = runPutMBuilder $ runM $ runInterpret (\_ (Met m) knt -> sendM m $> knt) $ execWriter @(RenvList Int64) $ metS a
  in runPut $ put (rlAlList indj) *> putBuilder val

indikj :: KonsStatik a => a -> [Int64]
indikj a = akirRenv $ run $ runInterpret (\_ (Met _) knt -> pure knt) $ execWriter @(RenvList Int64) $ metS a

malkonsStatik :: ByteString -> Either String ([Int64], KonsStatik a => Either String a)
malkonsStatik origBs = runGetState (get @[Int64]) origBs 0 <&> \(indj, bs) ->
  (indj, runGet (runM $ evalState indj akirS) bs)

instance Statik Int where
  type Din s Int = Int
instance KonsStatik Int where
instance Statik Int64 where
  type Din s Int64 = Int64
instance KonsStatik Int64

instance Statik DbEvent where
  type Din s DbEvent = DbEvent
instance (Statik a, Statik b) => Statik (a, b) where
  type Din s (a, b) = (Din s a, Din s b)
instance (KonsStatik a, KonsStatik b) => KonsStatik (a, b) where
  metS (a, b) = metS a *> metS b
  akirS  = (,) <$> akirS <*> akirS

instance Statik a => Statik (Maybe a) where
  type Din s (Maybe a) = Maybe (Din s a)
instance KonsStatik a => KonsStatik (Maybe a) where
  metS = \case
    Nothing -> lMet $ putWord8 0
    Just v -> lMet (putWord8 1) *> metS v
  akirS = lAkir getWord8 >>= \case
    0 -> pure Nothing
    _ -> Just <$> akirS

instance Statik a => Statik [a] where
  type Din s [a] = [Din s a]
instance KonsStatik a => KonsStatik [a] where
  metS l = lMet (putWord64be $ fromIntegral $ length l) *> traverse_ metS l
  akirS = lAkir getWord64be >>= go []
    where
    go as 0 = return $! reverse as
    go as i = do
      x <- akirS
      x `seq` go (x:as) (i - 1)

instance (Statik a, Statik b) => Statik (a -> b) where
  type Din s (a -> b) = (Din s a -> Din s b)

instance Statik SText where
  type Din s SText = SText
instance KonsStatik SText

instance Statik a => Statik (Ind () a) where
  type Din s (Ind () a) = Ind s a
instance Statik a => KonsStatik (Ind () a) where
  metS (Ind i) = tell $ rl1 i
  akirS = State.get >>= \case
    (x:xs) -> State.put xs $> Ind x
    [] -> sendM @Get $ fail "Index storage exhausted"

instance Statik EventInd where
  type Din s EventInd = EventInd
instance KonsStatik EventInd

-- RHM
type Hash   = Word
type Bitmap = Word

data RHM s k v
    = RHMEmpty
    | RHMBitmapIndexed !Bitmap !(A.Array (IndDet s (RHM () k v)))
    | RHMLeaf !Hash !(RHMLeaf' s k v)
    | RHMFull !(A.Array (IndDet s (RHM () k v)))
    | RHMCollision !Hash !(A.Array (RHMLeaf' s k v))

-- FARENDE: limigi uzeblajn elementoj ene de DPur

data RHMLeaf' s k v = RHMLeaf' !k !(Din s v)

instance (Eq k, Eq v, Statik v) => Eq (RHM s k v) where
  (==) = go
    where
      go RHMEmpty RHMEmpty = True
      go (RHMBitmapIndexed bm1 ary1) (RHMBitmapIndexed bm2 ary2)
        = bm1 == bm2 && A.sameArray1 (==) ary1 ary2
      go (RHMLeaf h1 l1) (RHMLeaf h2 l2) = h1 == h2 && leafEq l1 l2
      go (RHMFull ary1) (RHMFull ary2) = A.sameArray1 (==) ary1 ary2
      go (RHMCollision h1 ary1) (RHMCollision h2 ary2)
        = h1 == h2 && isPermutationBy leafEq (A.toList ary1) (A.toList ary2)
      go _ _ = False

      leafEq (RHMLeaf' k1 v1) (RHMLeaf' k2 v2) = k1 == k2 && unsafeSt @s @v v1 == unsafeSt @s @v v2

instance (Hashable k, Hashable v, Statik v) => Hashable (RHM s k v) where
  hashWithSalt = go
    where
      go :: Int -> RHM s k v -> Int
      go s RHMEmpty = s
      go s (RHMBitmapIndexed _ a) = A.foldl' hashWithSalt s a
      go s (RHMLeaf h (RHMLeaf' _ v))
        = s `hashWithSalt` h `hashWithSalt` unsafeSt @s @v v
      go s (RHMFull a) = A.foldl' hashWithSalt s a
      go s (RHMCollision h a)
        = (s `hashWithSalt` h) `hashCollisionWithSalt` a

      hashRHMLeafWithSalt :: Int -> RHMLeaf' s k v -> Int
      hashRHMLeafWithSalt s (RHMLeaf' k v) = s `hashWithSalt` k `hashWithSalt` unsafeSt @s @v v

      hashCollisionWithSalt :: Int -> A.Array (RHMLeaf' s k v) -> Int
      hashCollisionWithSalt s
        = foldl' hashWithSalt s <<< arrayHashesSorted s

      arrayHashesSorted :: Int -> A.Array (RHMLeaf' s k v) -> [Int]
      arrayHashesSorted s = sort <<< map (hashRHMLeafWithSalt s) <<< A.toList

instance (Statik k, Statik v) => Statik (RHMLeaf' () k v) where
  type Din s (RHMLeaf' () k v) = RHMLeaf' s k v
instance (KonsStatik k, KonsStatik v) => KonsStatik (RHMLeaf' () k v) where
  metS (RHMLeaf' k v) = metS k *> metS v
  akirS = RHMLeaf' <$> akirS <*> akirS
  

instance (Statik k, Statik v) => Statik (RHM () k v) where
  type Din s (RHM () k v) = RHM s k v

instance (KonsStatik k, KonsStatik v) => KonsStatik (RHM () k v) where
  metS = \case
    RHMEmpty -> lMet $ putWord8 0
    RHMBitmapIndexed bm ar -> lMet (putWord8 1 *> put bm) *> metS (A.toList ar)
    RHMLeaf h f -> lMet (putWord8 2 *> put h) *> metS f
    RHMFull ar -> lMet (putWord8 3) *> metS (A.toList ar)
    RHMCollision h ar -> lMet (putWord8 4 *> put h) *> metS (A.toList ar)
  akirS = lAkir getWord8 >>= \case
    0 -> pure RHMEmpty
    1 -> RHMBitmapIndexed <$> lAkir get <*> lAkirList
    2 -> RHMLeaf <$> lAkir get <*> akirS
    3 -> RHMFull <$> lAkirList
    _ -> RHMCollision <$> lAkir get <*> lAkirList
    where
    lAkirList :: (Has (State [Int64] :+: Lift Get) sig m, KonsStatik a) => m (A.Array a)
    lAkirList = akirS <&> \l -> A.fromList (length l) l

-- Fald

instance Eq (FaldF a) where
  FaldF == FaldF = True
instance Hashable (FaldF a) where
  hashWithSalt s FaldF = s
instance Statik (FaldF a) where
  type Din s (FaldF a) = FaldF a
instance Typeable a => KonsStatik (FaldF a) where
  metS FaldF = pure ()
  akirS = case eqT @a @(RHM () Int SText) of
    Just Refl -> pure FaldF
    Nothing -> sendM @Get $ fail $ malgxustTip @a @(RHM () Int SText)

-- KonataTip

malgxustTip :: forall a e s. (Typeable a, Typeable e, IsString s, Semigroup s) => s
malgxustTip = "Unexpected type " <> fromString (show $ typeRep @a) <> ", expected " <> fromString (show $ typeRep @e)

class (Hashable a, KonsStatik a) => KonataTip a
instance KonataTip Int
instance KonataTip SText
instance (KonataTip k, KonataTip v) => KonataTip (RHM () k v)
instance Statik v => KonataTip (Ind () v)

malkodKonat :: forall a sig m. (Typeable a, Has (Lift Get) sig m) => m (Dict (KonataTip a))
malkodKonat = case typeRep @a of
    rhm `App` s `App` (TypeRep :: TypeRep k) `App` (TypeRep :: TypeRep v)
      | Just HRefl <- eqTypeRep rhm (typeRep @RHM)
      , Just HRefl <- eqTypeRep s (typeRep @())
      -> malkodKonat @k <**> (malkodKonat @v <&> \Dict Dict -> Dict)
    ind `App` s `App` (TypeRep :: TypeRep v)
      | Just HRefl <- eqTypeRep ind (typeRep @Ind)
      , Just HRefl <- eqTypeRep s (typeRep @())
      , Dict <- unsafeEstasStatik @v
      -> pure Dict
    t | Just HRefl <- eqTypeRep t (typeRep @Int)
      -> pure Dict
      | Just HRefl <- eqTypeRep t (typeRep @SText)
      -> pure Dict
      | otherwise 
      -> sendM @Get $ fail $ "Unknown type " <> show t

-- Det
instance Eq (Det s a) where
  (DPur a) == (DPur b) = a == b
  (DPur _) == _ = False
  (DDu f1) == (DDu f2) = f1 == f2
  (DDu _) == _ = False
  (Fib x) == (Fib y) = x == y
  (Fib _) == _ = False
  (DMet a b c) == (DMet a' b' c') = a == a' && b == b' && c == c'
  (DMet {}) == _ = False

instance Hashable (Det s a) where
  hashWithSalt s (DPur f) = s `hashWithSalt` (0 :: Int) `hashWithSalt` f
  hashWithSalt s (DDu f) = s `hashWithSalt` (1 :: Int) `hashWithSalt` f
  hashWithSalt s (Fib x) = s `hashWithSalt` (2 :: Int) `hashWithSalt` x
  hashWithSalt s (DMet a b c) = s `hashWithSalt` (3 :: Int) `hashWithSalt` a `hashWithSalt` b `hashWithSalt` c

instance Statik a => Statik (Det () a) where
  type Din s (Det () a) = Det s a

metSDet :: Has (Writer (RenvList Int64) :+: Met) sig m => Det () b -> m ()
metSDet = \case
  DPur a -> lMet (putWord8 0) *> metS a
  DDu f -> lMet (putWord8 1) *> metSDet f
  Fib x -> lMet (putWord8 2 *> put x)
  DMet a b c -> lMet (putWord8 3) *> metSDet a *> metSDet b *> metSDet c

instance (Statik a, Typeable a) => KonsStatik (Det () a) where
  metS = metSDet
  akirS = lAkir getWord8 >>= \case
    0 -> malkodKonat @a >>= \Dict -> DPur <$> akirS
    1 -> case typeRep @a of
      (k1 `App` a1 `App` a2) `Fun` (k2 `App` b1 `App` b2)
        | Just HRefl <- eqTypeRep k1 (typeRep @(,))
        , Just HRefl <- eqTypeRep k2 (typeRep @(,))
        , Just HRefl <- eqTypeRep a1 a2
        , Just HRefl <- eqTypeRep b1 b2
        , TypeRep :: TypeRep a1 <- a1
        , TypeRep :: TypeRep a2 <- b1
        , Dict <- unsafeEstasStatik @(a1 -> a2)
        -> DDu <$> akirS
      _ -> sendM @Get $ fail $ malgxustTip @a @(((),()) -> ((), ()))
    2 -> case eqT @a @Int of
      Just Refl -> Fib <$> lAkir get
      _ -> sendM @Get $ fail $ malgxustTip @a @Int
    3 -> case typeRep @a of
      (k1 `App` malplen `App` k `App` v)
        | Just HRefl <- eqTypeRep k1 (typeRep @RHM)
        , Just HRefl <- eqTypeRep malplen (typeRep @())
        , TypeRep :: TypeRep k <- k
        , TypeRep :: TypeRep v <- v
        -> malkodKonat @k >>= \Dict -> malkodKonat @v >>= \Dict ->
          DMet <$> akirS <*> akirS <*> akirS
      _ -> sendM @Get $ fail $ malgxustTip @a @(RHM () () ())
    _ -> sendM @Get $ fail "Wrong tag"
