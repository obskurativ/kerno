-- FARENDE: DbKonekt estu ena?
{-# LANGUAGE RecursiveDo #-}
module Db.RocksDb (DbKonekt(..), UzDet(..), DbKnt, DbErar(..), Fald(..), UzFaldF(..), Region, Lokalig, dbKonekt, region, skrInd, skrIndTen, lokSt, dPur, unsafeMalSt, uzKasxDet, legInd, legIndD, sendEvent, uzFald, uzUnikDet, finTrans) where

import SPrelude
import qualified Database.RocksDB as R
import qualified StmHamt.Hamt as H
import qualified Data.Serialize as S
import Data.Hashable
import Datum (UzSTM (..), stm, IIO, sekuraIO, STMC (..), medio, SIO(..), inf, InfTip (..), nomspac)
import Control.Effect.Lift
import Control.Carrier.Reader
import Type.Reflection
import Data.Typeable (cast, eqT)
import Control.Carrier.Lift
import Control.Concurrent (forkFinally)
import Data.Kind
import Data.Constraint (withDict, (:-), refl, Dict(..))
import Unsafe.Coerce (unsafeCoerce)
import Db.Tipoj
import RIO.List (headMaybe, find, uncons)
import Control.Carrier.State.Strict (runState, StateC, State, get, put, execState, evalState, modify)
import qualified Focus as F
import qualified ListT as LT
import Data.List (partition)
import qualified Control.Effect.State.Labelled as SL
import qualified RIO.HashSet as HS
import qualified Data.HashMap.Strict as HM
import Control.Carrier.Writer.Strict (execWriter, tell, runWriter)
import qualified Data.Bifunctor as B
import Data.Functor.Compose (Compose(..))

-- | @kunFinig finigo ago@ plenumigos agon @ago@ kaj, kiam ĝi estas finita, plenumigas @finigo@.
-- Rimarku, ke @finigo@ garantie plenumiĝas, en komenca kunteksto, kaj ĝiaj ŝanĝoj de kunteksto estas ignorataj.
kunFinig :: Has (Lift SIO) sig m => m () -> m a -> m a
kunFinig finig ag = liftWith @SIO \trk knt -> 
  UnsafeIO $ malSIO (trk (knt $> ag)) <* malSIO (trk (knt $> finig))
{-# INLINE kunFinig #-}

newtype RegionC s m a = RegionC { malRegionC :: ReaderC (STM () -> STM ()) m a }
  deriving newtype (Functor, Applicative, Monad)

data Region (s :: Type) m a where
  RegionAldFinig :: Region s m (STM () -> STM ())

instance Algebra sig m => Algebra (Region s :+: sig) (RegionC s m) where
  alg trk sig knt = RegionC $ case sig of
    L RegionAldFinig -> (knt $>) <$> ask
    R r -> ReaderC \dat -> alg (runReader dat <<< malRegionC <<< trk) r knt
  {-# INLINE alg #-}

class Lokala sG sM
lokSt :: forall a s s'. (Lokala s s', Statik a) => Din s a -> Din s' a
lokSt = unsafeCoerce
{-# INLINE lokSt #-}

type Lokalig s sig m = forall (s0 :: Type). Has (Region s0) sig m :- Lokala s0 s
-- Krei novan regionon.
-- @@antS@@ estas iam necesa kaze de multaj regionoj por certigi GHC, ke antS /= novS.
region :: forall antS sig m a. Has (UzSTM :+: Lift SIO) sig m => (forall s. Proxy (antS, s) -> Lokalig (antS, s) sig m -> RegionC (antS, s) m a) -> m a
region f = do
  l <- stm $ newTVar []
  kunFinig (finig l) $ runReader
    (\x -> modifyTVar' l (x:))
    (malRegionC $ f Proxy (unsafeCoerce refl))
  where
  finig l = do
    oldL <- stm $ swapTVar l []
    unless (null oldL) do
      traverse_ stm oldL
      finig l
{-# INLINE region #-}

kunRegionFinig :: forall s sig m. Has (UzSTM :+: Region s) sig m => STM () -> m ()
kunRegionFinig ag = do
  ald <- send (RegionAldFinig @s)
  stm $ ald ag
{-# INLINE kunRegionFinig #-}

newtype DetRez a = DetRez { malDetRez :: Maybe a }
instance Statik a => Statik (DetRez a) where
  type Din s (DetRez a) = DetRez (Din s a)
instance KonsStatik a => KonsStatik (DetRez a) where
  metS = metS <<< malDetRez
  akirS = DetRez <$> akirS

data Fald a = Fald !a !(FaldF a) deriving (Eq, Generic, Hashable)

instance Statik a => Statik (Fald a) where
  type Din s (Fald a) = Fald a
instance (KonsStatik a, Typeable a) => KonsStatik (Fald a) where
  metS (Fald a b) = metS a *> metS b
  akirS = Fald <$> akirS <*> akirS

-- FARENDE: Forigi @s@?
data Sxl s a where
  ProvInd :: (Typeable a, KonsStatik a) => !(Ind s a) -> Sxl s a
  ProvMemo :: (Typeable a, KonsStatik a) => !(Det s a) -> Sxl s (DetRez a)
  ProvFald :: (Typeable a, Hashable a, KonsStatik a) => !(Fald a) -> Sxl s (EventInd, a)

-- Maltipigita Sxl
data MSxl = forall s a. MSxl !(Sxl s a)

sxlElCxel :: MCxel -> MSxl 
sxlElCxel (MCxel sxl _) = MSxl sxl

instance Eq MSxl where
  (MSxl a) == (MSxl b) = case (a, b) of
    (ProvMemo f1, ProvMemo f2) -> Just (stDet f1) == cast (stDet f2)
    (ProvMemo _, _) -> False
    (ProvInd i1, ProvInd i2) -> malInd i1 == malInd i2
    (ProvInd _, _) -> False
    (ProvFald f1, ProvFald f2) -> Just f1 == cast f2
    (ProvFald _, _) -> False
    where
    stDet :: Det s a -> Det () a
    stDet = unsafeCoerce

instance Hashable MSxl where
  hashWithSalt s (MSxl x) = case x of
    ProvMemo f -> s `hashWithSalt` f 
    ProvInd i -> s `hashWithSalt` malInd i
    ProvFald f -> s `hashWithSalt` f

newtype Epok = Epok Int
  deriving newtype Eq
data Transigx e = Transigx { _trNov :: ![e], _trSavat :: ![e], _trSavit :: ![e] }

type TransigxEl e = (Int, Int, e) -- (indekso, uzoj, val)
type Transigx' e = Transigx (TransigxEl e)
data ETransigx e = ETransigx !Epok !(Transigx' e)

trElNulUzoj :: TransigxEl e -> Bool
trElNulUzoj (_, uzoj, _) = uzoj == 0
{-# INLINE trElNulUzoj #-}

malETransigx :: Epok -> ETransigx e -> Transigx' e
malETransigx nun (ETransigx tiam tr@(Transigx nov savot savit)) =
  if nun == tiam
    then tr
    else Transigx [] (nov<>savot) savit
{-# INLINE malETransigx #-}

data MCxel = forall s a. MCxel !(Sxl s a) !(ETransigx a)

mCxel :: Sxl s e -> Epok -> Transigx' e -> Maybe MCxel
mCxel sxl epok tr = case tr of
  Transigx [] [] [] -> Nothing
  _ -> Just $ MCxel sxl $ ETransigx epok tr
{-# INLINE mCxel #-}

data DbErar = DbKorupt !Utf8Builder | DbErar
instance Display DbErar where
  display = \case
    DbKorupt v -> "DB corruption:" <> v
    DbErar -> "DB access error"

type DbKnt = IIO :+: Reader DbKonekt :+: Throw DbErar

newtype UzDet = UzDet (forall s sig m b. Has (DbKnt :+: Region s) sig m => Det s b -> m (Din s b))
newtype UzFaldF = UzFaldF (forall s a sig m. Has (DbKnt :+: Region s) sig m => FaldF a -> Din s a -> DbEvent -> m (Din s a))
data TransigilSig = TransigilMortig | TransigilVeki
data DbKonekt = DbKonekt {
    akirDb :: !R.DB,
    akirUzDet :: !UzDet,
    akirUzFaldF :: !UzFaldF,
    akirProv :: !(H.Hamt MCxel),
    akirEpok :: !(TVar Epok),
    akirList :: !(TVar EventInd, R.ColumnFamily),
    -- (ind, signalilo al RK, (olda epoko, nova epoko), cf)
    akirAmas :: !(TVar Int64, R.ColumnFamily),
    akirMemoCf :: !R.ColumnFamily,
    akirFaldCf :: !R.ColumnFamily,
    akirAmasUzojCf :: !R.ColumnFamily,
    akirStatusCf :: !R.ColumnFamily,
    akirTransigil :: !(MVar TransigilSig)
  }

malkodErar :: (Has (Throw DbErar) sig m) => Utf8Builder -> Either String a -> m a
malkodErar obj = levE <<< B.first \x -> DbKorupt $ "Deserialization error while parsing " <> obj <> ": " <> fromString x

malkons :: (Has (Throw DbErar) sig m, KonsStatik a) => Utf8Builder -> ByteString -> m a
malkons obj = malkodErar obj <<< ((\(_, v) -> v) =<<) <<< malkonsStatik

dbAkirSxl :: forall s sig m a. (Has (IIO :+: Reader DbKonekt :+: Throw DbErar) sig m) => Sxl s a -> m a
dbAkirSxl sxl = ask @DbKonekt >>= \kon -> case sxl of
  ProvMemo f -> do
    dbRez <- akir kon akirMemoCf (konsStatik $ unsafeSt @_ @(Det () _) f)
    DetRez <$> traverse (malkons "memoization") dbRez
  ProvInd i -> 
    malkons "index" =<< levM (DbKorupt $ "Invalid pointer\n" <> error "hi") =<< akir kon (snd <<< akirAmas) (S.encode $ malInd i)
  ProvFald f@(Fald kom _) -> do
    dbRez <- akir kon akirFaldCf (konsStatik $ f)
    fromMaybe (EventInd 0, kom) <$> traverse (malkons "folder") dbRez
  where
  akir kon akirCf ind = levM DbErar =<< sekuraIO (R.getCF (akirDb kon) (akirCf kon) ind)

malpakCxel :: forall s a. Sxl s a -> Dict (Typeable a, KonsStatik a)
malpakCxel = \case
  ProvMemo _ -> Dict
  ProvInd _ -> Dict
  ProvFald _ -> Dict
{-# INLINE malpakCxel #-}

data MCxel1 = forall s a. MCxel1 { cxelSxl :: !(Sxl s a), cxelVal :: !a, cxelSavu :: !Bool, cxelUzata :: !Bool }

-- FARENDE: Forigi @s@ en @Sxl s a@
dbMetSxl :: forall sig m. (Has (Reader DbKonekt) sig m) => MCxel1 -> m (Maybe R.BatchOp)
dbMetSxl (MCxel1 { cxelSxl, cxelVal, cxelSavu } ) = if not cxelSavu
  then pure Nothing
  else ask @DbKonekt <&> \kon ->
    case cxelSxl of
      ProvMemo @a f -> malDetRez cxelVal <&> \val' ->
        R.PutCF (akirMemoCf kon) (konsStatik $ unsafeSt @_ @(Det () a) f) (konsStatik val')
      ProvInd @a i -> Just $ R.PutCF (snd $ akirAmas kon) (S.encode $ malInd i) (konsStatik cxelVal)
      ProvFald f -> Just $ R.PutCF (akirFaldCf kon) (konsStatik $ f) (konsStatik cxelVal)
{-# INLINE dbMetSxl #-}

data Cxen m a = CxenFin a | Cxen (m (Cxen m a))

uzCxen :: Monad m => Cxen m a -> m a
uzCxen = \case
  CxenFin a -> pure a
  Cxen ag -> ag >>= uzCxen
{-# INLINE uzCxen #-}

enmetH :: MCxel -> H.Hamt MCxel -> STM ()
enmetH v = void <<< H.insert sxlElCxel v

forigH :: MSxl -> H.Hamt MCxel -> STM ()
forigH = H.focus F.delete sxlElCxel

akirH :: MSxl -> H.Hamt MCxel -> STM (Maybe MCxel)
akirH = H.lookup sxlElCxel

newtype TrGxis = TrGxis (forall e. Transigx' e -> Either DbErar (Transigx' e))
type STMCx s a = StateC (Transigx' a) (RegionC s (ReaderC (TrGxis -> STM (Either DbErar ())) (ErrorC DbErar (STMC STM))))
unsafeFokusCxel :: forall sOr s a sig m b. (Statik a, Typeable a, Has (DbKnt :+: Region sOr :+: Region s) sig m)
  => Sxl sOr a -> Bool -> ((forall c. STMCx s a c -> m c) -> STMCx s a (Cxen m b)) -> m b
unsafeFokusCxel sxl pravaloriz ago = do
  kon <- ask
  let 
    sxl' = MSxl sxl
    prov = akirProv kon
    legEpok = readTVar $ akirEpok kon
    erarMalaperis = DbKorupt "Expected cell to persist, found nothing"

    rapidGxis (TrGxis trGxis) = do
      epok <- legEpok
      let
        ag = F.lookup >>= \case
          Nothing -> pure $ Left erarMalaperis
          Just (MCxel oldSxl oldTr) ->
            for (trGxis (malETransigx epok oldTr)) $
              maybe F.delete F.insert <<< mCxel oldSxl epok
      H.focus ag sxlElCxel sxl' prov

    stmcx :: forall c. Bool -> STMCx s a c -> m c
    stmcx komenca f = do
      ald <- send (RegionAldFinig @s)
      uzCxen =<< stm (sxargu komenca \epok orTr -> runM $ malSTMC do
          rez <- runError $ runReader rapidGxis $ runReader ald $ malRegionC do
            (tr, rez) <- runState orTr f
            stm do
              epok' <- epok
              case mCxel sxl epok' tr of
                Just cxel -> enmetH cxel prov
                Nothing -> forigH sxl' prov
            pure rez
          pure $ either (Cxen <<< throwError) CxenFin rez
        )

    sxargu :: forall c. Bool -> (STM Epok -> Transigx' a -> STM (Cxen m c)) -> STM (Cxen m c)
    sxargu komenca f = do
      -- supozo: Savita Transigx neniam tute malplenas
      valM <- akirH sxl' prov
      case valM of
        Nothing
          | pravaloriz && komenca -> pure $ Cxen do
              dbSavit <- dbAkirSxl sxl
              stm $ f legEpok (Transigx [] [] [(0, 0, dbSavit)])
          | komenca -> f legEpok (Transigx [] [] [])
          | otherwise -> pure $ Cxen $ throwError erarMalaperis
        Just (MCxel @_ @ef (malpakCxel -> Dict) etr) -> case eqT @ef @a of
          Just Refl -> do
            epok <- legEpok
            f (pure epok) $ malETransigx epok etr
          Nothing -> pure $ Cxen $ throwError $ DbKorupt $ malgxustTip @ef @a
  uzCxen =<< stmcx True (ago $ stmcx False)
{-# INLINE unsafeFokusCxel #-}
{-# SCC unsafeFokusCxel #-}

-- | Forfiltri elementon kun 0 uzoj
trFiltr :: TransigxEl e -> [TransigxEl e]
trFiltr = \case
  (_, 0, _) -> []
  el -> [el]
{-# INLINE trFiltr #-}

-- | List zipper
listZip :: [a] -> [([a], a, [a])]
listZip = zip' [] where
  zip' _ [] = []
  zip' ant (x:xs) = (ant, x, xs) : zip' (x:ant) xs
{-# INLINE listZip #-}

malzip :: ([e], [e]) -> [e]
malzip (ant, post) = foldl' (flip (:)) post ant
{-# INLINE malzip #-}

trVic :: Alternative f => (Int -> [TransigxEl a] -> ([TransigxEl a] -> Transigx' a) -> f b) -> Transigx' a -> f b
trVic f (Transigx nov savot savit) =
  f 0 nov (\v -> Transigx v savot savit)
  <|> f 1 savot (\v -> Transigx nov v savit)
  <|> f 2 savit (Transigx nov savot)
{-# INLINE trVic #-}

trGxisUzoj :: Transigx' a -> [(TransigxEl a, (Int -> Int) -> Transigx' a)]
trGxisUzoj = trVic \poz list rekrei ->
  listZip list <&> \(ant, oldNun@(ind, oldUzoj, val), post) -> (oldNun,) \gxisUzoj ->
    let nun = (ind, gxisUzoj oldUzoj, val) in
    rekrei $ malzip $ if not (null ant) && poz /= 2
      then (ant, trFiltr nun <> post)
      else (ant, nun:post)
{-# INLINE trGxisUzoj #-}

trGxisInd :: Has (Throw DbErar) sig m => Int -> (Int -> Int) -> Transigx' a -> m (Transigx' a)
trGxisInd sercxInd gxisUzoj tr = do
  levM
    (DbKorupt "Attempted to update lock for non-existent element") 
    (find (\((ind, _, _), _) -> sercxInd == ind) $ trGxisUzoj tr) 
  <&> \(_, gxisPer) -> gxisPer gxisUzoj
{-# INLINE trGxisInd #-}

kreiMalUz :: forall e sig m. Has (UzSTM :+: Throw DbErar :+: State (Transigx' e) :+: Reader (TrGxis -> STM (Either DbErar ()))) sig m => Int -> TVar (STM ()) -> m (m ())
kreiMalUz ind fin = do
  rapidGxis <- ask @(TrGxis -> STM (Either DbErar ()))
  stm $ writeTVar fin $ void $ rapidGxis $ TrGxis $ trGxisInd ind (\x -> x - 1)
  pure $ get @(Transigx' e) >>= trGxisInd ind (\x -> x - 1) >>= put

trLegUz :: forall sig m e. Has (UzSTM :+: Throw DbErar :+: State (Transigx' e) :+: Reader (TrGxis -> STM (Either DbErar ()))) sig m
  => m (e, TVar (STM ()) -> m (m ()))
trLegUz = do
  orTr <- get
  (ind, val) <- case trVic (\_ l _ -> headMaybe l) orTr of
    Just (ind, _, val) -> pure (ind, val)
    Nothing -> throwError $ DbKorupt "Cell is empty"
  -- FARENDE: savu erarojn
  pure (val, \fin -> do
    get @(Transigx' e) >>= trGxisInd ind (+1) >>= put
    kreiMalUz @e ind fin)
{-# INLINE trLegUz #-}

trSkrUz :: forall sig m e. Has (UzSTM :+: Throw DbErar :+: State (Transigx' e) :+: Reader (TrGxis -> STM (Either DbErar ()))) sig m => e -> TVar (STM ()) -> m (m ())
trSkrUz el fin = do
  orTr <- get
  let
    ind = fromMaybe 0 $ trVic (\_ l _ -> (\(oldInd, _, _) -> oldInd + 1) <$> headMaybe l) orTr
    filtrKap = \case
      x:xs -> trFiltr x <> xs
      [] -> []
    tr = let (Transigx nov savot savit) = orTr in Transigx ((ind, 1, el):filtrKap nov) savot savit
  put tr
  kreiMalUz @e ind fin
{-# INLINE trSkrUz #-}

-- FARENDE: Din s' a?
uzKasxDet :: forall s a sig m. (Typeable a, KonsStatik a, Statik a, Has (DbKnt :+: Region s) sig m, Statik a) => Det s a -> m (Din s a)
uzKasxDet origF = unsafeMalSt @s <$> case origF of
  DPur v -> pure v
  _ -> unsafeFokusCxel @s @s (ProvMemo origF) True \stmcx -> do
    fin <- stm $ newTVar $ pure ()
    kunRegionFinig @s $ join $ readTVar fin
    (orVal, ($ fin) -> orUz) <- trLegUz
    unsafeMaluzOr <- orUz
    pure case malDetRez orVal of
      Just jam -> CxenFin jam
      Nothing -> Cxen do
        nov <- region @s \(Proxy @s') lokalig -> unsafeSt @s' <$>
          uzUnikDet @s' (withDict (lokalig @s) $ lokSt @(Det () a) origF)
        stmcx do
          (jamVal, ($ fin) -> jamUz) <- trLegUz
          unsafeMaluzOr
          CxenFin <$> case malDetRez jamVal of
            Just jam -> jamUz $> jam
            Nothing -> trSkrUz (DetRez $ Just nov) fin $> nov
{-# SCC uzKasxDet #-}
{-# INLINE uzKasxDet #-}

uzUnikDet :: forall s sig m b. Has (DbKnt :+: Region s) sig m => Det s b -> m (Din s b)
uzUnikDet d = do
  (akirUzDet -> UzDet f) <- ask
  f d
{-# INLINE uzUnikDet #-}

legInd :: forall sOr s sig m a. (Statik a, Typeable a, KonsStatik a, Has (DbKnt :+: Region sOr :+: Region s) sig m) => Ind sOr a -> m (Din s a)
legInd ind = unsafeMalSt @s @a <$> unsafeFokusCxel @sOr @s (ProvInd ind) True \_ -> 
  (CxenFin <<< fst) <$> trLegUz
{-# SCC legInd #-}
{-# INLINE legInd #-}

legIndD :: forall sOr s a sig m. (Statik a, Typeable a, Has (DbKnt :+: Region sOr :+: Region s) sig m) => IndDet sOr a -> m (Din s a)
legIndD = {-trace "Unimplemented" $ -}legInd >=> uzUnikDet @s
{-# INLINE legIndD #-}

-- Skribi, teni enskribitaj risurcoj en la memoro. Preferu @@skrInd@@
skrIndTen :: forall s s' sig m a. (Statik a, Typeable a, KonsStatik a, Has (DbKnt :+: Region s :+: Region s') sig m) => Din s' a -> m (Ind s a)
skrIndTen (unsafeSt @s' @a -> val) = do
  kon <- ask
  amInd <- stm do
    let tv = fst $ akirAmas kon
    readTVar tv
      ^| \x -> writeTVar tv (x + 1)
  let ind = Ind @s @a amInd
  unsafeFokusCxel @s @s (ProvInd ind) False \_ -> do
    fin <- stm $ newTVar $ pure ()
    kunRegionFinig @s $ join $ readTVar fin
    trSkrUz val fin $> CxenFin ()
  pure ind
{-# SCC skrIndTen #-}
{-# INLINE skrIndTen #-}

skrInd :: forall s sig m a. (Statik a, Typeable a, KonsStatik a, Has (DbKnt :+: Region s) sig m)
  => (forall s'. Proxy s' -> Lokalig s' sig m -> RegionC s' m (Din s' a)) -> m (Ind s a)
skrInd f = region @s \proxy@(Proxy @s') l -> f proxy l >>= skrIndTen @s @s'
{-# INLINE skrInd #-}

uzFald :: forall s a sig m. (KonsStatik a, Typeable a, Hashable a, Has (DbKnt :+: Region s) sig m) => Fald a -> m (Din s a)
uzFald fald@(Fald nulVal faldf) = do
  kon <- ask
  celInd <- stm $ readTVar (fst $ akirList kon)
  unsafeMalSt @s @a <$> unsafeFokusCxel @s @s (ProvFald fald) True \stmcx -> do
    get @(Transigx' (EventInd, a)) >>= \case
      Transigx [] [] [] -> put (Transigx [(0 :: Int, 0 :: Int, (EventInd 0, nulVal))] [] [])
      _ -> pure ()
    -- FARENDE: Okazas ofte, movi al aparta funkcio
    fin <- stm $ newTVar $ pure ()
    kunRegionFinig @s $ join $ readTVar fin

    (trLegUz >>= \(a, ($ fin) -> uz) -> (a,) <$> uz) <&> fix \rik ((origInd, origVal), origMalUz) ->
      if origInd >= celInd
        then CxenFin origVal
        else Cxen do
          event <- (malkodErar "db event" <<< S.decode) =<< 
            levM (DbKorupt "DbEvent not found") =<< levM DbErar =<< sekuraIO (R.getCF (akirDb kon) (snd $ akirList kon) (S.encode origInd))
          (unsafeSt @s @a -> novVal) <- akirUzFaldF kon & \(UzFaldF uzFaldF) -> uzFaldF @s faldf (unsafeMalSt @s origVal) event
          stmcx do
            origMalUz
            ((jamInd, jamVal), ($ fin) -> uz) <- trLegUz
            rik <$> if jamInd /= origInd
              then ((jamInd, jamVal),) <$> uz
              else
                let novInd = EventInd (malEventInd origInd + 1) in
                ((novInd, novVal),) <$> trSkrUz (novInd, novVal) fin

sendEvent :: Has (IIO :+: UzSTM :+: DbKnt) sig m => DbEvent -> m (Maybe ())
sendEvent event = do
  kon <- ask
  ind <- stm do
    let listInd = fst $ akirList kon
    (malEventInd <$> readTVar listInd)
      ^| \i -> writeTVar listInd $ EventInd $ i+1
  sekuraIO $ R.putCF (akirDb kon) (snd $ akirList kon) (S.encode ind) (S.encode event)

-- | Aktivigi transigilon, savante nuna db statuso al la disko
finTrans :: Has (IIO :+: Reader DbKonekt) sig m => m ()
finTrans = ask >>= \kon -> void $ sekuraIO $ tryPutMVar (akirTransigil kon) TransigilVeki

data SxlPart v = SxlPart { spHm :: HashMap Int64 (Int, v), spNul :: HashSet Int64, spNeNul :: HashSet Int64 }

transigil :: DbKonekt -> IO (MVar TransigilSig, MVar ())
transigil db = ((,) <$> newEmptyMVar <*> newEmptyMVar) ^| \(sig, fin) ->
  let trk = (sekuraIO (readMVar sig) <* trk' indeksProv) >>= \case
        Just TransigilVeki -> trk
        _ -> pure ()
  in void $
    forkFinally 
      (malSIO $ runM $ malSTMC $ medio $ nomspac "db.transaction" do
        rez <- runError @DbErar $ runReader db $ trk' indeksFinigLasta *> trk
        case rez of
          Left er -> do
            inf IErar $ "Transaction failed: " <> display er
            inf IErar "Switching DB to readonly. The End Is Nigh"
          Right () -> pure ())
      (\_ -> putMVar fin ()) 
  where

  -- teorie ne estas necesa, sed estu
  filtrSavot = \case
    [] -> []
    (x:xs) -> x:filter (not <<< trElNulUzoj) xs

  -- | (forigu, aldonu, rubuj, (laste savita, cxu superskribu, cxu uzata), tr)
  trSav :: Transigx' a -> Maybe ([TransigxEl a], [TransigxEl a], [TransigxEl a], TransigxEl a, Bool, Bool, Transigx' a)
  trSav (Transigx nov (filtrSavot -> savot) savit) =
    let
      -- (savotIgnor, old, last, superskribu last?)
      sxn = case (savot, savit) of
        ([], x:xs) -> Just ([x], xs, x, False)
        (x:_, _) -> Just (savot, savit, x, True)
        ([], []) -> Nothing
    in sxn <&> \(savotIgnor, old, last, superskribu) ->
        let 
          uzata = any (not <<< trElNulUzoj) $ nov <> savot <> savit 
          (oldForig, oldTen) = partition trElNulUzoj old
        in (oldForig, savot, oldTen, last, superskribu, uzata, if uzata || not (null nov) then Transigx nov [] $ savotIgnor <> oldTen else Transigx [] [] [])

  spGxis :: forall f v. Monad f => (Int64 -> f ()) -> Int64 -> (Maybe (Int, v) -> f (Maybe (Int, v))) -> SxlPart v -> f (SxlPart v)
  spGxis aldon ind f origSp = do
    let
      nul = fst >>> (== 0)
      gxis :: f (HS.HashSet Int64 -> HS.HashSet Int64) -> Maybe Bool -> SxlPart v -> f (SxlPart v)
      gxis ago = maybe pure \cxuNul sp ->
        ago <&> \ago' -> if cxuNul
          then sp { spNul = ago' $ spNul sp }
          else sp { spNeNul = ago' $ spNeNul sp }
    ((fmap nul -> orig', fmap nul -> nov'), hm') <- getCompose $ HM.alterF 
          (\orig -> Compose do
            nov <- f orig
            pure ((orig, nov), nov))
          ind
          (spHm origSp)
    origSp { spHm = hm' } & if orig' == nov'
      then pure
      else gxis (pure $ HS.delete ind) orig' >=> gxis (aldon ind $> HS.insert ind) nov'

  dbAkirUzoj :: Has (IIO :+: Throw DbErar) sig m => Int64 -> Maybe (Int, Maybe a) -> m (Int, Maybe a)
  dbAkirUzoj el = \case
    Nothing -> do
      valM <- levM DbErar =<< sekuraIO (R.getCF (akirDb db) (akirAmasUzojCf db) (S.encode el))
      (, Nothing) <$> maybe (pure 0) (malkodErar "reference counter" <<< S.decode) valM
    Just v -> pure v

  dbMetUzoj :: Int64 -> Int -> R.BatchOp
  dbMetUzoj i uz = R.PutCF (akirAmasUzojCf db) (S.encode i) (S.encode uz)

  prov = akirProv db
  por :: Has UzSTM sig m => LT.ListT STM a -> (a -> STM (Cxen m ())) -> m ()
  por list f = do
    join $ stm do
      LT.uncons list >>= \case
        Nothing -> pure $ pure ()
        Just (x, xs) -> do
          ag <- f x
          pure $ uzCxen ag *> por xs f

  dumJust :: Monad m => m (Maybe a) -> (a -> m ()) -> m ()
  dumJust ag f = fix \rik ->
    ag >>= traverse_ (\v -> f v *> rik)

  -- FARENDE: ni ne faru dbAkirUzoj por objektojn, kiuj estas novaj
  spGxisUzoj aldOp f el =
    SL.put @"uzoj" =<< (spGxis aldOp el (fmap (Just <<< f) <<< dbAkirUzoj el) =<< SL.get @"uzoj")

  spGxisUzojPur = spGxisUzoj (const $ pure ())

  indeksFinigLasta = do
    let leg nom = maybe (pure []) (malkodErar (displayBytesUtf8 nom) <<< S.decode) =<< levM DbErar =<< sekuraIO (R.getCF (akirDb db) (akirStatusCf db) nom)
    -- Ni nur memorigu al la sistemo pri la indekso.
    leg "rubInd" >>= traverse_ (spGxisUzojPur id)
    leg "rubLigil" >>= traverse_ (spGxisUzojPur $ B.first \x -> x - 1)

  indeksProv = do
    novEpok <- stm do
      Epok oldE <- readTVar (akirEpok db)
      let novE = Epok $ oldE + 1
      writeTVar (akirEpok db) novE
      pure novE
    let
      -- (forig, aldon, rub, MCxel1)
      savPlan :: MCxel -> Maybe (STM ([Int64], [Int64], [Int64], MCxel1))
      savPlan (MCxel sxl etr) = malpakCxel sxl & \Dict -> trSav (malETransigx novEpok etr) <&>
        \(forig, aldon, rub, (_, _, last), super, uzat, tr) ->
          let
            indikj' (_, _, v) = indikj v
          in (mCxel sxl novEpok tr & maybe (forigH (MSxl sxl) prov) (`enmetH` prov))
            $> (forig >>= indikj', aldon >>= indikj', rub >>= indikj', MCxel1 sxl last super uzat)
    por (H.listT prov) \cxel@(MCxel sxl _) ->
      savPlan cxel & maybe (pure $ CxenFin ()) 
        \ag -> ag <&> \(forig, aldon, rub, rezcxel) -> Cxen do
        -- Problemo: spGxisUzoj faras petojn al datumbazo eĉ se Ind certe ne estas tie
          eti @"rubLigil" $ tell $ rl1 rub
          for_ forig $ spGxisUzojPur $ B.first (\x -> x - 1)
          for_ aldon $ spGxisUzojPur $ B.first (+ 1)
          case sxl of
            ProvInd (Ind i) ->
              spGxisUzojPur (B.second (const $ Just rezcxel)) i
            _ -> do
              dbMetSxl rezcxel >>= traverse_ (tell <<< rl1)
          pure $ CxenFin ()
    
  trk' indeks = do
    (akirRenv -> opj, (akirRenv -> rubInd, akirRenv -> rubLigil)) <-
      runWriter @(RenvList R.BatchOp) $
      runWriter @(RenvList Int64) $ runLabelled @"rubInd" do
      execWriter @(RenvList [Int64]) $ runLabelled @"rubLigil" do
        uzoj <- execState (SxlPart @(Maybe MCxel1 {- rezcxel -}) HM.empty HS.empty HS.empty) $ runLabelled @"uzoj" do
          -- INDEKSADO
          indeks
          -- FORIGO
          origNul <- (HS.toList <<< spNul) <$> SL.get @"uzoj"
          evalState origNul $
            let legNul = do
                  l <- get
                  for (uncons l) \(x, xs) ->
                    put xs $> x
            in dumJust legNul \ind ->
              (HM.lookup ind <<< spHm) <$> SL.get @"uzoj" >>= \case
                Just (0, rezcxelM) -> do
                  let indk = S.encode ind
                  ag <- case rezcxelM of
                    Just (MCxel1 { cxelSxl, cxelVal, cxelUzata }) ->
                      pure $ if cxelUzata 
                        then Left ()
                        else Right $ malpakCxel cxelSxl & \Dict -> indikj cxelVal
                    Nothing -> Right <$> do
                      krudM <- levM DbErar =<< sekuraIO (R.getCF (akirDb db) (snd $ akirAmas db) indk)
                      krud <- levM (DbKorupt "Deleted link to an non-existing object") krudM
                      (malkodErar "index dependencies" <<< fmap fst <<< malkonsStatik) krud
                  tell $ rl1 $ dbMetUzoj ind 0
                  case ag of
                    Left () -> do
                      for_ rezcxelM (dbMetSxl >=> traverse_ (tell <<< rl1))
                      eti @"rubInd" $ tell $ rl1 ind
                    Right indj -> do
                      tell $ rl1 $ R.DelCF (snd $ akirAmas db) indk
                      for_ indj $ spGxisUzoj (\el -> modify (el:)) $ B.first (\x -> x - 1)
                _ -> throwError $ DbKorupt "GC index corruption"
        for_ (HS.toList $ spNeNul uzoj) \ind -> case HM.lookup ind $ spHm uzoj of
          Just (indUz, rezcxelM) | indUz /= 0 -> do
            tell $ rl1 $ dbMetUzoj ind indUz
            for_ rezcxelM (dbMetSxl >=> traverse_ (tell <<< rl1))
          _ -> throwError $ DbKorupt "GC index corruption"
    levM DbErar =<< sekuraIO (R.write (akirDb db) $
      R.PutCF (akirStatusCf db) "rubInd" (S.encode rubInd):R.PutCF (akirStatusCf db) "rubLigil" (S.encode $ join rubLigil):opj)

dbKonekt :: forall sig m. Has IIO sig m => UzDet -> UzFaldF -> (DbKonekt -> m ()) -> m ()
dbKonekt uzDet uzFaldF f = liftWith @SIO (\trk knt -> UnsafeIO $ 
  (R.withDBCF "obs-db" agord ((, agord) <$> tabelList) \db -> malSIO $ trk $ knt $> f' db)
  `catchAny` (\e -> malSIO $ trk $ knt $> inf IErar ("IO: " <> fromString (displayException e))))
  where
  agord = def { R.createIfMissing = True }
  tabelList = ["status", "memo", "amas", "amasUzoj", "eventList", "fald"]
  f' :: R.DB -> m ()
  f' db = case R.columnFamilies db of
    [status, memo, amas, amasUzoj, eventList, fald] -> do      
      rezM <- sendM $ UnsafeIO $ runError @String do
        it <- R.createIterator db (Just eventList)
        R.iterLast it
        listInd <- R.iterKey it >>= maybe (pure $ EventInd 0) (S.decode >>> levE) >>= newTVarIO
        R.destroyIterator it
        amasInd <- R.getCF db status "amasLast" >>= maybe (pure 0) (S.decode >>> levE) >>= newTVarIO
        prov <- lift H.newIO
        epok <- newTVarIO $ Epok 0
        rec
          (sig, fin) <- lift $ transigil kon
          let kon = DbKonekt 
                { akirDb = db
                , akirUzDet = uzDet
                , akirUzFaldF = uzFaldF
                , akirList = (listInd, eventList)
                , akirMemoCf = memo, akirFaldCf = fald
                , akirProv = prov
                , akirEpok = epok
                , akirAmas = (amasInd, amas)
                , akirAmasUzojCf = amasUzoj
                , akirStatusCf = status
                , akirTransigil = sig
                }
        pure (fin, kon)
      case rezM of
        Left e -> inf IErar (fromString e)
        Right (fin, kon) -> do
          nomspac "db" $ inf IInf "Starting the DB & transaction daemon"
          liftWith @SIO \trk knt -> UnsafeIO $ malSIO (trk $ (knt $>) $ f kon) `finally` do
            _ <- malSIO $ trk $ (knt $>) $ nomspac "db" $ inf IInf "Stopping the DB & banishing the daemon..."
            putMVar (akirTransigil kon) TransigilMortig
            readMVar fin
    _ -> nomspac "db" $ inf IErar "DB tables mismatch"

{-
data Arb a = ArbNul | Arb (IndDet (Arb (a, a, a, a))) (ArbEl a)
data ArbEl a = ArbEl0 | ArbEl1 a | ArbEl2 a a | ArbEl3 a a a

-- ind :: Has Transag sig m => a -> Ind a

pusxArb :: Has Transag sig m => a -> Arb a -> Arb a
pusxArb a ArbNul = ind ArbNul >>= \n -> Arb n (ArbEl1 a)
pusxArb a (Arb sub el) = case el of
  ArbEl0 -> supr $ ArbEl1 a
  ArbEl1 a1 -> supr $ ArbEl2 a1 a
  ArbEl2 a1 a2 -> supr $ ArbEl1 a1 a2 a
  ArbEl3 a1 a2 a3 -> do
    sub' <- ind $ Det $ PusxArb (a1, a2, a3, a) sub
    pure $ Arb sub' ArbEl0
  where supr = pure <<< Arb sub

mapArb :: Has Transig sig m => DetFunk (a -> b) -> Arb a -> m (Arb b)
mapArb f ArbNul = ArbNul
mapArb f (Arb sub el) = do
  sub' <- ind $ Det (MapArb (DDu f) sub)
  Arb sub' <$> case el of
    ArbEl0 -> pure ArbEl0
    ArbEl1 a -> ArbEl1 <$> uzDF a
    ArbEl2 a1 a2 -> ArbEl2 <$> uzDF a1 a2
    ArbEl3 a1 a2 a3 -> ArbEl3 <$> uzDF a1 a2 a3

malIndDet :: Has Transag sig m => Ind (Det a) -> m a
malIndDet ind = legInd ind >>= \case
  DetPur a -> pure a
  Det f -> do
    rez <- uzDF f
    ?unsafeSuperskrib ind rez
    pure rez

uzDF :: Has Transag sig m => DetFunk a -> m a
uzDF = \case
  DDu f -> (\f' (a, b) -> (f' a, f' b)) <$> uzDF f
  PusxArb a arb -> malInd arb >>= malDet >>= 
  


-- kolektArb :: Has Transig sig m => DetFunk (a -> b) -> DetFunk (b -> b -> b) -> Arb a -> 
-}