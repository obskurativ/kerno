module Event where

import Control.Effect.Empty
import Control.Effect.Throw
import Data.Aeson
import qualified Data.Aeson as Aeson
import qualified Data.Text as T
import Datum
import Network.HTTP.Types (Status)
import SPrelude
import Db.RocksDb (DbKonekt)

-- FARENDE: Mi ne scias... esperantigi?
-- Komenco
data InitEvent = InitEvent deriving (Generic)

instance Event InitEvent where
  type ESignal InitEvent = ()

type RequestErr =
  Var
    ( "missingField" .== Text
        .+ "parseError" .== Text
        .+ "fieldParseError" .== Text
        .+ "internalError" .== ()
        .+ "noAuth" .== ()
        -- Aŭtentiĝo
        .+ "limitNotSatisfied" .== Text
        .+ "nonUniqueName" .== ()
        .+ "corruptedFile" .== ()
        .+ "invalidCode" .== ()
        .+ "invalidEmail" .== ()
        -- Pet
        .+ "unapplicable" .== ()
        .+ "badGateway" .== ()
    )

petErar :: (RequestErr ~ Var r, KnownSymbol l) => Label l -> (r .! l) -> RequestErr
petErar = IsJust

enErar :: RequestErr
enErar = IsJust #internalError ()

-- trans :: Has (Reader DbKonekt :+: IIO :+: Throw RequestErr) sig m => ErrorC DbErar m a => m a
-- trans f = nomspac "db-transaction" $ runError () 

data RequestEvent = RequestEvent
  { getLanguages :: [Lingvo],
    getBodyField :: forall sig m. Has (UzSTM :+: IIO :+: Error RequestErr) sig m => Text -> m Value,
    getHost :: Text
  }
  deriving (Typeable)

data RequestSignal = forall a b. ToJSON (ApiRespond a b) => RequestSignal' (ApiRespond a b)

instance Event RequestEvent where
  type ESignal RequestEvent = RequestSignal
  type EUzKnt RequestEvent = [Text]

data ApiRespond a b = ERROR a | OK b
  deriving (Generic)

instance (ToJSON a, ToJSON b) => ToJSON (ApiRespond a b) where
  toEncoding = genericToEncoding'

apiRespond :: (ToJSON a, Functor m) => ErrorC RequestErr m a -> m RequestSignal
apiRespond ag =
  runError ag <&> \rez -> RequestSignal' $ case rez of
    Left e -> ERROR e
    Right x -> OK x

newtype AssetEvent = AssetEvent [Text]

instance Event AssetEvent where
  type ESignal AssetEvent = Either Status (ByteString, LByteString)
  type EUzKnt AssetEvent = [Text]

akirKamp :: (FromJSON a, Has (Error RequestErr :+: UzSTM :+: IIO) sig m) => RequestEvent -> Text -> m a
akirKamp ev kamp = do
  krud <- getBodyField ev kamp
  case fromJSON krud of
    Aeson.Error e -> throwError $ petErar #fieldParseError (T.pack e)
    Aeson.Success x -> pure x
