{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Datum where

import Control.Carrier.Lift (LiftC)
import qualified Control.Carrier.Reader as E
import Control.Effect.Lift (Lift, sendM)
import qualified Data.Aeson as A
import qualified Data.Bifunctor as BF
import Data.Pool
import qualified Data.Text.IO as T
import Data.Typeable
import Evakuil (WikidotKonekt)
import GHC.Generics (Rep)
import qualified ListT
import Network.DNS (ResolvSeed)
import qualified Network.Greskell.WebSocket as G
import qualified RIO.HashMap as HM
import RIO.Time
import SPrelude
import qualified StmContainers.Map as SM

-- Sekura IO
newtype SIO a = UnsafeIO {malSIO :: IO a}
  deriving newtype (Functor, Applicative, Monad)

io' :: IO a -> SIO (Either Utf8Builder a)
io' ag = UnsafeIO $ (Right <$> ag) `catchAny` (pure <<< Left <<< display)

-- Inf
data InfTip = IInf | IAvert | IErar

data Inf m a where
  Inf :: InfTip -> Utf8Builder -> Inf m ()
  Nomspac :: Utf8Builder -> m a -> Inf m a

newtype InfC m a = InfC {malInfC :: E.ReaderC (Handle, Maybe Utf8Builder) m a}
  deriving newtype (Functor, Monad, Applicative)

instance Has (Lift SIO) sig m => Algebra (Inf :+: sig) (InfC m) where
  alg trk sig knt = InfC $ case sig of
    L (Inf tip msg) ->
      (<$ knt) <$> do
        (h, spac) <- E.ask
        let ttip = case tip of
              IInf -> "INF"
              IAvert -> "WARN"
              IErar -> "ERR"
        r <-
          sendM $
            io' $
              T.hPutStrLn h $
                utf8BuilderToText $
                  "[" <> ttip <> "]" <> maybe "" (\x -> " |" <> x <> "|") spac <> " " <> msg
        when (isLeft r) $
          void $
            sendM $
              io' $
                T.putStrLn "Failed to log message."
    L (Nomspac ald m) ->
      trk (m <$ knt) & \(InfC legil) ->
        E.local
          ( \(h :: Handle, nunNomspac) -> (h,) $ Just case nunNomspac of
              Just n -> n <> "." <> ald
              Nothing -> ald
          )
          legil
    R au -> alg (malInfC . trk) (R au) knt

class Monad m => MonadSTM m where
  stm' :: STM a -> m a

instance MonadSTM SIO where
  stm' = UnsafeIO <<< atomically

instance MonadSTM STM where
  stm' = id

newtype STMC m a = STMC {malSTMC :: LiftC m a}
  deriving newtype (Functor, Applicative, Monad)

newtype UzSTM m a = UzSTM (STM a)

instance MonadSTM m => Algebra (UzSTM :+: Lift m) (STMC m) where
  alg trk sig knt = STMC $ case sig of
    L (UzSTM ago) -> (<$ knt) <$> sendM @m (stm' ago)
    R r -> alg (malSTMC <<< trk) r knt

stm :: Has UzSTM sig m => STM a -> m a
stm = send <<< UzSTM

inf :: Has Inf sig m => InfTip -> Utf8Builder -> m ()
inf tip = send . Inf tip

nomspac :: Has Inf sig m => Utf8Builder -> m a -> m a
nomspac knt = send . Nomspac knt

sekuraIO :: Has (Lift SIO :+: Inf) sig m => IO a -> m (Maybe a)
sekuraIO ago = do
  r <- sendM $ io' ago
  case r of
    Left erar -> do
      inf IErar ("IO " <> erar)
      pure Nothing
    Right x -> pure $ Just x

sekuraIOAu ::
  forall label sig m a.
  (Has (Lift SIO :+: Inf) sig m, LabelledMember label (Error ()) sig) =>
  IO a ->
  m a
sekuraIOAu io = sekuraIO io >>= eti @label . levM ()

-- Sekura ago je nomspaco
sekuraION :: Has (Lift SIO :+: Inf) sig m => Utf8Builder -> IO a -> m (Maybe a)
sekuraION n = nomspac n . sekuraIO

newtype Spac = Spac {malSpac :: Text} deriving newtype (Eq, Hashable)

newtype Etend = Etend {malEtend :: ByteString} deriving newtype (Eq, Hashable)

class (Typeable e, Typeable (ESignal e), Typeable (EUzKnt e)) => Event e where
  type ESignal e
  type EUzKnt e
  type EUzKnt e = ()

data EventTraktil e = EventTraktil !(EUzKnt e) !(forall sig m. Has FunkKnt sig m => e -> m (ESignal e))

data EventTraktiloj = forall e. Event e => EventTraktiloj [EventTraktil e]

type Lingvo = Text

type Netradukita = Text

type Tradukoj = HashMap Netradukita A.Value

newtype PigraVal a = PigraVal {malPigraVal :: forall sig m. Has IIO sig m => m (Maybe a)}

data Servil = Servil
  { akirGr :: !(Pool G.Client),
    akirDNSSem :: !ResolvSeed,
    akirLingvDat :: !(SM.Map Etend (HashMap Lingvo Tradukoj)),
    akirSpac :: HashMap Spac [Etend],
    akirTraktil :: !(SM.Map Etend (SM.Map TypeRep EventTraktiloj)),
    akirSendmail :: !FilePath,
    akirWikidotKonekt :: !(PigraVal WikidotKonekt)
  }

type IIO = Inf :+: Lift SIO :+: UzSTM

type ServilKnt = Reader Servil :+: IIO

type SpacKnt = ServilKnt :+: Reader Spac

type FunkKnt = SpacKnt :+: Reader Etend

-- | Peti traktilojn por specifa evento, provizinte elektilon de partoprenantaj etandaĵoj.
traktilPorEvent ::
  forall e sig m.
  (Has ServilKnt sig m, Event e) =>
  (forall a. SM.Map Etend a -> STM [(Etend, a)]) ->
  m [(Etend, EventTraktil e)]
traktilPorEvent elektil = do
  trkMap <- asks akirTraktil
  fontoj <- stm (elektil trkMap)
  join <$> forM fontoj \(etend, etendTrkj) -> do
    trkj <- stm $ SM.lookup (typeRep $ Proxy @e) etendTrkj
    pure $ case trkj of
      Nothing -> []
      Just (EventTraktiloj x)
        | Just x' <- cast x -> (\tr -> (etend, tr)) <$> x'
        | otherwise -> error "List of event handlers corrupted, unable to cast"

traktilCxiuj :: forall e sig m. (Has ServilKnt sig m, Event e) => m [(Etend, EventTraktil e)]
traktilCxiuj = traktilPorEvent (ListT.toList . SM.listT)

spacEtend :: Has SpacKnt sig m => m [Etend]
spacEtend = do
  spacoj <- asks akirSpac
  spac <- ask
  case HM.lookup spac spacoj of
    Just etj -> pure etj
    Nothing -> pure []

traktilSpac :: forall e sig m. (Has SpacKnt sig m, Event e) => m [(Etend, EventTraktil e)]
traktilSpac = do
  etj <- spacEtend
  traktilPorEvent \m ->
    catMaybes <$> forM etj \et -> (et,) <<$>> SM.lookup et m

enmetTraktil :: forall e sig m. (Has ServilKnt sig m, Event e) => Etend -> [EventTraktil e] -> m ()
enmetTraktil etend trk = do
  servil :: Servil <- ask
  etTrk <- stm $ do
    etTrk <- SM.lookup etend (akirTraktil servil)
    case etTrk of
      Just x -> pure x
      Nothing ->
        SM.new
          ^| \novEtTrk -> SM.insert novEtTrk etend (akirTraktil servil)
  stm $ SM.insert (EventTraktiloj trk) (typeRep $ Proxy @e) etTrk

genericToEncoding' :: (Generic a, A.GToJSON' A.Encoding A.Zero (Rep a)) => a -> A.Encoding
genericToEncoding' =
  A.genericToEncoding $
    A.defaultOptions
      { A.sumEncoding =
          A.TaggedObject
            { A.tagFieldName = "tag",
              A.contentsFieldName = "value"
            }
      }

-- Forigi?
medio :: InfC m a -> m a
medio = E.runReader (stdout, Nothing) . malInfC

medioS :: Servil -> InfC (E.ReaderC Servil m) a -> m a
medioS s = E.runReader s . medio

unsnoc :: [a] -> Maybe ([a], a)
unsnoc [] = Nothing
unsnoc [x] = Just ([], x)
unsnoc (x : xs) = BF.first (x :) <$> unsnoc xs

traceTime' :: Monad m => m UTCTime -> Text -> m a -> m a
traceTime' temp tekst ag = do
  kom <- temp
  r <- ag
  fin <- temp
  traceM $ tekst <> ":" <> tshow (diffUTCTime fin kom)
  pure r

{-# WARNING traceTimeS "Trace" #-}
traceTimeS :: Has (Lift SIO) sig m => Text -> m a -> m a
traceTimeS = traceTime' (sendM (UnsafeIO getCurrentTime))

{-# WARNING traceTimeI "Trace" #-}
traceTimeI :: Has LiftIO sig m => Text -> m a -> m a
traceTimeI = traceTime' (sendIO getCurrentTime)
