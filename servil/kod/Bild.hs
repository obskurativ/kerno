module Bild where

import Codec.Picture
import Codec.Picture.Extra (crop)
import qualified Codec.Picture.Jpg as Jpg
import qualified Codec.Picture.Png as Png
import qualified Codec.Picture.WebP as WebP
import Data.Row
import GHC.IO (unsafePerformIO)
import RIO
import SPrelude

type BildPart =
  Rec
    ( "topx" .== Int
        .+ "topy" .== Int
        .+ "bottomx" .== Int
        .+ "bottomy" .== Int
    )

permBildoj :: [Text]
permBildoj = ["image/jpeg", "image/png", "image/webp"]

webpBildet :: Text -> ByteString -> BildPart -> Maybe ByteString
webpBildet mime font part =
  ( case mime of
      "image/jpeg" -> convertRGBA8 <$> alMaybe (Jpg.decodeJpeg font)
      "image/png" -> convertRGBA8 <$> alMaybe (Png.decodePng font)
      "image/webp" -> WebP.decodeRgba8 font
      _ -> Nothing
  )
    <&> ( WebP.encodeRgba8Lossless
            <<< crop
              (part .! #topx)
              (part .! #topy)
              (part .! #bottomx - part .! #topx)
              (part .! #bottomy - part .! #topy)
        )
