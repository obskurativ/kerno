{ mkDerivation, aeson, attoparsec, base, base64, bifunctors, binary
, bytestring, case-insensitive, cereal, cmdargs, conduit
, constraints, cryptohash-sha256, cryptonite, data-default
, directory, dns, email-validate, filepath, fin, focus, free
, fused-effects, greskell, greskell-websocket, hashable, http-types
, JuicyPixels, JuicyPixels-extra, lib, list-t, mime-mail
, monad-logger, mtl, network, resource-pool, rio
, rocksdb-haskell-jprupp, row-types, stm, stm-containers, stm-hamt
, template-haskell, text, text-icu, time, transformers
, unordered-containers, vec, wai, wai-app-static, warp, webp, which
, wikidot-evakuilo, xml-conduit, xml-types
}:
mkDerivation {
  pname = "obskurativ";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    aeson attoparsec base base64 bifunctors binary bytestring
    case-insensitive cereal cmdargs conduit constraints
    cryptohash-sha256 cryptonite data-default directory dns
    email-validate filepath fin focus free fused-effects greskell
    greskell-websocket hashable http-types JuicyPixels
    JuicyPixels-extra list-t mime-mail monad-logger mtl network
    resource-pool rio rocksdb-haskell-jprupp row-types stm
    stm-containers stm-hamt template-haskell text text-icu time
    transformers unordered-containers vec wai wai-app-static warp webp
    which wikidot-evakuilo xml-conduit xml-types
  ];
  license = lib.licenses.agpl3Only;
  mainProgram = "obskurativ";
}
